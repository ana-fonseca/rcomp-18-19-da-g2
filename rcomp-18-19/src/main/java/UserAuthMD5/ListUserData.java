/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserAuthMD5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author 1161232
 */
public class ListUserData {

    List<User> userDataList = new ArrayList<>();
    String filePath;

    public ListUserData(String path) {
        filePath = path;
    }

    public void addUser(String username, String password) {
        String pass = Authentication.ToMD5Hash(password);
        User user = new User(username, pass, null);
        userDataList.add(user);
    }

    public void updateListFromFile() throws FileNotFoundException {
        userDataList.clear();
        File file = new File(filePath);
        Scanner inputStream = new Scanner(file);
        while (inputStream.hasNextLine()) {

            String userData = inputStream.nextLine();
            String[] data = userData.split(";");
            userDataList.add(new User(data[0], data[1], null));
        }
        inputStream.close();
    }

    public void updateFileFromList() throws IOException {
        Formatter f = new Formatter(new File(filePath));
        String filename = filePath;
        FileWriter fw = new FileWriter(filename);
        for (User user : userDataList) {
            fw.write(user.Username() + ";" + user.HashPassword() + "\n");
        }
        fw.close();
    }

    public void updateUserToken(User user, String token) {
        user.ChangeAuthenticationToken(token);
    }

    public User findUser(String username) {
        User user = null;
        for (User u : userDataList) {
            if (u.Username().equals(username)) {
                user = u;
                break;
            }
        }
        return user;
    }

    public void updateUserToken(String username, String token) {
        User u = findUser(username);
        u.ChangeAuthenticationToken(token);
    }

}
