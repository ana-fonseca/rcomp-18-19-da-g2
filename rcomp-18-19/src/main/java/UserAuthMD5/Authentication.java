/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserAuthMD5;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 *
 * @author João
 */
public class Authentication {

    private static String passwordToHash;
    private static final String FILE = "userHashes.txt";
    private ListUserData list;

    public Authentication(ListUserData list, String username) {
        this.list = list;
    }

    public static String ToMD5Hash(String string) {

        String myHash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] digest = md.digest();
            myHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Error Creating Hash");
        }
        return myHash;
    }

    public boolean Authenticate(String username, String password) {
        User user = list.findUser(username);
        boolean valid;
        if (user == null || user.AuthenticationToken() == null) {
            valid = false;
        } else {

            String validationHashInserted = ToMD5Hash(ToMD5Hash(password) + user.AuthenticationToken());
            String validationHash = ToMD5Hash(user.HashPassword() + user.AuthenticationToken());
            if (validationHash.equals(validationHashInserted)) {
                valid = true;
            } else {
                valid = false;
            }
            user.ChangeAuthenticationToken(null);
        }

        return valid;
    }

    public String getAuthToken(String username) {

        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[8];
        random.nextBytes(bytes);
        String token = bytes.toString();
        try {
            list.updateUserToken(username, token);
        } catch (NullPointerException ex) {

        }
        return token;
    }

}
