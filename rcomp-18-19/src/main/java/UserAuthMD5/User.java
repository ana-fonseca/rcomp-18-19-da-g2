/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserAuthMD5;

/**
 *
 * @author 1161232
 */
public class User {

    private String username;
    private String hashPassword;
    private String authenticationToken;

    public User(String username, String hashPassword, String authenticationToken) {
        this.username = username;
        this.hashPassword = hashPassword;
        this.authenticationToken = authenticationToken;
    }

    public String Username() {
        return username;
    }

    public String HashPassword() {
        return hashPassword;
    }

    public String AuthenticationToken() {
        return authenticationToken;
    }

    public void ChangeAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

}
