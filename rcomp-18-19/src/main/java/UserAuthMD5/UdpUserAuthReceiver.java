/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserAuthMD5;

import core.Config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class UdpUserAuthReceiver implements Runnable {

    private static final String FILE = "userHashes.txt";
    static ListUserData list = new ListUserData(FILE);
    static Authentication authentication = new Authentication(list, FILE);

    private DatagramSocket sock;

    public UdpUserAuthReceiver(DatagramSocket sock) throws SocketException, IOException {
        try {
            list.updateListFromFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UdpUserAuthReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.sock = sock;
        sock.setSoTimeout(Config.TIMEOUT);
    }

    public static String Authenticate(String username, String password) {
        boolean authenticate = authentication.Authenticate(username, password);
        if (authenticate) {
            return "Authenticated!";
        } else {
            return "Not Authenticated!";
        }
    }

    public static String GetAuthToken(String username) {
        String token = authentication.getAuthToken(username);
        return token;
    }

    @Override
    public void run() {
        int flag = 1;
        byte[] data = new byte[500];
        String frase, appName, function, response = "";
        DatagramPacket p;
        InetAddress currPeerAddress;
        p = new DatagramPacket(data, data.length);
        while (true) {
            data = new byte[500];
            p.setData(data, 0, data.length);
            try {
                sock.receive(p);
            } catch (SocketTimeoutException ex) {
                Thread.currentThread().interrupt(); // TIMEOUT
            } catch (IOException e) {
                Thread.currentThread().interrupt();
            }

            if (p.getPort() == Config.SERVICE_PORT) {

                currPeerAddress = p.getAddress();
                frase = new String(p.getData(), 0, p.getLength()); // frase
                String[] paramList = (frase.trim()).split(Config.SEPARATOR);
                appName = paramList[0];
                function = paramList[1];
                String username;
                if (appName.equalsIgnoreCase(Config.USER_AUTH_MD5_NAME)) {

                    switch (function) {
                        case Config.ARRIVAL_MSG:
                            UdpUserAuth.addIP(appName, currPeerAddress);
                            System.out.println(appName + Config.SEPARATOR + Config.ARRIVAL_MSG);
                            flag = 1;
                            break;
                        case Config.EXIT_MSG:
                            UdpUserAuth.remIP(appName, currPeerAddress);
                            System.out.println(appName + Config.SEPARATOR + Config.EXIT_MSG);

                            flag = 1;
                            break;
                        case Config.GET_AUTH_TOKEN_OPTION:
                            username = paramList[2];
                            String token = authentication.getAuthToken(username);

                            System.out.println("Token:" + token);

                            break;
                        case Config.AUTHENTICATE_OPTION:
                            username = paramList[2];

                            String pass = paramList[3];
                            boolean authenticate = authentication.Authenticate(username, pass);
                            if (authenticate) {
                                System.out.println("Authenticated");
                            } else {
                                System.out.println("Ups! Wrong credentials!");
                            }

                            break;
                        default:
                            flag = 1;
                            break;
                    }

                } else if (function.trim().equalsIgnoreCase(Config.ARRIVAL_MSG)) {

                    if (UdpUserAuth.checkApplicationName(appName.trim()) == null) {

                        UdpUserAuth.addIP(appName.trim(), currPeerAddress);
                        data = (Config.USER_AUTH_MD5_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG).getBytes();
                        p.setData(data);
                        p.setLength(data.length);
                        p.setAddress(currPeerAddress);

                        try {
                            sock.send(p);
                        } catch (IOException ex) {
                            System.out.println("Reply ping not sent.");
                        }
                        System.out.println(appName.trim() + " is now online.");
                    }
                } else if (function.trim().equalsIgnoreCase(Config.EXIT_MSG)) {//APP EXITED

                    UdpUserAuth.remIP(appName.trim(), currPeerAddress);
                    System.out.println(appName.trim() + " is now offline.");

                }
                p.setPort(1);

            }
        }

    }

}
