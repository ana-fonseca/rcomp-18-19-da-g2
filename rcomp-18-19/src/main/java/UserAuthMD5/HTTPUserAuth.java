package UserAuthMD5;

import core.Config;
import Stack.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;

/**
 * Based in PL21-Rcomp
 */
@SuppressWarnings("Duplicates")
public class HTTPUserAuth {

    static private final String BASE_FOLDER = "www/userAuthMD5";
    static private ServerSocket sock;
    private HTTPUserAuthRequest req;



    public void runHTTP(DatagramSocket s, DatagramPacket p) {
        // OPEN SOCKET
        try {
            sock = new ServerSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + Config.SERVICE_PORT);
            System.exit(1);
        }
        // SERVER LOOP
        req = new HTTPUserAuthRequest(sock,s,p, BASE_FOLDER);
        req.start();
    }

}
