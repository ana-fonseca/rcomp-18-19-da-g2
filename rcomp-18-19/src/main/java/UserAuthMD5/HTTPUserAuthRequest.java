package UserAuthMD5;

import core.Config;
import core.HTTPmessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

;


@SuppressWarnings("Duplicates")
public class HTTPUserAuthRequest extends Thread {

    String baseFolder;
    Socket sock;
    ServerSocket svSock;
    DatagramSocket s;
    DatagramPacket p;
    DataInputStream inS;
    DataOutputStream outS;

    public HTTPUserAuthRequest(ServerSocket cliSock, DatagramSocket s, DatagramPacket p, String baseFolder) {
        this.baseFolder = baseFolder;
        this.svSock = cliSock;
        this.s = s;
        this.p = p;
    }

    public void run() {
        while (true) {
            try {
                sock = svSock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outS = new DataOutputStream(sock.getOutputStream());
                inS = new DataInputStream(sock.getInputStream());
            } catch (IOException ex) {
                System.out.println("Thread error on data streams creation");
            }
            try {
                HTTPmessage request = new HTTPmessage(inS);
                HTTPmessage response = new HTTPmessage();
                if (request.getMethod().equals(Config.GET)) {
                    if (request.getURI().equals(Config.USERAUTH_APPLICATION_REQUEST)) {
                        response.setContentFromString(UdpUserAuth.printIpsHtml(), "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else {
                        String fullname = baseFolder + "/";
                        if (request.getURI().equals("/")) {
                            fullname = fullname + "index.html";
                        } else {
                            fullname = fullname + request.getURI();

                            System.out.println(fullname);
                        }
                        if (response.setContentFromFile(fullname)) {
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else {
                            response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                    }

                    response.send(outS);
                } else { // NOT GET
                    if (request.getMethod().equals(Config.PUT)) {

                        String resposta;
                        System.out.println(request.getURI() + " " + request.getURI().length());
                        String split[] = request.getURI().split(Config.SEPARATOR);
                        System.out.println(split[2]);
                        if (split.length > 2) { // blank space / stacks / instuction / id / elemento(push)
                            if (split[2].trim().equalsIgnoreCase("Authenticate")) { // push
                                Thread.currentThread().sleep(500);
                                response.setContentFromString("<p>" + UdpUserAuthReceiver.Authenticate(split[3], split[4]) + "</p>", "text/html"); // resposta
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                            } else if (split[2].trim().equalsIgnoreCase("GetAuthToken")) {
                                try {
                                    Thread.currentThread().sleep(500);
                                    response.setContentFromString("<p>" + UdpUserAuthReceiver.GetAuthToken(split[3]) + "/p>", "text/html"); // resposta
                                    response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                } catch (IllegalArgumentException ex) {
                                    response.setContentFromString("Error, Not found!", "text/html"); // erro
                                    response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                }
                            }
                        }
                    } else {
                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                    }
                    response.send(outS);
                }
            } catch (IOException | InterruptedException ex) {
                System.out.println("Thread error when reading request");
            }
            try {
                sock.close();
            } catch (IOException ex) {
                System.out.println("CLOSE IOException");
            }
        }

    }
}
