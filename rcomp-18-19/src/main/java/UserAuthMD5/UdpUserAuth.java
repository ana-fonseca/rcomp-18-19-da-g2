package UserAuthMD5;

import core.Application;
import core.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;

@SuppressWarnings("Duplicates")
public class UdpUserAuth {

    static InetAddress address;
    static DatagramSocket sock;
    private static HashSet<Application> peersList = new HashSet<>();

    /**
     * Adds IP to the application list.
     *
     * @param name appName
     * @param ip ip
     * @return boolean if added
     */
    public static synchronized boolean addIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        return peersList.add(a);
    }

    /**
     * Removes ip from peersList
     *
     * @param name appName
     * @param ip ip
     */
    public static synchronized void remIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        peersList.remove(a);
    }

    /**
     * Prints all the application in the list.
     */
    public static synchronized void printIPs() {
        for (Application a : peersList) {
            System.out.print(" " + a.getName() + " ip: " + a.getIp());
        }
    }

    public static synchronized String printIpsHtml() {
        String p = Config.APP_TABLES;
        for (Application a : peersList) {
            p = p + "<tr><td>" + a.getName() + "</td><td>" + a.getIp() + "</td></tr>";
        }
        return p;
    }

    /**
     * Checks if the name passed by parameter exists in the application list and
     * returns it if it does.
     *
     * @param s name
     * @return application
     */
    public static synchronized Application checkApplicationName(String s) {
        for (Application a : peersList) {
            if (a.getName().equals(s.trim())) {
                return a;
            }
        }
        return null;
    }

    /**
     * Sends packets to the application passed by parameter
     *
     * @param s socket
     * @param p packet
     * @param appName app name
     * @throws IOException expcetion
     */
    public static void sendPacketToApplication(DatagramSocket s, DatagramPacket p, Application appName) throws
            IOException {
        p.setAddress(appName.getIp());
        s.send(p);
    }

    /**
     * Sends message by socket + packet.
     *
     * @param str message
     * @param s socket
     * @param p packet
     */
    public static synchronized void sendPacket(String str, DatagramSocket s, DatagramPacket p) {
        byte[] data = new byte[50];
        data = str.getBytes();
        p.setData(data);
        p.setLength(data.length);
        try {
            s.send(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Main SENDER
     *
     * @param args args
     * @throws IOException exc
     */
    public static void main(String[] args) throws IOException {
        String frase;
        byte[] data = new byte[300];
        DatagramPacket udpPacket;
        // SOCK OPENER

        try {
            sock = new DatagramSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        // ADDRESS
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        address = InetAddress.getByName(Config.BCAST_ADDR);

        // THREAD CREATION
        // NOTIFICATION ARRIVAL
        frase = Config.USER_AUTH_MD5_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG;
        data = frase.getBytes();
        udpPacket = new DatagramPacket(data, data.length, address, Config.SERVICE_PORT);
        // HTTP SERVER
        HTTPUserAuth http = new HTTPUserAuth();
        http.runHTTP(sock, udpPacket);

        //THREAD CREATION
        Thread udpReceiver = new Thread(new UdpUserAuthReceiver(sock));
        udpReceiver.start();

        // SEND
        sock.send(udpPacket);

    }
}
