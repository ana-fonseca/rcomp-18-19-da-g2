package NumConvert;

import core.Config;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

/**
 * Based in PL21-Rcomp
 */
@SuppressWarnings("Duplicates")
public class HTTPNumConvert {

    static private final String BASE_FOLDER = "www/numConvert";
    static private ServerSocket sock;
    private static int accessesCounter;

    public ServerSocket runHTTP(DatagramSocket s) throws IOException {
        accessesCounter = 0;
        // OPEN SOCKET
        try {
            sock = new ServerSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + Config.SERVICE_PORT);
            System.exit(1);
        }
        // SERVER LOOP
        HTTPNumConvertRequest req = new HTTPNumConvertRequest(sock,s, BASE_FOLDER);
        req.start();
        return sock;
    }


    public static String convert(NumConverter nc) {
        return nc.numConvert();
    }

    public static String listAllPossibleTypes() {
        return NumConverter.listPossibleTypes();
    }
    public static void sendResponseFromOtherServer(ServerSocket s){

    }
}
