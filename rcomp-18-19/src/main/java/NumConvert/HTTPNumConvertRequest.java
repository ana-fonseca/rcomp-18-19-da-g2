package NumConvert;

import core.Application;
import core.Config;
import core.HTTPmessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class HTTPNumConvertRequest extends Thread {

    String baseFolder;
    ServerSocket serverSock;
    Socket sock;
    DatagramSocket dgSock;
    DataInputStream inS;
    DataOutputStream outS;

    public HTTPNumConvertRequest(ServerSocket cliSock, DatagramSocket dg, String baseFolder) {
        this.baseFolder = baseFolder;
        this.serverSock = cliSock;
        this.dgSock = dg;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sock = serverSock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outS = new DataOutputStream(sock.getOutputStream());
                inS = new DataInputStream(sock.getInputStream());
            } catch (IOException ex) {
                System.out.println("Thread error on data streams creation");
            }
            try {
                HTTPmessage request = new HTTPmessage(inS);
                HTTPmessage response = new HTTPmessage();
                //System.out.println(request.getURI());
                //  System.out.println(request.getMethod());
                String split[] = (request.getURI().trim()).split(Config.SEPARATOR);
                if (request.getMethod().equals(Config.GET)) {
                       System.out.println("URI " + request.getURI().trim());
                    if (split.length == 0) {
                        String fullname = baseFolder + "/";
                        if (request.getURI().equals("/")) {
                            fullname = fullname + "index.html";
                        } else {
                            fullname = fullname + request.getURI();
                        }
                        if (response.setContentFromFile(fullname)) {
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else {
                            response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                    } else if (split[1].equalsIgnoreCase("rcomp-http.js")) {
                        String fullname = baseFolder + "/";
                        if (request.getURI().equals("/")) {
                            fullname = fullname + "index.html";
                        } else {
                            fullname = fullname + request.getURI();
                        }
                        if (response.setContentFromFile(fullname)) {
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else {
                            response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                    } else if (split[1].equals((Config.REQUEST_NAME_NUM_CONVERT.substring(1)))) {
                        System.out.println("converter");
                        if (split.length > 2) {//3 ou mais ("" numConvert List"
                            try {
                                String result;
                                switch (split[2].trim().toUpperCase()) {
                                    case Config.CONVERT_OPTION:
                                        System.out.println("convert");
                                        if (split.length > 5) {
                                            NumConverter nc = new NumConverter(split[3].trim(), split[4].trim(), split[5].trim()); //BIN HEX 1111 (exemplo)
                                            result = HTTPNumConvert.convert(nc);
                                            response.setContentFromString("<p> Conversão para " + split[4].trim() + ": " + result + "</p>", "text/html"); //resposta
                                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                        } else {
                                            System.out.println("erro convert");
                                            response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                        }
                                        break;

                                    case Config.LIST_NUM_OPTION:
                                        System.out.println("list");
                                        result = HTTPNumConvert.listAllPossibleTypes();
                                        response.setContentFromString("<p> Possible Types: " + result + "</p>", "text/html"); //resposta
                                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                        break;
                                    case "APPLICATION":
                                        String s = "";
                                        HashMap<String, Integer> tempMap = new HashMap<>();
                                        for (Application a : UdpNumConvert.peersList) {
                                            if (tempMap.containsKey(a.getName())) {
                                                tempMap.put(a.getName(), tempMap.get(a.getName() + 1));
                                            } else {
                                                tempMap.put(a.getName(), 1);
                                            }
                                        }
                                        for (String str2 : tempMap.keySet()) {
                                            s = s + "<tr> <td>" + str2 + "</td> <td>" + tempMap.get(str2) + "</td> </tr>";
                                        }
                                        response.setContentFromString(s, "text/html"); //resposta
                                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                        break;
                                    case "RESPONSE":
                                        System.out.println(request.getURI());
                                    default:
                                        System.out.println("default");
                                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                        break;
                                }
                            } catch (IllegalArgumentException ex) {
                                System.out.println("illegal arg exp");
                                response.setContentFromString(Config.NUM_CONVERT_NOT_FOUND, "text/html"); // erro
                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                            }
                        }
                    } else if (UdpNumConvert.checkApplicationName(split[1]) != null) {
                        // System.out.println("tou");
                        Application app = UdpNumConvert.checkApplicationName(split[1]);
                        String msgOut = ((request.getURI().trim().replace(" ", Config.SEPARATOR)).trim()).substring(1);
                        //System.out.println(msgOut);
                        byte[] arrayByte = msgOut.getBytes();
                        DatagramPacket dgp = new DatagramPacket(arrayByte, arrayByte.length);
                        dgp.setAddress(app.getIp());
                        dgp.setPort(Config.SERVICE_PORT);
                        dgSock.send(dgp);
                    } else {
                        System.out.println("else");
                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                    }
                    response.send(outS);
                } else { // NOT GET
                    if (request.getMethod().equals(Config.PUT)) {
                        String resposta;
                        if (split.length > 3) {

                        }
                    } else {
                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                    }
                    response.send(outS);
                    // stack push 1 addas

                }
            } catch (IOException ex) {
                System.out.println("Thread error when reading request");
            }
            try {
                sock.close();
            } catch (IOException ex) {
                System.out.println("CLOSE IOException");
            }
        }
    }
}
