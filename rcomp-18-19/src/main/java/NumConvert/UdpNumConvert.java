package NumConvert;

import core.Application;
import core.Config;
import core.HTTPmessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashSet;
import java.util.Scanner;

@SuppressWarnings("ALL")
public class UdpNumConvert {

    public static Scanner scanner = new Scanner(System.in);
    public static DatagramSocket udpSock;
    private static InetAddress address;
    public static HashSet<Application> peersList = new HashSet<>();
    public static ServerSocket svSock;

    public static synchronized void addIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        peersList.add(a);
    }

    public static synchronized void remIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        peersList.remove(a);
    }

    public static synchronized void printIPs() {
        for (Application a : peersList) {
            System.out.print(" " + a.getName() + " ip: " + a.getIp());
        }
    }

    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception {
        for (Application a : peersList) {
            p.setAddress(a.getIp());
            s.send(p);
        }
    }

    public static synchronized Application checkApplicationName(String s) {
        for (Application a : peersList) {
            if (a.getName().equals(s.trim())) {
                return a;
            }
        }
        return null;
    }

    public static void sendPacketToApplication(DatagramSocket s, DatagramPacket p, Application param) throws IOException {
        p.setAddress(param.getIp());
        s.send(p);
    }

    private static boolean checkApplication(String s) {
        if (s.equals(Config.NUM_CONVERT_NAME)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
        byte[] data;
        DatagramPacket udpPacket;
        // CONNECTION
        try {
            udpSock = new DatagramSocket(Config.SERVICE_PORT);
            udpSock.setBroadcast(true);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }
        HTTPNumConvert httpServer = new HTTPNumConvert();
        svSock = httpServer.runHTTP(udpSock);
        address = InetAddress.getByName(Config.BCAST_ADDR);

        // Sinalização de inicio
        String sinalMsg = Config.NUM_CONVERT_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG;
        data = (sinalMsg).getBytes();
        udpPacket = new DatagramPacket(data, data.length, address, Config.SERVICE_PORT);
        udpSock.send(udpPacket);

        //Thread pra receber mensagens
        Thread udpReceiver = new Thread(new UdpNumConvertReceive(udpSock));
        udpReceiver.start();
        //Thread pra mandar lista de aplicações available
        while (true) { //handle user inputs
            data = new byte[500];
            System.out.println("Insert one: \nApplication Name\nStatus\nExit");
            String param = scanner.nextLine();
            switch (param.toUpperCase()) {
                case Config.NUM_CONVERT_NAME:
                    String function;
                    int flag = -1;
                    do {
                        System.out.println("Choose: \nConvert \nList \n");
                        function = scanner.nextLine();
                        if (function.equalsIgnoreCase(Config.CONVERT_OPTION)) {//Convert
                            System.out.println(convert());
                            flag = 1;
                        } else if (function.equalsIgnoreCase(Config.LIST_NUM_OPTION)) {//List
                            System.out.println(list());
                            flag = 1;
                        } else {
                            System.out.println("Invalid Option \n");
                        }
                    } while (flag == -1);
                    break;
                case Config.EXIT_OPTION:
                    // ANNOUNCE LEAVING
                    data = (Config.STACK_APP_NAME + Config.EXIT_MSG).getBytes();
                    udpPacket.setData(data);
                    udpPacket.setLength(data.length);
                    sendToAll(udpSock, udpPacket);
                    udpSock.close();
                    udpReceiver.interrupt(); // wait for the UdpNumConvertReceive thread to end
                    System.exit(0);
                    break;
                case Config.STATUS_OPTION:
                    System.out.print("Active peers:");
                    printIPs();
                    System.out.println();
                    break;
                default:
                    Application a = checkApplicationName(param);
                    if (a == null) {
                        System.out.println("Invalid parameter");
                    } else {
                        String otherAppParam = "", temp;
                        do {
                            System.out.println("Insert parameter: (insert \"z\" to exit)");
                            temp = scanner.nextLine();
                            if (!temp.equals("z")) {
                                if(otherAppParam.isEmpty()){ //Primeira Vez
                                    otherAppParam=temp;
                                }else {
                                    otherAppParam = otherAppParam + Config.SEPARATOR + temp;
                                }
                            } else {
                                break;
                            }
                        } while (true);
                        String rep = (a.getName()+ Config.SEPARATOR +otherAppParam).trim();
                        byte[] dataSend = rep.getBytes();
                        DatagramPacket udpPacket2 = new DatagramPacket(dataSend, dataSend.length,address,Config.SERVICE_PORT);
                        sendPacketToApplication(udpSock, udpPacket2, a);
                    }
                    break;

            }
        }
    }

    /**
     * Given a representation identifier for input (IR), another representation identifier for the  output  (OR),
     * and  a  number  representation,  parse  the  provided  number  representation  as being IR and
     * return back as response the same number in OR representation.
     * If parsing fails, a suitable error text should be returned instead.
     */
    private static String convert() {
        System.out.println("Insert Input Type: ");
        String inputType = scanner.nextLine();
        System.out.println("Insert Output Type: ");
        String outputType = scanner.nextLine();
        System.out.println("Insert Number: ");
        String number = scanner.nextLine();
        String convertedNumber = new NumConverter(inputType, outputType, number).numConvert();
        return convertedNumber;
    }

    /**
     * Returns,as response,the list of identifiers for supported representations.
     */
    private static String list() {
        return NumConverter.listPossibleTypes();
    }
}

class UdpNumConvertReceive implements Runnable {

    private DatagramSocket sock;

    public UdpNumConvertReceive(DatagramSocket sock) throws SocketException {
        this.sock = sock;
        sock.setSoTimeout(Config.TIMEOUT);
    }

    @Override
    public void run() {
        boolean maintenance = false;
        int i;
        byte[] data = new byte[500];
        String lastMessage = "lastCreated";
        String frase, appName, function;
        DatagramPacket p;
        InetAddress currPeerAddress;
        InetAddress lastPeerAddress = null;
        p = new DatagramPacket(data, data.length);
        while (true) {
            data = new byte[500];
            p.setLength(data.length);
            try {
                sock.receive(p);
            } catch (SocketTimeoutException ex) {
                Thread.currentThread().interrupt(); // TIMEOUT
            } catch (IOException e) {
                Thread.currentThread().interrupt();
            }
            currPeerAddress = p.getAddress();
            frase = new String(p.getData(), 0, p.getLength()); // frase
            String[] paramList = (frase.trim()).split(Config.SEPARATOR);
            if (paramList.length >= 2) { // PARAMETRO 0: AppName PARAMETRO 1: Function/sysMsg PARAMETRO 2 : (...)
                appName = paramList[0];
                function = paramList[1];
                try {
                    if (p.getPort() == Config.SERVICE_PORT && !p.getAddress().equals(InetAddress.getLocalHost())) { //Sistema de manutenção
                  //  System.out.println(frase);
                        if (p.getAddress().equals(InetAddress.getLocalHost())) {
                        }
                        DatagramPacket udpPacket;
                        byte[] dataSend;
                        switch (function) {
                            case Config.ARRIVAL_MSG:
                                //Reencaminhar
                                if(UdpNumConvert.checkApplicationName(appName)==null) { //if app is already received
                                    UdpNumConvert.addIP(appName, currPeerAddress);
                                    System.out.println(appName +" is online");
                                    String answerMsg = (Config.NUM_CONVERT_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG).trim();
                                    byte[] answerBytes = answerMsg.getBytes();
                                    DatagramPacket packTemp = new DatagramPacket(answerBytes, answerBytes.length, currPeerAddress, Config.SERVICE_PORT);
                                    sock.send(packTemp);
                                }
                                break;
                            case Config.EXIT_MSG:
                                UdpNumConvert.remIP(appName, p.getAddress());
                                System.out.println(appName +Config.SEPARATOR+ Config.EXIT_MSG);
                                break;
                            case Config.LIST_NUM_OPTION:
                                System.out.println("list request");
                                String listPossibleTypes = NumConverter.listPossibleTypes();
                                String out = Config.NUM_CONVERT_NAME + Config.SEPARATOR+Config.RESPONSE_MESSAGE+Config.SEPARATOR+listPossibleTypes;
                                dataSend = out.trim().getBytes();
                                udpPacket = new DatagramPacket(dataSend, dataSend.length, currPeerAddress, Config.SERVICE_PORT);
                                sock.send(udpPacket);
                                break;
                            case Config.CONVERT_OPTION:
                                System.out.println("convert request");
                                if (paramList.length == 5) {
                                    String result = new NumConverter(paramList[2], paramList[3], paramList[4]).numConvert();
                                    String out2 = Config.NUM_CONVERT_NAME + Config.SEPARATOR+Config.RESPONSE_MESSAGE+Config.SEPARATOR+result;
                                    dataSend = out2.trim().getBytes();
                                    System.out.println(new String(dataSend));
                                    udpPacket = new DatagramPacket(dataSend, dataSend.length, currPeerAddress, Config.SERVICE_PORT);
                                    sock.send(udpPacket);
                                    System.out.println("Packet sent "+out2);
                                } else {
                                    String error = "parameters number invalid";
                                    dataSend = error.trim().getBytes();
                                    udpPacket = new DatagramPacket(dataSend, dataSend.length, currPeerAddress, Config.SERVICE_PORT);
                                    sock.send(udpPacket);
                                }
                                break;
                            case Config.RESPONSE_MESSAGE:
                                System.out.println("resposta");
                                String str = "response#<p>";
                                for (int k = 0; k < paramList.length; k++) {
                                    str = str + paramList[k];
                                }
                                str = str + "</p>";
                                HTTPmessage response = new HTTPmessage();
                                Socket s = UdpNumConvert.svSock.accept();
                                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                                response.setContentFromString(str, "text/html");
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                response.send(dos);

                                break;
                            default:
                                System.out.println("default" +new String(p.getData()));
                                Application app = UdpNumConvert.checkApplicationName(appName); //Validar appName
                                if (app != null) {
                                    UdpNumConvert.sendPacketToApplication(sock, p, app);
                                }
                                break;
                        }
                        p.setPort(1);
                    }
                } catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

}
