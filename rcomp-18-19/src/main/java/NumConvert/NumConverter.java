package NumConvert;

public class NumConverter {

    private enum numTypes{
        BIN,
        OCT,
        DEC,
        HEX
    };
    private String inputType;
    private String outputType;
    private String number;
    
    public NumConverter(String inputType, String outputType, String number){
        this.inputType=inputType;
        this.outputType=outputType;
        this.number=number;
    }

    /**
     *  Converts the parameter number, of type inputType, into a number of type outputType
     * @return The number converted
     */
    public String numConvert(){
        String tempoutputType;
        int temp;
        try {
            switch (inputType) {
                case "BIN":
                    temp = Integer.parseInt(number, 2);
                    break;
                case "OCT":
                    temp = Integer.parseInt(number, 8);
                    break;
                case "DEC":
                    temp = Integer.parseInt(number);
                    break;
                case "HEX":
                    temp = Integer.parseInt(number, 16);
                    break;
                default:
                    return "Input type Inválido";
            }
            switch (outputType) {
                case "BIN":
                    tempoutputType = Integer.toBinaryString(temp);
                    break;
                case "OCT":
                    tempoutputType = Integer.toOctalString(temp);
                    break;
                case "DEC":
                    tempoutputType = String.format("%d", temp);
                    break;
                case "HEX":
                    tempoutputType = Integer.toHexString(temp);
                    break;
                default:
                    return "output type Inválido";

            }
        }catch(Exception e){
            return "Número inserido inválido";
        }
        return tempoutputType;
    }

    /**
     * Lists all possible Number Types
     * @return a string with all possible number types
     */
    public static String listPossibleTypes(){
        String out  = String.format("%s ; %s ; %s ; %s .", numTypes.BIN.toString(), numTypes.OCT.toString(), numTypes.DEC.toString(), numTypes.HEX.toString());
        return out;
    }
}
