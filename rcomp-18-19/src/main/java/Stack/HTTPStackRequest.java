package Stack;

import core.Config;
import core.HTTPmessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

@SuppressWarnings("Duplicates")
public class HTTPStackRequest extends Thread {

    String baseFolder;
    Socket sock;
    ServerSocket svSock;
    DatagramSocket s;
    DatagramPacket p;
    DataInputStream inS;
    DataOutputStream outS;

    public HTTPStackRequest(ServerSocket cliSock, DatagramSocket s, DatagramPacket p, String baseFolder) {
        this.baseFolder = baseFolder;
        this.svSock = cliSock;
        this.s = s;
        this.p = p;
    }

    public void run() {
        while (true) {
            try {
                sock = svSock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outS = new DataOutputStream(sock.getOutputStream());
                inS = new DataInputStream(sock.getInputStream());
            } catch (IOException ex) {
                System.out.println("Thread error on data streams creation");
            }
            try {
                HTTPmessage request = new HTTPmessage(inS);
                HTTPmessage response = new HTTPmessage();
                if (request.getMethod().equals(Config.GET)) { // PEDIDOS GET
                    if (request.getURI().equals(Config.STACKS_REQUEST_NAME)) { // SE FOR REQUEST STACKS IMPRIME LIST DE STACKS HTML
                        response.setContentFromString(UdpStackReceiver.getStacksHTML(), "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else if (request.getURI().equals(Config.STACK_APPLICATION_REQUEST)) { // REFRESH AUTOMATICO DE APLICA��ES LIGADAS
                        response.setContentFromString(UDPStackFunction.printIpsHtml(), "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else if (request.getURI().contains("otherApps")) { // PEDIDOS PARA OUTRAS APLICA��ES
                        String cmd = request.getURI().substring(11);
                        String resposta[] = cmd.split("%20"); // ESPA�O
                        String appName = resposta[0].trim().toUpperCase();
                        String rep = appName;
                        for (int j = 1; j < resposta.length; j++) { // PARAMETROS
                            rep = rep + Config.SEPARATOR + resposta[j];
                        }
                        UDPStackFunction.sendPacket(rep, s, p); // manda o packet para o server
                        Thread.currentThread().sleep(300);
                        // BUSCAR A ULTIMA MENSAGEM RECEBIDA(RESPOSTA)
                        response.setContentFromString("<p>" + UdpStackReceiver.getLastMessage() + "</p>", "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else { // CRIA��O HTML SITE
                        String fullname = baseFolder + "/";
                        if (request.getURI().equals("/")) {
                            fullname = fullname + "index.html";
                        } else {
                            fullname = fullname + request.getURI();
                        }
                        if (response.setContentFromFile(fullname)) {
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else {
                            response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                    }
                    response.send(outS); // MANDA RESPOSTA P HTML
                } else { // NOT GET
                    if (request.getMethod().equals(Config.PUT)) {
                        String resposta;
                        String split[] = request.getURI().split(Config.SEPARATOR);
                        if (split.length > 3) { // blank space / stacks / instuction / id / elemento(push ONLY)
                            if (split[2].trim().equalsIgnoreCase("Push")) { // push
                                resposta = Config.STACK_APP_NAME + Config.SEPARATOR + split[2].trim() + Config.SEPARATOR + split[3].trim() + Config.SEPARATOR + split[4].trim();
                                UDPStackFunction.sendPacket(resposta, s, p); // manda o packet para o server
                                Thread.currentThread().sleep(500);
                                response.setContentFromString("<p>" + UdpStackReceiver.checkSize(split[3].trim()) + "</p>", "text/html"); // resposta
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                            } else if (split[2].trim().equalsIgnoreCase("Pop")) {
                                try {
                                    resposta = Config.STACK_APP_NAME + Config.SEPARATOR + split[2].trim() + Config.SEPARATOR + split[3].trim();
                                    UDPStackFunction.sendPacket(resposta, s, p); // manda o packet para o server
                                    Thread.currentThread().sleep(500);
                                    response.setContentFromString("<p>Message popped!</p>", "text/html"); // resposta
                                    response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                } catch (IllegalArgumentException ex) {
                                    response.setContentFromString(Config.STACK_NOT_FOUND, "text/html"); // erro
                                    response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                }
                            }
                        }
                    } else {
                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                    }
                    response.send(outS);
                }
            } catch (IOException | InterruptedException ex) {
                System.out.println("Thread error when reading request");
            }
            try {
                sock.close();
            } catch (IOException ex) {
                System.out.println("CLOSE IOException");
            }
        }

    }
}
