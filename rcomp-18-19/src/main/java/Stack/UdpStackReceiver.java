package Stack;

import core.Application;
import core.Config;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class UdpStackReceiver implements Runnable {

    private DatagramSocket s;
    static List<Stack> stacks = new ArrayList<>();
    private static String lastMessage;

    public UdpStackReceiver(DatagramSocket udp_s) throws SocketException {
        this.s = udp_s;
        udp_s.setSoTimeout(Config.TIMEOUT * 1000);
    }

    @Override
    public void run() {
        byte[] data = new byte[300];
        DatagramPacket p;
        InetAddress currPeerAddress;
        p = new DatagramPacket(data, data.length);
        String lastFunction = "";
        String function = "_";
        String appName = "_";

        // LOOP RECEIVER
        while (true) {
            data = new byte[300];
            p.setData(data, 0, data.length);
            try {
                s.receive(p);
            } catch (SocketTimeoutException ex) {
                Thread.currentThread().interrupt(); // TIMEOUT
            } catch (IOException e) {
                Thread.currentThread().interrupt();
            }
            data = p.getData();
            String frase = new String(data); // frase
            System.out.println(frase);
            String[] test = frase.split(Config.SEPARATOR); // PARAMETRO 0: AppName PARAMETRO 1: Function
            System.out.println(frase);
            if (test.length > 1) {
                function = test[1].trim();
                appName = test[0].trim();
            }
            if (p.getPort() == Config.SERVICE_PORT) { // Avoid spamming
                currPeerAddress = p.getAddress();
                if (appName.equals(Config.STACK_APP_NAME.trim())) {
                    if (function.contains(Config.PUSH_MSG)) {
                        try {
                            Stack st = checkStack(test[2].trim());
                            frase = "Stack size " + pushStack(st, test[3].trim());
                        } catch (IllegalArgumentException ex) {
                            frase = ex.getMessage(); // if size is bigger than 80
                        }
                        System.out.println("Stack size: " + frase);
                        // RESPONDER
                        frase = Config.STACK_APP_NAME + Config.SEPARATOR + Config.RESPONSE_MESSAGE + Config.SEPARATOR + frase; // adicionar flag de resposta
                        UDPStackFunction.sendPacket(frase, s, p);
                    } else if (function.contains(Config.POP_MSG)) {
                        try {
                            Stack st = checkStack(test[2].trim().substring(0, 1));
                            st.pop();
                        } catch (IllegalArgumentException ex) {
                            frase = ex.getMessage(); // if size is bigger than 80
                        }
                        System.out.println(frase);
                        // RESPONDER
                        frase = Config.STACK_APP_NAME + Config.SEPARATOR + Config.RESPONSE_MESSAGE + Config.SEPARATOR + frase; // adicionar flag de resposta
                        UDPStackFunction.sendPacket(frase, s, p);
                    } else if (function.contains(Config.LIST_STACKS_OPTION)) {
                        frase = "Stacks\n";
                        for (Stack s : stacks) {
                            frase = frase + "\n" + "Stack " + s.id() + " size " + s.size();
                        }
                        System.out.println(frase);
                        // RESPONDER
                        frase = Config.STACK_APP_NAME + Config.SEPARATOR + Config.RESPONSE_MESSAGE + Config.SEPARATOR + frase; // adicionar flag de resposta
                        UDPStackFunction.sendPacket(frase, s, p);
                    } else if (function.contains(Config.ARRIVAL_MSG)) {
                        UDPStackFunction.addIP(appName, p.getAddress());
                    }
                } else { // APPLICATION NOT OURS
                    Application a = UDPStackFunction.checkApplicationName(appName.toUpperCase()); // CHECK IF APPLICATION REGISTERED
                    if (a == null) { // IF NOT REGISTERED, REGIST IT
                        if (UDPStackFunction.addIP(appName, p.getAddress())) {
                            System.out.println("APP: " + appName + " created");
                            frase = Config.STACK_APP_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG;
                            UDPStackFunction.sendPacket(frase, s, p);
                        }
                    } else {
                        if (function.contains(Config.EXIT_MSG)) { // EXIT OR MAINTENANCE - CORE FUNCTIONS
                            System.out.println(appName + " has closed!");
                            UDPStackFunction.remIP(appName, p.getAddress());
                        } else if (function.contains(Config.MAINTENANCE_MSG)) {
                            System.out.println(appName + " has poked you!");
                        } else if (function.contains(Config.RESPONSE_MESSAGE)) { // OTHER APPLICATION RESPONSE
                            lastMessage = "Response " + new String(p.getData());
                            System.out.println(lastMessage);
                        } else if (function.contains(Config.ARRIVAL_MSG)) {
                            if (UDPStackFunction.addIP(appName, p.getAddress())) {
                                System.out.println(appName + " created");
                            }
                        }
                    }
                }
                p.setPort(1);
            }
        }
    }

    /**
     * Pops the element from the stack passed by parameter.
     *
     * @param s stack
     * @return string
     */
    private static synchronized String popStack(Stack s) {
        return s.pop();
    }

    /**
     * Pushes the element passed by parameter to the stack and returns its size.
     *
     * @param stack stack
     * @param s1    element
     * @return size
     */
    private static synchronized String pushStack(Stack stack, String s1) {
        stack.push(s1);
        return stack.size().toString();
    }

    /**
     * Checks if stack already exists(through the id) - throws illegal argument
     * exception if not
     *
     * @param id id
     * @return stack
     */
    private static synchronized Stack checkStack(String id) {
        if (id.getBytes().length > 80) {
            throw new IllegalArgumentException("ID error");
        }
        for (Stack s : stacks) {
            if (s.id().equals(id)) {
                return s;
            }
        }
        Stack s = new Stack(id);
        stacks.add(s);
        return s;
    }

    /**
     * Retrieves the html text with all the stacks - listed.
     *
     * @return stacks
     */
    public static synchronized String getStacksHTML() {
        String textHtml = "<ul>";
        for (Stack s : stacks) {
            textHtml = textHtml + "<li>Stack id: " + s.id() + " size: " + s.size() + " messages: " + s.messages() + "</li>";
        }
        textHtml = textHtml + "</ul>";
        return textHtml;
    }

    /**
     * Retrieves stack size
     *
     * @param id id
     * @return size
     */
    public static synchronized String checkSize(String id) {
        for (Stack s : stacks) {
            if (s.id().equals(id)) {
                return "Stack size: " + s.size();
            }
        }
        return "No stack";
    }

    public static synchronized String getLastMessage() {
        return lastMessage;
    }
}
