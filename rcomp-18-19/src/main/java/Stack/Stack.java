package Stack;

import java.util.Objects;

public class Stack {

    private java.util.Stack<String> s;

    private String index;

    public Stack(String ind) {
        s = new java.util.Stack<String>();
        index = ind;
    }

    public String pop() {
        if (s.size() > 0) {
            return s.pop();
        }
        return "Empty stack";
    }

    public String push(String str) {
        return s.push(str);

    }

    public String id() {
        return index;
    }


    public String size() {
        return Integer.toString(s.size());
    }

    public String messages() {
        String s ="";
        int size = Integer.parseInt(this.size());
        for (int i = 0; i < size; i++) {
            s = s + this.s.get(i) +";";
        }
        return s;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "s=" + s +
                ", index='" + index + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stack stack = (Stack) o;
        return Objects.equals(index, stack.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }
}
