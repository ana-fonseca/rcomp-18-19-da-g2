package TCPMonitor;

import Core.Application;
import Core.Config;
import Core.HTTPmessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

@SuppressWarnings("Duplicates")
public class HTTP_TCP_MonitorRequest extends Thread {

    String baseFolder;
    ServerSocket serverSock;
    Socket sock;
    DatagramSocket dgSock;
    DataInputStream inS;
    DataOutputStream outS;

    public HTTP_TCP_MonitorRequest(ServerSocket cliSock, DatagramSocket dg, String baseFolder) {
        this.baseFolder = baseFolder;
        this.serverSock = cliSock;
        this.dgSock = dg;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sock = serverSock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outS = new DataOutputStream(sock.getOutputStream());
                inS = new DataInputStream(sock.getInputStream());
            } catch (IOException ex) {
                System.out.println("Thread error on data streams creation");
            }
            try {
                String lastURI = "";
                HTTPmessage request = new HTTPmessage(inS);
                HTTPmessage response = new HTTPmessage();
                //System.out.println(request.getURI());
                //System.out.println(request.getMethod());
                String split[] = (request.getURI().trim()).split(Config.SEPARATOR);
                if (!lastURI.equalsIgnoreCase(request.getURI())) {
                    if (request.getMethod().equals(Config.GET)) {
                        if (split.length == 0) {
                            String fullname = baseFolder + "/";
                            if (request.getURI().equals("/")) {
                                fullname = fullname + "index.html";
                            } else {
                                fullname = fullname + request.getURI();
                            }
                            if (response.setContentFromFile(fullname)) {
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                            } else {
                                response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                            }
                        } else if (split[1].equalsIgnoreCase("rcomp-http.js")) {
                            String fullname = baseFolder + "/";
                            if (request.getURI().equals("/")) {
                                fullname = fullname + "index.html";
                            } else {
                                fullname = fullname + request.getURI();
                            }
                            if (response.setContentFromFile(fullname)) {
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                            } else {
                                response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                            }
                        } else if (split[1].equals((Config.TCP_MONITOR_REQUEST_NAME.substring(1)))) {
                            if (split.length > 2) {
                                try {
                                    String result;
                                    switch (split[2].trim().toUpperCase()) {
                                        case Config.ADD_SERVICE_OPTION:
                                            if (split.length > 5) {
                                                result = HTTP_TCP_Monitor.addService(split[3].trim(), split[4].trim(), split[5].trim());
                                                response.setContentFromString("<p> " + result + "</p>", "text/html"); //resposta
                                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                            } else {
                                                System.out.println("erro add");
                                                response.setContentFromString(Config.TCP_SERVICE_NOT_ADDED, "text/html");
                                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                            }
                                            break;
                                        case Config.REMOVE_SERVICE_OPTION:
                                            if (split.length > 2) {
                                                result = HTTP_TCP_Monitor.removeService(split[3].trim());
                                                response.setContentFromString("<p> " + result + "</p>", "text/html"); //resposta
                                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                            } else {
                                                System.out.println("erro remove");
                                                response.setContentFromString(Config.SERVICE_NOT_FOUND, "text/html");
                                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                            }
                                            break;
                                        case Config.STATUS_OPTION:
                                            result = HTTP_TCP_Monitor.getServices();
                                            response.setContentFromString(result, "text/html"); //resposta
                                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                            break;
                                        case "APPLICATION":
                                            String s = "";
                                            HashMap<String, Integer> tempMap = new HashMap<>();
                                            for (Application a : TCPMonitorMain.getAvailableApps()) {
                                                if (tempMap.containsKey(a.getName())) {
                                                    tempMap.put(a.getName(), tempMap.get(a.getName() + 1));
                                                } else {
                                                    tempMap.put(a.getName(), 1);
                                                }
                                            }
                                            for (String str2 : tempMap.keySet()) {
                                                s = s + "<tr> <td>" + str2 + "</td> <td>" + tempMap.get(str2) + "</td> </tr>";
                                            }
                                            response.setContentFromString(s, "text/html"); //resposta
                                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                                            break;
                                        case "RESPONSE":
                                            System.out.println(request.getURI());
                                        default:
                                            System.out.println("default");
                                            response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                            break;
                                    }
                                } catch (IllegalArgumentException ex) {
                                    System.out.println("illegal arg exp");
                                    response.setContentFromString("ERROR", "text/html"); // erro
                                    response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                                }
                            }
                        } else if (TCPMonitorMain.checkApplicationName(split[1]) != null) {
                            // System.out.println("tou");
                            Application app = TCPMonitorMain.checkApplicationName(split[1]);
                            String msgOut = ((request.getURI().trim().replace(" ", Config.SEPARATOR)).trim()).substring(1);
                            //System.out.println(msgOut);
                            byte[] arrayByte = msgOut.getBytes();
                            DatagramPacket dgp = new DatagramPacket(arrayByte, arrayByte.length);
                            dgp.setAddress(app.getIp());
                            dgp.setPort(Config.SERVICE_PORT);
                            dgSock.send(dgp);
                        } else {
                            response.setContentFromString("Application not available", "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                        response.send(outS);
                    } else { // NOT GET
                        if (request.getMethod().equals(Config.PUT)) {
                            String resposta;
                            if (split.length > 3) {

                            }
                        } else {
                            response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                        response.send(outS);
                    }
                }
            } catch (IOException ex) {
                System.out.println("Thread error when reading request");
            }
            try {
                sock.close();
            } catch (IOException ex) {
                System.out.println("CLOSE IOException");
            }
        }
    }
}
