package TCPMonitor;

import core.Application;
import core.Config;
import core.DrawMenu;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Scanner;

@SuppressWarnings("Duplicates")
public class TCPMonitorMain {

    private static HashSet<Application> availableApps = new HashSet<>();

    private static TCP_ServiceList availableServices = new TCP_ServiceList();

    static InetAddress address;

    static DatagramSocket sock;

    public static ServerSocket svSock;

    public static Scanner in = new Scanner(System.in);

    public static synchronized void addApp(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        availableApps.add(a);
    }

    public static synchronized void removeApp(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        availableApps.remove(a);
    }

    public static synchronized void printIPs() {
        for (Application a : availableApps) {
            System.out.print(" " + a.getName() + " ip: " + a.getIp());
        }
    }

    public static InetAddress getIpByName(String appName){
        for (Application app:availableApps){
            if (app.getName().equalsIgnoreCase(appName)){
                return app.getIp();
            }
        }
        return null;
    }

    public static boolean checkApplication(String name){
        for (Application app:availableApps){
            if (app.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public static synchronized Application checkApplicationName(String s) {
        for (Application a : availableApps) {
            if (a.getName().equalsIgnoreCase(s.trim())) {
                return a;
            }
        }
        return null;
    }

    public static void main(String args[]) throws Exception {
        byte[] data;
        DatagramPacket udpPacket;
        // CONNECTION
        try {
            sock = new DatagramSocket(Config.SERVICE_PORT);
            sock.setBroadcast(true);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }
        HTTP_TCP_Monitor httpServer = new HTTP_TCP_Monitor();
        svSock = httpServer.runHTTP(sock);
        address = InetAddress.getByName(Config.BCAST_ADDR);

        // Sinalização de inicio
        String sinalMsg = Config.TCP_MONITOR_APP_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG;
        data = (sinalMsg).getBytes();
        udpPacket = new DatagramPacket(data, data.length, address, Config.SERVICE_PORT);
        sock.send(udpPacket);

        //Thread pra receber mensagens
        Thread udpReceiver = new Thread(new TCPMonitorReceiver(sock));
        udpReceiver.start();

        //SERVICE CHECK
        Thread serviceCheck=new Thread(new TCP_MonitorServiceCheck());
        serviceCheck.start();

        //LOCAL USER INPUTS
        while (true) {
            Thread.sleep(1000);
            System.out.println(DrawMenu.drawHeadlineTCPMonitor());
            System.out.println(DrawMenu.drawMenuTCPMonitor());
            String input=in.nextLine();
            switch(input){
                case "EXIT":
                    // ANNOUNCE LEAVING
                    udpReceiver.interrupt();
                    data = (Config.TCP_MONITOR_APP_NAME + "/" + Config.EXIT_MSG).getBytes();
                    udpPacket.setData(data);
                    udpPacket.setLength(data.length);
                    udpPacket.setAddress(InetAddress.getByName(Config.BCAST_ADDR));
                    sock.send(udpPacket);
                    // CLOSE SOCKET
                    sock.close();
                    System.exit(0);
                case "STATUS":
                    System.out.print("Active peers:");
                    printIPs();
                    System.out.println();
                    break;
                case "TCP_MONITOR":
                    System.out.println(DrawMenu.drawMenuTcpMonitorOptions());
                    String option = in.nextLine();
                    option = Config.TCP_MONITOR_APP_NAME + "/" + option;
                    data = option.getBytes();
                    udpPacket.setData(data);
                    udpPacket.setLength(data.length);
                    udpPacket.setAddress(getIpByName(Config.TCP_MONITOR_APP_NAME.trim()));
                    sock.send(udpPacket);
                    break;
                case "OTHER APP":
                    System.out.println("INTRODUCE THE APPLICATION NAME AND THE COMMAND WITH THE RESPECTIVE PARAMETERS (*APP NAME* *Command* *Parameter1* *Parameter2* ...):");
                    input=in.nextLine();
                    String[] line=input.split(" ");
                    String appName=line[0].trim();
                    udpPacket.setData(input.getBytes());
                    InetAddress destination=getIpByName(appName);
                    if (destination!=null){
                        udpPacket.setAddress(destination);
                        udpPacket.setLength(input.getBytes().length);
                        sock.send(udpPacket);
                    }else{
                        System.out.println("APPLICATION IS NOT AVAILABLE");
                    }
                    break;
            }
        }
    }

    /**
     * @return the TCP services list
     */
    public static TCP_ServiceList getTCP_ServiceList() {
        return availableServices;
    }

    public static HashSet<Application> getAvailableApps(){
        return availableApps;
    }

    public static String addService(String id, String ip, String port){
        return availableServices.addService(id,ip,port);
    }

    public static String removeService(String id){
        return availableServices.removeService(id);
    }

    public static String serviceStatus(){
        return availableServices.serviceStatus();
    }
}
