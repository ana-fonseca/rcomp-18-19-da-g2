package TCPMonitor;

import Core.Config;
import Core.HTTPmessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;

@SuppressWarnings("Duplicates")
public class TCPMonitorReceiver implements Runnable {
    private DatagramSocket s;
    private static String lastResponse;

    public static String getLastResponse() {
        return lastResponse;
    }

    public TCPMonitorReceiver(DatagramSocket udp_s) throws SocketException {
        s = udp_s;
        s.setSoTimeout(Config.TIMEOUT);
    }

    @Override
    public void run() {
        int flag = 1;
        byte[] data = new byte[500];
        String message, response = "";
        DatagramPacket p;
        InetAddress packetAddress;
        TCP_ServiceList tcpList = TCPMonitorMain.getTCP_ServiceList();
        p = new DatagramPacket(data, data.length);
        while (true) {
            data = new byte[500];
            p.setData(data, 0, data.length);
            try {
                s.receive(p);
            } catch (SocketTimeoutException ex) {
                Thread.currentThread().interrupt();
            } catch (IOException ex) {
                Thread.currentThread().interrupt();
            }
            if (p.getPort() == Config.SERVICE_PORT) {
                packetAddress = p.getAddress();
                message = new String(p.getData(), 0, p.getLength());
                String[] msg = message.split("/");
                if (msg[0].trim().equalsIgnoreCase(Config.TCP_MONITOR_APP_NAME.trim())) {//SENDING APP IS LOCAL APP
                    switch (msg[1].trim()) {
                        case "add":
                            response = Config.TCP_MONITOR_APP_NAME + "/" + Config.RESPONSE_MESSAGE + "/" + HTTP_TCP_Monitor.addService(msg[2], msg[3], msg[4]);
                            break;
                        case "remove":
                            response = Config.TCP_MONITOR_APP_NAME + "/" + Config.RESPONSE_MESSAGE + "/" + HTTP_TCP_Monitor.removeService(msg[2]);
                            break;
                        case "status":
                            response = Config.TCP_MONITOR_APP_NAME + "/" + Config.RESPONSE_MESSAGE + "/" + HTTP_TCP_Monitor.getList();
                            break;
                        case Config.RESPONSE_MESSAGE:
                            System.out.println("TCP MONITOR's response: ");
                            for (int i = 2; i < msg.length; i++) {
                                System.out.print(msg[i] + " ");
                            }
                            System.out.println();
                            flag = 1;
                            break;
                        case Config.ARRIVAL_MSG:
                            TCPMonitorMain.addApp(msg[0].trim(), packetAddress);
                            System.out.println("Application is now online.");
                            flag = 1;
                            break;
                        default:
                            flag = 1;
                    }
                    if (flag == 0) {
                        p.setAddress(packetAddress);
                        p.setData(response.getBytes());
                        p.setLength(response.getBytes().length);
                        try {
                            s.send(p);
                        } catch (IOException ex) {
                            System.out.println("Response not sent");
                        }
                    }
                    flag = 0;
                } else if (msg[1].trim().equalsIgnoreCase(Config.RESPONSE_MESSAGE)) {//RESPONSE FROM AN APP
                    String string = "response#" + msg[0] + "'s response: ";
                    for (int i = 2; i < msg.length; i++) {
                        string = string + " " + msg[i];
                    }
                    try {
                        HTTPmessage res = new HTTPmessage();
                        Socket s = TCPMonitorMain.svSock.accept();
                        DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                        res.setContentFromString(string, "text/html");
                        res.setResponseStatus(Config.POSITIVE_RESPONSE);
                        res.send(dos);
                    } catch (Exception ex) {
                        System.out.println("ERROR");
                    }
                } else if (msg[1].trim().equalsIgnoreCase(Config.ARRIVAL_MSG)) { //APP ARRIVED
                    if (!TCPMonitorMain.checkApplication(msg[0].trim())) {
                        TCPMonitorMain.addApp(msg[0].trim(), packetAddress);
                        data = (Config.TCP_MONITOR_APP_NAME + "/" + Config.ARRIVAL_MSG).getBytes();
                        p.setData(data);
                        p.setLength(data.length);
                        p.setAddress(packetAddress);
                        try {
                            s.send(p);
                        } catch (IOException ex) {
                            System.out.println("Reply ping not sent.");
                        }
                        System.out.println(msg[0].trim() + " is now online.");
                    }
                } else if (msg[1].trim().equalsIgnoreCase(Config.EXIT_MSG)) {//APP EXITED
                    TCPMonitorMain.removeApp(msg[0].trim(), packetAddress);
                    System.out.println(msg[0].trim() + " is now offline.");
                }
                p.setPort(1);
            }
        }
    }
}
