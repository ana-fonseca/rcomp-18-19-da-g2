package TCPMonitor;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TCP_MonitorServiceCheck implements Runnable{

    public TCP_MonitorServiceCheck() {

    }

    @Override
    public void run() {

        while (true) {
            List<TCP_Service> lista = HTTP_TCP_Monitor.getList().getList();

            for (int i = 0; i < lista.size(); i++) {
                TCP_Service srv = lista.get(i);
                InetAddress serverIP = srv.getIpAddress();
                Socket sock = new Socket();
                try {
                    sock.connect(new InetSocketAddress(serverIP, srv.getPort()), 500);
                    lista.get(i).setStatus("available");
                } catch (Exception ex) {
                    lista.get(i).setStatus("unavailable");
                }
            }

            try {
                Thread.sleep(60000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TCP_MonitorServiceCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
