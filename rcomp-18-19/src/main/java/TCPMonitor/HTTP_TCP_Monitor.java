package TCPMonitor;

import Core.Config;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Based in PL21-Rcomp
 */
@SuppressWarnings("Duplicates")
public class HTTP_TCP_Monitor {

    static private final String BASE_FOLDER = "www/TCP_Monitor";
    static private ServerSocket sock;

    public ServerSocket runHTTP(DatagramSocket s) throws IOException {
        Socket cliSock;

        // OPEN SOCKET
        try {
            sock = new ServerSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port 30503");
            System.exit(1);
        }
        HTTP_TCP_MonitorRequest req = new HTTP_TCP_MonitorRequest(sock,s, BASE_FOLDER);
        req.start();
        return sock;
    }

    private static TCP_ServiceList list=new TCP_ServiceList();

    /**
     * Retrieves the html text with all the services - listed.
     * @return services
     */
    public static synchronized String getServices() {
        String textHtml = "";
        for (TCP_Service s : list.getList()) {
            textHtml = textHtml + "<tr><td>" + s.getId() +"</td><td>"  + s.getIpAddress() + "</td><td>" + s.getPort()+"</td><td>" +  s.getStatus() + "</td></tr>";
        }
        textHtml = textHtml + "</ul>";
        return textHtml;
    }
    /**
     * removes the service passed by parameter.
     *
     * @param s service
     * @return string
     */
    public static synchronized String removeService(String s) throws IllegalArgumentException{
        String response=list.removeService(s);
        if (!response.equalsIgnoreCase("Service Removed")){
            throw new IllegalArgumentException();
        }
        return response;
    }

    /**
     * Adds a service to the list
     * @param id service id
     * @param ip service ip
     * @param port service port
     * @return result of the operation
     */
    public static synchronized String addService(String id, String ip, String port) throws IllegalArgumentException{
        String response=list.addService(id,ip,port);
        if (!response.equalsIgnoreCase("Service Added")){
            throw new IllegalArgumentException();
        }
        return response;
    }

    public static TCP_ServiceList getList(){
        return list;
    }
}
