package TCPMonitor;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class TCP_ServiceList {

    private List<TCP_Service> list;

    public TCP_ServiceList() {
        this.list = new ArrayList<>();
    }

    public List<TCP_Service> getList() {
        return this.list;
    }

    public String removeService(String id) {
        String response = "Service doesn't exist";
        for (int i =0;i<4;i++) {
            try {
                for (TCP_Service srv : list) {
                    if (srv.getId().equals(id)) {
                        list.remove(srv);
                        return "Service Removed";
                    }
                }
            } catch (ConcurrentModificationException ex) {
                response = "Service Not Removed. Service may be in use. Try again later.";
            }
        }
        return response;
    }

    public String addService(String id, String ip, String port) {
        boolean result = true;
        TCP_Service service = null;
        try {
            service = new TCP_Service(id, ip, port);
        } catch (UnknownHostException ex) {
            result = false;
        }
        try {
            if (result) {
                result = list.add(service);
            }
        } catch (ConcurrentModificationException ex) {
            result = false;
        }
        if (result) {
            return "Service Added";
        } else {
            return "Service was not added. Service's list may be in use. Try again later.";
        }
    }

    public String serviceStatus() {
        String response = "";
        try {
            if (list.isEmpty()) {
                return "No available services.";
            }
            for (TCP_Service srv : list) {
                response = response + srv.getId() + ": " + srv.getStatus() + "\n";
            }
        } catch (ConcurrentModificationException ex) {
            response = "List currently unavailable. Try again later.";
        }
        return response;
    }
}
