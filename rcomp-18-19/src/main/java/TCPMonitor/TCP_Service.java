package TCPMonitor;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class TCP_Service {

    private String id;
    private InetAddress ip;
    private int port;
    private String status;

    public TCP_Service(String id, String ipAddress, String port)throws UnknownHostException{
        this.id=id;
        this.ip= InetAddress.getByName(ipAddress);
        this.port=Integer.parseInt(port);
        this.status="awaiting verification";
    }

    public InetAddress getIpAddress(){
        return this.ip;
    }

    public void setStatus(String status){
        this.status=status;
    }

    public String getStatus(){
        return this.status;
    }

    public String getId(){
        return this.id;
    }

    public int getPort(){
        return this.port;
    }
}
