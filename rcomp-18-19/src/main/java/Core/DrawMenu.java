package core;

public class DrawMenu {

    public static String drawHeadlineStack() {
        return String.format("----- STACK APPLICATION -----\n");
    }

    public static String drawHeadlineTCPMonitor() {
        return String.format("----- TCP MONITOR -----\n");
    }
    
    public static String drawHeadlineHash() {
        return String.format("----- HASH APPLICATION -----\n");
    }

    public static String drawMenuStack() {
        return String.format("-%s\n-%s\n-%s\n-%s\n\n", Config.MAINTENANCE_OPTION, Config.STATUS_OPTION, Config.EXIT_OPTION, Config.STACK_APP_NAME);
    }
    
    public static String drawMenuHash() {
        return String.format("-%s\n-%s\n-%s\n-%s\n\n", Config.MAINTENANCE_OPTION, Config.STATUS_OPTION, Config.EXIT_OPTION, Config.HASH_NAME);
    }

    public static String drawMenuTCPMonitor() {
        return String.format("-%s\n-%s\n-%s\n-%s\n\n", Config.STATUS_OPTION, Config.EXIT_OPTION, Config.TCP_MONITOR_APP_NAME, Config.GENERIC_APPLICATION_OPTION);
    }

    public static String drawMenuStacks() {
        return String.format("-%s\n-%s\n-%s\n", Config.PUSH_OPTION, Config.POP_OPTION, Config.LIST_STACKS_OPTION);
    }
    
    public static String drawMenuHashOption() {
        return String.format("-%s\n-%s\n", Config.DIGEST_OPTION, Config.LIST_OPTION);
    }

    public static String drawMenuTcpMonitorOptions() {
        return String.format("-%s\n-%s\n-%s\n", Config.ADD_SERVICE_OPTION, Config.REMOVE_SERVICE_OPTION, Config.SERVICES_STATUS_OPTION);
    }
}
