package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Config {

    // METHODS
    public static final String GET = "GET";
    public static final String PUT = "PUT";

    // REQUEST NAMES
    public static final String STACKS_REQUEST_NAME = "/stacks";
    public static final String TCP_MONITOR_REQUEST_NAME = "/tcp_monitor";
    public static final String REQUEST_NAME_NUM_CONVERT = "/numConvert";
    public static final String HASH_REQUEST_NAME = "/hash";

    // RESPONSES
    public static final String POSITIVE_RESPONSE = "200 - Server is OK!";
    public static final String NEGATIVE_RESPONSE = "404 - Not Found";

    // HTML RESPONSES
    public static final String PAGE_NOT_FOUND = "<html><body><h1>404 File not found</h1></body></html>";
    public static final String METHOD_NOT_ALLOWED = "<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>";
    public static final String APP_TABLES = "<tr><th>App Name</th><th>IP</th></tr>";

    // SEPARATOR
    public static final String SEPARATOR = "/";
    public static final String SEPARATOR_SPACE = " ";

    // STACK
    public static final String STACK_NOT_FOUND = "<p> STACK NOT FOUND <p>";
    public static final String STACK_APPLICATION_REQUEST = "/stacks/application";

    //NUM_CONVERT
    public static final String NUM_CONVERT_NOT_FOUND = "<p> NUM CONVERT NOT FOUND <p>";
    public static final String CONVERT_OPTION = "CONVERT";
    public static final String LIST_NUM_OPTION = "LIST";
    
    // HASH
    public static final String HASH_NOT_FOUND = "<p> HASH NOT FOUND <p>";
    public static final String HASH_APPLICATION_REQUEST = "/hash/application";
    // USERAUTH
    public static final String USERAUTH_NOT_FOUND = "<p> USERAUTH NOT FOUND <p>";
    public static final String USERAUTH_APPLICATION_REQUEST = "/userAuthMD5/application";

    // ------------ NUMBERS
    public static final int TIMEOUT = 5;
    public static final String BCAST_ADDR = "255.255.255.255";
    public static final int SERVICE_PORT = 30102;
    public static final int FLAGS_LENGTH = 1;

    // ------------ APP NAME (com espaço)
    public static final String STACK_APP_NAME = "STACK";
    public static final String NUM_CONVERT_NAME = "NUMCONVERTER";
    public static final String TCP_MONITOR_APP_NAME = "TCP_MONITOR";
    public static final String HASH_NAME = "HASH ";
    public static final String USER_AUTH_MD5_NAME = "USERAUTHMD5";

    // ------------- FLAGS
    public static byte ARRIVAL_FLAG = 0;
    public static byte EXIT_FLAG = 1;
    public static byte MAIN_FLAG = 2;

    //------------- MESSAGE
    public static final String EXIT_MSG = "closed";
    public static final String ARRIVAL_MSG = "created";
    public static final String MAINTENANCE_MSG = "running!";
    public static final String RESPONSE_MESSAGE = "response";

    //------------- CORE MENU OPTIONS
    public static final String EXIT_OPTION = "EXIT";
    public static final String STATUS_OPTION = "STATUS";
    public static final String MAINTENANCE_OPTION = "MAINTENANCE";
    public static final String LIST_OPTION = "LIST";
    public static final String GENERIC_APPLICATION_OPTION = "OTHER APP";

    //------------- STACK OPTIONS
    public static final String PUSH_OPTION = "Push *ID(0-9) *ELEMENT";
    public static final String POP_OPTION = "Pop *ID(0-9)*";
    public static final String LIST_STACKS_OPTION = "List";
    public static final String PUSH_MSG = "Push";
    public static final String POP_MSG = "Pop";

    //------------- TCP MONITOR OPTIONS
    public static final String ADD_SERVICE_OPTION = "ADD";
    public static final String REMOVE_SERVICE_OPTION = "REMOVE";
    public static final String SERVICES_STATUS_OPTION = "STATUS";
    public static final String TCP_SERVICE_NOT_ADDED="<p> SERVICE NOT ADDED <p>";
    public static final String SERVICE_NOT_FOUND = "<p> SERVICE NOT FOUND <p>";

    //------------- HASH OPTIONS
    public static final String DIGEST_OPTION = "Digest";
    public static final String MD5_HASH = "MD5";
    public static final String SHA_0_HASH = "SHA-0";
    public static final String SHA_256_HASH = "SHA-256";

    //---USERAUTH OPTIONS
    public static final String GET_AUTH_TOKEN_OPTION = "GetAuthToken";
    public static final String AUTHENTICATE_OPTION = "Authenticate *username* *password*";


    //Var methods

    public Scanner in = new Scanner(System.in);

    public List<String> getVarStoreInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        System.out.print("Insert text content: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getVarFetchInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getVarEraseInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getHashDigestInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        System.out.print("Insert hash algorithm: ");
        input.add(in.nextLine());
        System.out.print("Insert text content: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getNumConvertConvertInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert input representation: ");
        input.add(in.nextLine());
        System.out.print("Insert output representation: ");
        input.add(in.nextLine());
        System.out.print("Insert number representation: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getStackPushInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        System.out.print("Insert text content: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getStackPopInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getQueueEnqueueInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        System.out.print("Insert text content: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getQueueDequeueInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        return input;
    }

    public List<String> getQueueRemoveInput(){
        List<String> input = new ArrayList<>();
        System.out.print("Insert identifier: ");
        input.add(in.nextLine());
        return input;
    }
}
