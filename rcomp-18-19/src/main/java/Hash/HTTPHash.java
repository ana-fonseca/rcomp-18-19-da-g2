package Hash;

import core.Config;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;

/**
 * Based in PL21-Rcomp
 */
@SuppressWarnings("Duplicates")
public class HTTPHash {

    static private final String BASE_FOLDER = "www/hash";
    static private ServerSocket sock;
    private HTTPHashRequest req;



    public void runHTTP(DatagramSocket s, DatagramPacket p) {
        // OPEN SOCKET
        try {
            sock = new ServerSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + Config.SERVICE_PORT);
            System.exit(1);
        }
        // SERVER LOOP
        req = new HTTPHashRequest(sock,s,p, BASE_FOLDER);
        req.start();
    }

}
