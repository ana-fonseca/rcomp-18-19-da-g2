/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hash;

import core.Application;
import core.Config;
import core.DrawMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * UDPHash
 *
 * @author Ricardo Costa
 */
public class UDPHash {

    static InetAddress address;
    static DatagramSocket sock;
    public static DatagramSocket sockSend;
    private static HashSet<Application> peersList = new HashSet<>();

    public static synchronized void addIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        peersList.add(a);
    }

    public static synchronized void remIP(String name, InetAddress ip) {
        Application a = new Application(name, ip);
        peersList.remove(a);
    }

    public static synchronized void printIPs() {
        for (Application a : peersList) {
            System.out.print(" " + a.getName() + " ip: " + a.getIp());
        }
    }

    public static synchronized String printIpsHtml() {
        String p = Config.APP_TABLES;
        for (Application a : peersList) {
            p = p + "<tr><td>" + a.getName() + "</td><td>" + a.getIp() + "</td></tr>";
        }
        return p;
    }

    public static synchronized Application checkApplicationName(String s) {
        for (Application a : peersList) {
            if (a.getName().equals(s.trim())) {
                return a;
            }
        }
        return null;
    }

    public static boolean checkApplication(String name) {
        for (Application app : peersList) {
            if (app.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception {
        for (Application a : peersList) {
            p.setAddress(a.getIp());
            s.send(p);
        }
    }

    public static void sendPacketToApplication(DatagramSocket s, DatagramPacket p, Application appName) throws IOException {
        p.setAddress(appName.getIp());
        s.send(p);
    }

    public static synchronized void sendPacket(String str, DatagramSocket s, DatagramPacket p) {
        byte[] data = new byte[500];
        data = str.getBytes();
        p.setData(data);
        p.setLength(data.length);
        try {
            s.send(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static InetAddress getIpByName(String appName) {
        for (Application app : peersList) {
            if (app.getName().equalsIgnoreCase(appName)) {
                return app.getIp();
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        String frase;
        byte[] data = new byte[500];
        DatagramPacket udpPacket;
        // SOCK OPENER

        try {
            sock = new DatagramSocket(Config.SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // ADDRESS
        address = InetAddress.getByName(Config.BCAST_ADDR);
        String msg = Config.HASH_NAME + Config.SEPARATOR + Config.ARRIVAL_MSG;
        System.out.println(msg);
        data = msg.getBytes();
        udpPacket = new DatagramPacket(data, data.length, address, Config.SERVICE_PORT);
        sock.setBroadcast(true);
        sock.send(udpPacket);

        // HTTP SERVER
        HTTPHash http = new HTTPHash();
        http.runHTTP(sock, udpPacket);

        // THREAD CREATION
        Thread udpReceiver = new Thread(new UdpHashReceiver(sock));
        udpReceiver.start();

//        // NOTIFICATION ARRIVAL
//        frase = Config.HASH_NAME + Config.ARRIVAL_MSG;
//        data = frase.getBytes();
//        udpPacket = new DatagramPacket(data, data.length, address, Config.SERVICE_PORT);
//        sock.setBroadcast(true);
//        sock.send(udpPacket);
        // MENU
        while (true) {
            Thread.sleep(1000);
            // DRAW MENUS
            System.out.println(DrawMenu.drawHeadlineHash());
            System.out.println(DrawMenu.drawMenuHash());
            frase = in.readLine();

            switch (frase.toUpperCase()) {
                case "EXIT":
//                    sendPacket(Config.HASH_NAME + Config.EXIT_MSG, sock, udpPacket);
                    udpReceiver.interrupt();
                    data = (Config.HASH_NAME + Config.SEPARATOR + Config.EXIT_MSG).getBytes();
                    udpPacket.setData(data);
                    udpPacket.setLength(data.length);
                    udpPacket.setAddress(InetAddress.getByName(Config.BCAST_ADDR));
                    sock.send(udpPacket);
                    // CLOSE SOCKET
                    sock.close();
                    System.exit(0);
                case "STATUS":
                    System.out.println("Active applications: \n");
                    printIPs();
                    System.out.println("");
                    break;
                case "MAINTENANCE":
                    sendPacket(Config.HASH_NAME + Config.SEPARATOR + Config.MAINTENANCE_MSG, sock, udpPacket);
                    break;
                case "HASH":
                    System.out.println(DrawMenu.drawMenuHashOption());
                    String option = in.readLine();
                    if (option.equalsIgnoreCase("digest")) {
                        Scanner scanner = new Scanner(System.in);
                        String[] stringToHash = new String[2];
                        boolean next = true;
                        do {
                            System.out.println("insert valid identifier, representing the hash algorithm to be used:");
                            stringToHash[0] = scanner.nextLine();
                            if (stringToHash[0].equalsIgnoreCase(Config.MD5_HASH) || stringToHash[0].equalsIgnoreCase(Config.SHA_0_HASH) || stringToHash[0].equalsIgnoreCase(Config.SHA_256_HASH)) {
                                next = false;
                            } else {
                                System.out.println("wrong identifier, available ones:"
                                        + "\n" + Config.MD5_HASH
                                        + "\n" + Config.SHA_0_HASH
                                        + "\n" + Config.SHA_256_HASH);
                            }
                        } while (next);
                        System.out.println("Insert the String to convert:");
                        stringToHash[1] = scanner.nextLine();
                        option = option + Config.SEPARATOR + stringToHash[0] + Config.SEPARATOR + stringToHash[1];
                    }
                    option = Config.HASH_NAME + Config.SEPARATOR + option;
                    data = option.getBytes();
                    udpPacket.setData(data);
                    udpPacket.setLength(data.length);
                    udpPacket.setAddress(getIpByName(Config.HASH_NAME.trim()));
                    sock.send(udpPacket);
                    break;
                case "OTHER APP":
                    System.out.println("INTRODUCE THE APPLICATION NAME AND THE COMMAND WITH THE RESPECTIVE PARAMETERS (*APP NAME* *Command* *Parameter1* *Parameter2* ...):");
                    frase = in.readLine();
                    String[] line = frase.split(Config.SEPARATOR);
                    String appName = line[0].trim();
                    udpPacket.setData(frase.getBytes());
                    InetAddress destination = getIpByName(appName);
                    if (destination != null) {
                        udpPacket.setAddress(destination);
                        udpPacket.setLength(frase.getBytes().length);
                        sock.send(udpPacket);
                    } else {
                        System.out.println("APPLICATION IS NOT AVAILABLE");
                    }
                    break;

            }
//            Thread.sleep(500);
        }
    }

    private static String listHash() {
        return "Hash Type List:"
                + "\n" + Config.MD5_HASH
                + "\n" + Config.SHA_0_HASH
                + "\n" + Config.SHA_256_HASH;
    }
}

@SuppressWarnings("Duplicates")
class UdpHashReceiver implements Runnable {

    private DatagramSocket s;

    public UdpHashReceiver(DatagramSocket udp_s) throws SocketException {
        s = udp_s;
        udp_s.setSoTimeout(Config.TIMEOUT * 1000);
    }

    @Override
    public void run() {
        int flag = 1;
        byte[] data = new byte[500];
        DatagramPacket p;
        InetAddress packetAddress;
        p = new DatagramPacket(data, data.length);
        String message, response = "";
        while (true) {
            data = new byte[500];
            p.setData(data, 0, data.length);
            try {
                s.receive(p);
            } catch (SocketTimeoutException ex) {
                Thread.currentThread().interrupt();
            } catch (IOException ex) {
                Thread.currentThread().interrupt();
            }

//            if (!s.getLocalAddress().equals(p.getAddress()) && !lastFunction.equals(function)) { // Avoid spamming
            if (p.getPort() == Config.SERVICE_PORT) {
                packetAddress = p.getAddress();
                message = new String(p.getData(), 0, p.getLength());
                System.out.println(message);
                String[] msg = message.split(Config.SEPARATOR);
//                System.out.println(msg.length + ":" + msg[0] + ":" + msg[1]);
                if (msg[0].trim().equalsIgnoreCase(Config.HASH_NAME.trim())) {//SENDING APP IS LOCAL APP
                    switch (msg[1].trim().toUpperCase()) {
                        case "DIGEST":
                            if (msg[2].equalsIgnoreCase(Config.MD5_HASH)) {
                                try {
                                    response = generateMD5(msg[3]);
                                    System.out.println("hash code: " + response);
                                } catch (Exception ex) {
                                    Logger.getLogger(UdpHashReceiver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else if (msg[2].equalsIgnoreCase(Config.SHA_0_HASH)) {
                                try {
                                    response = generateSHA1(msg[3]);
                                    System.out.println("hash code: " + response);
                                } catch (Exception ex) {
                                    Logger.getLogger(UdpHashReceiver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else if (msg[2].equalsIgnoreCase(Config.SHA_256_HASH)) {
                                try {
                                    response = generateSHA256(msg[3]);
                                    System.out.println("hash code: " + response);
                                } catch (Exception ex) {
                                    Logger.getLogger(UdpHashReceiver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            flag = 1;
//                            response = Config.RESPONSE_MESSAGE + " " + tcpList.addService(msg[2], msg[3], msg[4]);
                            break;
                        case "LIST":
                            System.out.println(listHash());
                            flag = 1;
                            break;
                        case "CREATED":
                            UDPHash.addIP(msg[0].trim(), packetAddress);

//                            try {
//                                UDPHash.sendToAll(s, p);
//                            } catch (Exception ex) {
//                                Logger.getLogger(UdpHashReceiver.class.getName()).log(Level.SEVERE, null, ex);
//                            }
                            System.out.println(Config.HASH_NAME + "is now online.");
                            flag = 1;
                            break;
                        default:
                            flag = 1;
                    }
                    if (flag == 0) {
                        p.setAddress(packetAddress);
                        p.setData(response.getBytes());
                        p.setLength(response.getBytes().length);
                        try {
                            s.send(p);
                        } catch (IOException ex) {
                            System.out.println("Response not sent");
                        }
                    }
                    flag = 0;
                } else if (msg[1].trim().equalsIgnoreCase(Config.RESPONSE_MESSAGE)) {//RESPONSE FROM AN APP
                    System.out.print(msg[0] + "'s response: ");
                    for (int i = 2; i < msg.length; i++) {
                        System.out.print(msg[i] + Config.SEPARATOR);
                    }
                    System.out.println();
                } else if (msg[1].trim().equalsIgnoreCase(Config.ARRIVAL_MSG)) { //APP ARRIVED
                    if (!UDPHash.checkApplication(msg[0].trim())) {
                        UDPHash.addIP(msg[0].trim(), packetAddress);
                        data = (Config.HASH_NAME + Config.ARRIVAL_MSG).getBytes();
                        p.setData(data);
                        p.setLength(data.length);
                        p.setAddress(packetAddress);
                        try {
                            s.send(p);
                        } catch (IOException ex) {
                            System.out.println("Reply ping not sent.");
                        }
                        System.out.println(msg[0].trim() + " is now online.");
                    }
                } else if (msg[1].trim().equalsIgnoreCase(Config.EXIT_MSG)) {//APP EXITED
                    UDPHash.remIP(msg[0].trim(), packetAddress);
                    System.out.println(msg[0].trim() + " is now offline.");
                }
                p.setPort(1);
            }
        }
    }

    private static String listHash() {
        return "Hash Type List:"
                + "\n" + Config.MD5_HASH
                + "\n" + Config.SHA_0_HASH
                + "\n" + Config.SHA_256_HASH;
    }

    public static synchronized String getHashTypesHTML() {
        String textHtml = "<ul>";
        textHtml = textHtml + "<li>" + Config.MD5_HASH + "</li>";
        textHtml = textHtml + "<li>" + Config.SHA_0_HASH + "</li>";
        textHtml = textHtml + "<li>" + Config.SHA_256_HASH + "</li>";
        textHtml = textHtml + "</ul>";
        return textHtml;
    }

    private static String digest(String message, String algorithm)
            throws Exception {

        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));

            return convertByteArrayToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new Exception(
                    "Could not generate hash from String", ex);
        }
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }

    public static String generateMD5(String message) throws Exception {
        return digest(message, "MD5");
    }

    public static String generateSHA1(String message) throws Exception {
        return digest(message, "SHA-1");
    }

    public static String generateSHA256(String message) throws Exception {
        return digest(message, "SHA-256");
    }
}
