package Hash;

import core.Config;
import core.HTTPmessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

@SuppressWarnings("Duplicates")
public class HTTPHashRequest extends Thread {

    String baseFolder;
    Socket sock;
    ServerSocket svSock;
    DatagramSocket s;
    DatagramPacket p;
    DataInputStream inS;
    DataOutputStream outS;

    public HTTPHashRequest(ServerSocket cliSock, DatagramSocket s, DatagramPacket p, String baseFolder) {
        this.baseFolder = baseFolder;
        this.svSock = cliSock;
        this.s = s;
        this.p = p;
    }

    public void run() {
        while (true) {
            try {
                sock = svSock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outS = new DataOutputStream(sock.getOutputStream());
                inS = new DataInputStream(sock.getInputStream());
            } catch (IOException ex) {
                System.out.println("Thread error on data streams creation");
            }
            try {
                HTTPmessage request = new HTTPmessage(inS);
                HTTPmessage response = new HTTPmessage();
                if (request.getMethod().equals(Config.GET)) {
                    if (request.getURI().equals(Config.HASH_REQUEST_NAME)) {
                        response.setContentFromString(UdpHashReceiver.getHashTypesHTML(), "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else if (request.getURI().equals(Config.HASH_APPLICATION_REQUEST)) {
                        response.setContentFromString(UDPHash.printIpsHtml(), "text/html");
                        response.setResponseStatus(Config.POSITIVE_RESPONSE);
                    } else {
                        String fullname = baseFolder + "/";
                        if (request.getURI().equals("/")) {
                            fullname = fullname + "index.html";
                        } else {
                            fullname = fullname + request.getURI();
                        }
                        if (response.setContentFromFile(fullname)) {
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else {
                            response.setContentFromString(Config.PAGE_NOT_FOUND, "text/html");
                            response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                        }
                    }
                    response.send(outS);
                } else { // NOT GET
                    if (request.getMethod().equals(Config.PUT)) {
                        String resposta;
                        String split[] = request.getURI().split(Config.SEPARATOR);

                        if (split[2].trim().equalsIgnoreCase(Config.DIGEST_OPTION)) { // push
                            resposta = Config.HASH_NAME.trim() + Config.SEPARATOR + split[3].trim() + Config.SEPARATOR + split[4].trim();
                            UDPHash.sendPacket(resposta, s, p); // manda o packet para o server
                            Thread.currentThread().sleep(500);
                            response.setContentFromString("<p>" + "HASH requested to server" + "</p>", "text/html"); // resposta
                            response.setResponseStatus(Config.POSITIVE_RESPONSE);
                        } else if (split[2].trim().equalsIgnoreCase(Config.LIST_OPTION)) {
                            try {
                                resposta = Config.HASH_NAME + Config.SEPARATOR + split[2].trim();
                                UDPHash.sendPacket(resposta, s, p); // manda o packet para o server
                                Thread.currentThread().sleep(500);
                                response.setContentFromString("<p>list presented!</p>", "text/html"); // resposta
                                response.setResponseStatus(Config.POSITIVE_RESPONSE);
                            } catch (IllegalArgumentException ex) {
                                response.setContentFromString(Config.HASH_NOT_FOUND, "text/html"); // erro
                                response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                            }
                        }

                    } else {
                        response.setContentFromString(Config.METHOD_NOT_ALLOWED, "text/html");
                        response.setResponseStatus(Config.NEGATIVE_RESPONSE);
                    }
                    response.send(outS);
                }
            } catch (IOException | InterruptedException ex) {
                System.out.println("Thread error when reading request");
            }
            try {
                sock.close();
            } catch (IOException ex) {
                System.out.println("CLOSE IOException");
            }
        }

    }
}
