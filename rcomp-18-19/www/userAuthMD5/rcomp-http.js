function refreshUserAuthMD5() {
    var request = new XMLHttpRequest();
    var userAuthMD5 = document.getElementById("userAuthMD5");
    var error = document.getElementById("error");

    request.onload = function () {
        userAuthMD5.innerHTML = this.responseText;
        setTimeout(refreshUserAuthMD5, 2000);
    };

    request.ontimeout = function () {
        error.innerHTML = "Server timeout, still trying ...";
        error.style.color = "red";
        setTimeout(refreshUserAuthMD5, 100);
    };

    request.onerror = function () {
        error.innerHTML = "No server reply, still trying ...";
        error.style.color = "red";
        setTimeout(refreshUserAuthMD5, 8000);
    };


}

function refreshApps() {
    var request = new XMLHttpRequest();
    var apps = document.getElementById("apps");
    request.onload = function () {
        console.log(this.responseText);
        apps.innerHTML = this.responseText;
        setTimeout(refreshApps, 2000);
    };

    request.open("GET", "userAuthMD5/application", true);

    request.timeout = 5000;
    request.send(null);
}
/*
 * Returns a function that waits for the state change in XMLHttpRequest
 */
function getReadyStateHandler(xmlHttpRequest) {

    // an anonymous function returned
    // it listens to the XMLHttpRequest instance
    return function () {
        if (xmlHttpRequest.readyState == 4) {
            if (xmlHttpRequest.status == 200) {
                document.getElementById("response").innerHTML = xmlHttpRequest.responseText;
            } else {
                alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);
            }
        }
    };
}
function sendInstruction() {
    var string = document.getElementById("username").value + " " + document.getElementById("password").value;

    var array = string.split(" ");
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    request.onload = function () {
        console.log("flag");
        history.innerHTML = this.responseText + history.innerHTML;
    };

    if (array.length == 0) {
        document.getElementById("error").innerHTML = "Instruction contains errors.";
    } else {
        if (document.getElementById("GetAuthToken").checked == true) {
            var string = "userAuthMD5/GetAuthToken/";
            if (array.length != 0) {
                string = string + array[0].trim();
                request.open("PUT", string, true);
                request.timeout = 5000;
                request.onreadystatechange = function () {
                    document.getElementById("response").innerHTML = "Token: " + request.responseText;
                    document.getElementById("error").innerHTML = "";

                };
                request.send(null)


            } else {
                document.getElementById("error").innerHTML = "Write Username";
            }
        } else if (document.getElementById("Authenticate").checked == true) {
            var string = "/userAuthMD5/Authenticate/";
            if (array.length > 1) {
                string = string + array[0] + "/" + array[1];
                request.open("PUT", string, true);
                request.onreadystatechange = function () {
                    document.getElementById("response").innerHTML = request.responseText;
                    document.getElementById("error").innerHTML = "";

                };
                request.timeout = 5000;
                request.send(null);
            } else {
                document.getElementById("error").innerHTML = "Instruction contains errors - username or password";
            }
        } else {
            document.getElementById("error").innerHTML = "Instruction contains errors - Function not detected.";
        }
    }
}
