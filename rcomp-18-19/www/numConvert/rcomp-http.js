

function refreshApps() {
    var request = new XMLHttpRequest();
    var numConverts = document.getElementById("tBody");
    var history = document.getElementById("history");
    request.onload = function () {
        if (this.responseText.split("#")[0] === "response") {
            history.innerHTML = this.responseText.split("#")[1] + history.innerHTML;
            setTimeout(refreshApps, 2000);
        } else {
            numConverts.innerHTML = this.responseText;
            setTimeout(refreshApps, 2000);
        }
    };
    request.open("GET", "/numConvert/application", true);
    request.timeout = 5000;
    request.send();
}


function sendInstruction() {
    var instruction = document.getElementById("instruction").value;
    var array = instruction.split(" ");
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    request.onload = function () {
        history.innerHTML = this.responseText+history.innerHTML;
        setTimeout(sendInstruction, 2000);
    };
    if (array.length == 0) {
        document.getElementById("error").innerHTML = "Instruction contains errors - Message separated by spaces.";
    } else {
        if ((array[0].toUpperCase()) === "CONVERT") {
            var string = "/numConvert/CONVERT/"
            string = string + array[1] + "/" + array[2] + "/" + array[3]; //CONVERT HEX BIN 1111
            request.open("GET", string, true);
            request.timeout = 5000;
            request.send();
        } else if ((array[0].toUpperCase()) === "LIST") {
            var string = "/numConvert/LIST/";
            request.open("GET", string, true);
            request.timeout = 5000;
            request.send();
        } else {
            var i;
            var string = "";
            for (i = 0; i < array.length; i++) {
                string = string + "/" + array[i]; // /stack/push/1/lol
            }
            request.open("GET", string, true);
            request.timeout = 5000;
            request.send();
        }
    }
}

function convert() {
    var request = new XMLHttpRequest();
    var string = "/numConvert/CONVERT/";
    var inputTypeElem = document.getElementById("inputValue");
    var inputType = inputTypeElem.options[inputTypeElem.selectedIndex].value;
    var numberElem = document.getElementById("inputNumber");
    var number = numberElem.value;
    var outputTypeElem = document.getElementById("outputValue");
    var outputType = outputTypeElem.options[outputTypeElem.selectedIndex].value;
    var history = document.getElementById("history");
    request.onload = function () {
        history.innerHTML = this.responseText+history.innerHTML;
        setTimeout(convert, 2000);
    };
    string = string + inputType + "/" + outputType + "/" + number; //CONVERT HEX BIN 1111

    request.open("GET", string, true);
    request.timeout = 5000;
    request.send();
}

function list() {
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    request.onload = function () {
        history.innerHTML = this.responseText+history.innerHTML;
        setTimeout(list, 2000);
    };
    var string = "/numConvert/LIST/";
    request.open("GET", string, true);
    request.timeout = 5000;
    request.send();
}