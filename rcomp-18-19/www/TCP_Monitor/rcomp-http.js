function refreshServices() {
    var request = new XMLHttpRequest();
    var services = document.getElementById("services");
    var error = document.getElementById("answer");
    request.onload = function () {
        services.innerHTML = this.responseText;
        setTimeout(refreshServices, 2000);
    };

    request.ontimeout = function () {
        error.innerHTML = "Server timeout, still trying ...";
        error.style.color = "red";
        setTimeout(refreshServices, 100);
    };

    request.onerror = function () {
        error.innerHTML = "No server reply, still trying ...";
        error.style.color = "red";
        setTimeout(refreshServices, 8000);
    };

    request.open("GET", "/tcp_monitor/status", true);
    request.timeout = 5000;
    request.send();
}

function refreshApps() {
    var request = new XMLHttpRequest();
    var apps = document.getElementById("apps");
    var answer = document.getElementById("answer");
    request.onload = function () {
        if (this.responseText.split("#")[0] === "response") {
            answer.innerHTML = this.responseText.split("#")[1] + answer.innerHTML;
            setTimeout(refreshApps, 2000);
        } else {
            apps.innerHTML = this.responseText;
            setTimeout(refreshApps, 2000);
        }
    };
    request.open("GET", "/tcp_monitor/application", true);
    request.timeout = 5000;
    request.send();
}

function sendInstruction() {
    var instruction = document.getElementById("instruction").value;
    var request = new XMLHttpRequest();
    var answer = document.getElementById("answer");
    var array=instruction.split(" ");
    request.onload = function () {
        answer.innerHTML = this.responseText;
        setTimeout(refreshServices, 2000);
    };
    if (array.length == 0) {
        document.getElementById("answer").innerHTML = "Instruction contains errors - Message separated by spaces.";
    } else {
        var i;
        var string = "";
        for (i = 0; i < array.length; i++) {
            string = string + "/" + array[i]; // /stack/push/1/lol
        }
        request.open("GET", string, true);
        request.timeout = 5000;
        request.send();
    }
}

function addService() {
    var request = new XMLHttpRequest();
    var string = "/tcp_monitor/add/";
    var id = document.getElementById("addID").value;
    var ip = document.getElementById("addIP").value;
    var port = document.getElementById("addPort").value;
    var result = document.getElementById("addResult");
    request.onload = function () {
        result.innerHTML = this.responseText;
        setTimeout(refreshServices, 2000);
    };
    string = string + id + "/" + ip + "/" + port;

    request.open("GET", string, true);
    request.timeout = 5000;
    request.send();
}

function removeService() {
    var request = new XMLHttpRequest();
    var string = "/tcp_monitor/remove/";
    var id = document.getElementById("removeID").value;
    var result = document.getElementById("removeResult");
    request.onload = function () {
        result.innerHTML = this.responseText;
        setTimeout(refreshServices, 2000);
    };
    string = string + id;

    request.open("GET", string, true);
    request.timeout = 5000;
    request.send();
}