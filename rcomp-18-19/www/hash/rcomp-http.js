function refresh() {
    var request = new XMLHttpRequest();
    var hash = document.getElementById("hash");
    var error = document.getElementById("error");
    request.onload = function () {
        hash.innerHTML = this.responseText;
        setTimeout(refresh, 2000);
    };

    request.ontimeout = function () {
        error.innerHTML = "Server timeout, still trying ...";
        error.style.color = "red";
        setTimeout(refresh, 100);
    };

    request.onerror = function () {
        error.innerHTML = "No server reply, still trying ...";
        error.style.color = "red";
        setTimeout(refresh, 8000);
    };

    request.open("GET", "/hash", true);
    request.timeout = 5000;
    request.send();
}

function refreshApps() {
    var request = new XMLHttpRequest();
    var apps= document.getElementById("apps");
    request.onload = function () {
        console.log(this.responseText);
        apps.innerHTML = this.responseText;
        setTimeout(refreshApps, 2000);
    };
    request.open("GET", "/hash/application", true);
    request.timeout = 5000;
    request.send();
}

function sendInstruction() {
    var instruction = document.getElementById("instruction").value;
    var array = instruction.split(" ");
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    request.onload = function () {
        console.log("flag");
        history.innerHTML = this.responseText + history.innerHTML;
    };

    if (array.length === 0) {
        document.getElementById("error").innerHTML = "Instruction contains errors - Message separated by spaces.";
    } else {
        if (document.getElementById("digest").checked === true) {
            var string = "/hash/digest/";
            if (array.length === 2) {
                string = string + array[0].trim() + "/" + array[1].trim();
                request.open("PUT", string, true);
                request.timeout = 5000;
                request.send();
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("error").innerHTML = "Instruction contains errors - Misses Hash type or message";
            }

        } else if (document.getElementById("list").checked === true) {
            var string = "/hash/list/";
            if (array.length === 1) {
//                string = string + array[0].trim();
                request.open("PUT", string, true);
                request.timeout = 5000;
                request.send();
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("error").innerHTML = "Instruction contains errors - Clear box to use list option";
            }
        } else {
            document.getElementById("error").innerHTML = "Instruction contains errors - Digest or List not detected.";
        }
    }
}
