function refreshStacks() {
    var request = new XMLHttpRequest();
    var stacks = document.getElementById("stacks");
    var error = document.getElementById("error");
    request.onload = function () {
        stacks.innerHTML = this.responseText;
        setTimeout(refreshStacks, 2000);
    };

    request.ontimeout = function () {
        error.innerHTML = "Server timeout, still trying ...";
        error.style.color = "red";
        setTimeout(refreshStacks, 100);
    };

    request.onerror = function () {
        error.innerHTML = "No server reply, still trying ...";
        error.style.color = "red";
        setTimeout(refreshStacks, 8000);
    };

    request.open("GET", "/stacks", true);
    request.timeout = 5000;
    request.send();
}

function refreshApps() {
    var request = new XMLHttpRequest();
    var apps= document.getElementById("apps");
    request.onload = function () {
        console.log(this.responseText);
        apps.innerHTML = this.responseText;
        setTimeout(refreshApps, 2000);
    };
    request.open("GET", "/stacks/application", true);
    request.timeout = 5000;
    request.send();
}

function sendInstruction() {
    var instruction = document.getElementById("instruction").value;
    var array = instruction.split(" ");
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    request.onload = function () {
        console.log("flag");
        history.innerHTML = this.responseText + history.innerHTML;
    };

    if (array.length == 0) {
        document.getElementById("error").innerHTML = "Instruction contains errors - Message separated by spaces.";
    } else {
        if (document.getElementById("push").checked == true) {
            var string = "/stacks/Push/";
            if (array.length == 2) {
                string = string + array[0].trim() + "/" + array[1].trim();
                request.open("PUT", string, true);
                request.timeout = 5000;
                request.send();
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("error").innerHTML = "Instruction contains errors - Misses ID or Message";
            }

        } else if (document.getElementById("pop").checked == true) {
            var string = "/stacks/Pop/";
            if (array.length == 1) {
                string = string + array[0].trim();
                request.open("PUT", string, true);
                request.timeout = 5000;
                request.send();
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("error").innerHTML = "Instruction contains errors - Misses ID";
            }
        } else {
            document.getElementById("error").innerHTML = "Instruction contains errors - Push or Pop not detected.";
        }
    }
}

function sendOtherAppInstruction(){
    var request = new XMLHttpRequest();
    var history = document.getElementById("history");
    var information = document.getElementById("otherApps").value;
    request.onload = function () {
        history.innerHTML = this.responseText + history.innerHTML;
    };
    var information = "/otherApps/" + information;
    request.open("GET", information, true);
    request.timeout = 5000;
    request.send();
}
