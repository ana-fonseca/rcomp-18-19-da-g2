RCOMP 2018-2019 Project - Sprint 1 - Member 1170426 folder
===========================================
(This folder is to be created/edited by the team member 1170426 only)

#### This team member will take care of building B ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170426) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

IP's being used:
172.16.66.0
172.16.67.0

VLAN's being used:
Ground Floor 84
Floor One 85
Access Points 86
DMZ 87
VoIP 89

Router startup config:

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname RouterB
!
!
!
!
ip dhcp excluded-address 172.16.66.1
ip dhcp excluded-address 172.16.66.65
ip dhcp excluded-address 172.16.66.129
ip dhcp excluded-address 172.16.67.1
ip dhcp excluded-address 172.16.66.161
ip dhcp excluded-address 172.16.67.193
!
ip dhcp pool net84
 network 172.16.66.0 255.255.255.192
 default-router 172.16.66.1
ip dhcp pool net85
 network 172.16.66.64 255.255.255.192
 default-router 172.16.66.65
ip dhcp pool net86
 network 172.16.66.128 255.255.255.224
 default-router 172.16.66.129
ip dhcp pool net87
 network 172.16.67.0 255.255.255.128
 default-router 172.16.67.1
ip dhcp pool net89
 network 172.16.66.160 255.255.255.224
 default-router 172.16.66.161
 option 150 ip 172.16.66.161
ip dhcp pool net100
 network 172.16.67.192 255.255.255.192
 default-router 172.16.66.162
ip dhcp pool VOIP
 network 172.16.66.0 255.255.255.0
 option 150 ip 172.16.66.161
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.84
 encapsulation dot1Q 84
 ip address 172.16.66.1 255.255.255.192
!
interface FastEthernet0/0.85
 encapsulation dot1Q 85
 ip address 172.16.66.65 255.255.255.192
!
interface FastEthernet0/0.86
 encapsulation dot1Q 86
 ip address 172.16.66.129 255.255.255.224
!
interface FastEthernet0/0.87
 encapsulation dot1Q 87
 ip address 172.16.67.1 255.255.255.128
!
interface FastEthernet0/0.89
 encapsulation dot1Q 89
 ip address 172.16.66.161 255.255.255.224
!
interface FastEthernet0/0.100
 encapsulation dot1Q 100
 ip address 172.16.67.193 255.255.255.192
!
interface FastEthernet0/1
 ip address 190.5.200.50 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1/0
!
interface FastEthernet0/1/1
!
interface FastEthernet0/1/2
!
interface FastEthernet0/1/3
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
telephony-service
 max-ephones 4
 max-dn 4
 ip source-address 172.16.66.161 port 2000
 auto assign 1 to 2
 auto assign 2 to 3
 auto assign 3 to 4
 auto assign 4 to 1
 auto assign 1 to 4
!
ephone-dn 1
 number 123456
!
ephone-dn 2
 number 234567
!
ephone-dn 3
 number 345678
!
ephone-dn 4
 number 456789
!
ephone 1
 device-security-mode none
 mac-address 0006.2A33.D635
 type 7960
 button 1:3
!
ephone 2
 device-security-mode none
 mac-address 0060.7087.B316
 type 7960
 button 1:1
!
ephone 3
 device-security-mode none
 mac-address 0090.2147.3628
 type 7960
 button 1:2
!
ephone 4
 device-security-mode none
 mac-address 0001.C953.26C2
 type 7960
 button 1:4
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

