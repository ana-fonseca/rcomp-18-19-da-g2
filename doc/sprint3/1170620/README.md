# RCOMP 2018-2019 Project - Sprint 3 - Member 1170620 folder

##Overview

![BuildingGOverView.png](BuildingGOverView.png)


##Configuration

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname RouterG
!
!
!
!
ip dhcp excluded-address 172.16.76.1
ip dhcp excluded-address 172.16.76.65
ip dhcp excluded-address 172.16.76.129
ip dhcp excluded-address 172.16.77.1
ip dhcp excluded-address 172.16.76.161
ip dhcp excluded-address 172.16.77.193
!
ip dhcp pool net60
 network 172.16.76.0 255.255.255.192
 default-router 172.16.76.1
ip dhcp pool net61
 network 172.16.76.64 255.255.255.192
 default-router 172.16.76.65
ip dhcp pool net62
 network 172.16.76.128 255.255.255.224
 default-router 172.16.76.129
ip dhcp pool net63
 network 172.16.77.0 255.255.255.128
 default-router 172.16.77.1
ip dhcp pool net64
 network 172.16.76.160 255.255.255.224
 default-router 172.16.76.161
 option 150 ip 172.16.76.161
ip dhcp pool net100
 network 172.16.77.192 255.255.255.192
 default-router 172.16.76.162
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.60
 encapsulation dot1Q 60
 ip address 172.16.76.1 255.255.255.192
!
interface FastEthernet0/0.61
 encapsulation dot1Q 61
 ip address 172.16.76.65 255.255.255.192
!
interface FastEthernet0/0.62
 encapsulation dot1Q 62
 ip address 172.16.76.129 255.255.255.224
!
interface FastEthernet0/0.63
 encapsulation dot1Q 63
 ip address 172.16.77.1 255.255.255.128
!
interface FastEthernet0/0.64
 encapsulation dot1Q 64
 ip address 172.16.76.161 255.255.255.224
!
interface FastEthernet0/0.100
 encapsulation dot1Q 100
 ip address 172.16.77.193 255.255.255.192
!
interface FastEthernet0/1
 ip address 190.5.200.50 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1/0
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/1/1
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/1/2
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/1/3
 switchport mode access
 switchport nonegotiate
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
telephony-service
 max-ephones 2
 max-dn 2
 ip source-address 172.16.76.161 port 2000
 auto assign 1 to 2
 auto assign 2 to 1
!
ephone-dn 1
 number 905073
!
ephone-dn 2
 number 905074
!
ephone 1
 device-security-mode none
 mac-address 0006.2A68.8416
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0001.9638.12AE
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end



## Subnetting

First of all, we need to subnet the given ip addresses to each building (they are specified in the planning). The ips given to building G are:

*  172.16.76.0 255.255.255.0
*  172.16.77.0 255.255.255.0

These are already subnetted from prefix /19 to /24, although we are wasting a lot of ips. So, to the following nodes we have the respective networks subnetted:

| Subnet name  | Number of Nodes | Available Hosts | Network | Mask | Decimal Mask | Assignable Range | Broadcast |
|--------------|-----------------|-----------------|---------|------|--------------|------------------|-----------|
| Ground floor | 40 | 62 | 172.16.76.0 | /26 | 255.255.255.192 | 172.16.76.1 - 172.16.76.62 | 172.16.76.63 |
| Floor one  | 90 | 126 | 172.16.76.128 | /25 | 255.255.255.128 | 172.16.76.129 - 172.16.76.254 | 172.16.76.255 |
| Wi-Fi | 126 | 126 | 172.16.77.0 | /25 | 255.255.255.128 | 172.16.77.1 - 172.16.77.127 | 172.16.77.128 |
| DMZ | 12 | 14 | 172.16.76.64 | /28 | 255.255.255.240 | 172.16.76.65 - 172.16.76.78 | 172.16.76.79 |
| VoIP | 67 | 126 | 172.16.77.128 | /25 | 255.255.255.128 | 172.16.77.129 - 172.16.77.254 | 172.16.77.255 |

-------------------------------------------------------------


## Static routing

In order to each router of the buildings know all the other networks we need to configure the static routes properly. We can simplify them reducing its mask network bits to /24 prefix (255.255.255.0).

    HOST-G(config)# ip route network-address mask next-hop-address

-------------------------------------------------------------

## DHCP Service

We must configure the dhcp pools to each VLAN and for that we need to create sub-interfaces instead of using switch modules on routers.

    HOST-G(config)# interface f0/0.60
    HOST-G(config-subif)# encapsulation dot1Q 60
    HOST-G(config-subif)# ip address 172.16.76.1 255.255.255.192

This is an example for the VLAN 60 on building G. The others are all the same except for DMZ which is not needed. We assume this sub-interface is only for VLAN 70 encapsulating it and giving it an address that was previously subnetted.

Now for the devices successfully get their IP we need to configure the pools and respective addresses, minding that we need to exclude some addresses (gateway ports on this case):

    HOST-G(config)# ip dhcp pool VLAN60
    HOST-G(config-dhcp)# network 170.16.76.0 255.255.255.192
    HOST-G(config-dhcp)# default-router 170.16.76.1

For the IST services, we need to have an ip to each ip phone so we need to add a special command, enabling TFTP requests:

    HOST-G(config)# ip dhcp pool VLAN64
    HOST-G(config-dhcp)# option 150 ip 172.16.77.129
    HOST-G(config-dhcp)# default-router 172.16.77.129
    HOST-G(config-dhcp)# network 170.16.77.128 255.255.255.128

------------------------------------------------------------

## VoIP and IST services

The best part of this work is know for sure. We have to configure ip phones and for that we need to configure our IST service on the router of each building. IP phones are very tricky and we need to implement all the things carefully.

Let's start:

    HOST-G(config)# telephony-services
    HOST-G(config-telephony)# auto-reg-phone
    HOST-G(config-telephony)# ip source address 172.16.77.129 port 2000
    HOST-G(config-telephony)# max-ephones 2
    HOST-G(config-telephony)# max-dn 67
    HOST-G(config-telephony)# auto assign 1 to 2

    HOST-G(config)# ephone-dn 1
    HOST-G(config-ephone-dn)# number 905073
    HOST-G(config)#
    HOST-G(config)# ephone-dn 2
    HOST-G(config-ephone-dn)# number 905074
    HOST-G(config)#
    HOST-G(config)# ephone 1
    HOST-G(config-ephone)#
    HOST-G(config)# ephone 2
    HOST-G(config-ephone)# 

1. First of all, we enable auto-reg-phone which automatically registers a ip phone on IST service. 

2. Next we define the source address to the ip phones register successfully specifying its port (2000 is the default port)

3. Define the max o ip phones can be registered.

4. Auto assign numbers to the respective existent phones.

5. Declare the dial line and the respective numbers.

Then we define the dial-peer to interconnect the ip phones of each building:

    HOST-G(config)# dial-peer voice 10 voip
    HOST-G(config-dial-peer)# destination-pattern 94.....
    HOST-G(config-dial-peer)# session target ipv4:172.16.77.129