RCOMP 2018-2019 Project - Sprint 3 planning
===========================================
### Sprint master: 1170809 ###

# 1. Sprint's backlog #
![backlog](backlog.png)

# 2. Technical decisions and coordination #

## Priorities:

  * The IPv4 network address assigned to the backbone network.
  * The IPv4 node address each router will use in the backbone network.
  * The addresses block each team member will use, for assignment of IPv4 addresses to networks within the building the team member is in charge of.
  * VoIP phone numbers and prefix digits schema. Each building should have a different prefix digit.

  **Addresses block assign to each member can't overlap each other's (including backbone).**

-------------------------------------------------------------------

  ## Routers and static routing ##

  Each building must have a router of model 2811 (others may not support VoIP packets in packet tracer).
  
  The router in each building will assure IPv4 traffic forwarding between local IPv4 networks (withon the building) and to other buildings’ IPv4 networks. Traffic to other buildings will be routed through the backbone IPv4 network to the appropriate router.

-------------------------------------------------------------------

  ## Internet Connection ##

  Building A has an internet connection. The member in charge of this building will represent in by a DSL modem connection to a fake ISP router. Of course this is the default route for traffic
  to unknown addresses within the entire infrastructure.

  **The fake ISP router must have a static routing table such as it will forward the matching traffic into the
  infrastructure.**

-------------------------------------------------------------------

  ## DHCP service and IPv4 node addresses ##

  The router in each building must provide the DHCP service to local networks, except for DMZ networks and the backbone where IPv4 node addresses are static and manually set (routers and servers).

  For the VoIP VLAN, the DHCP server configuration will include option 150 to represent the IP address of the TFTP (Trivial File Transfer Protocol) server to be used by Cisco IP phones model 7960 to download their configuration file.

  -----------------------------------------------------------------

  ## VoIP service ##

  The router in each building provides the VoIP service (Cisco Telephony) to the local VoIP network, this encompasses calls forwarding to other buildings (other buildings’ routers with the VoIP service).

  Switches’ ports connecting to Cisco IP phones 7960, must have the voice VLAN enabled (switchpor voice vlan VLANID) and the access VLAN disabled (no switchport access vlan).

  -----------------------------------------------------------------

  ## Teamwork organization ##

  Each of these simulations will encompass a single building, however, they must include the campus backbone. The main-connect and all intermediate cross-connects must be represented by a switch, and a router (2811 model) must be connected to each.

  -----------------------------------------------------------------

  ## Devices naming ##

  When a new device is added to the Packet Tracer simulation, a display name is automatically assigned, but then it should be changed to a meaningful name.

  The display name is just for Packet Tracer to display, at the OS level, each device has a hostname (DNS name), that can be settled by using the hostname command, however, no spaces are allowed.

  -----------------------------------------------------------------

  ## IPv4 addresses blocks assignement ##

  The address space (addresses block) supplied to the team must be used to assign non overlapping address spaces to each team member.

  The addresses block size each member/building will need depends on the number of networks within the building and the maximum number of nodes each of those networks is required to support.

  -----------------------------------------------------------------

  ## Routers' IPv4 addresses in the backbone VLAN ##

  The IPv4 node address each router will use is a key configuration and coordination element. 

  This address is used by each building’s router as follows:

  * Static routing: when an IPv4 packet has a destination address belonging to an address space assigned to another building, then the packet is forwarded to that building’s router.
  * VoIP call forwarding: when an incoming call is for a phone prefix assigned to another building, then the call is forward to the telephony services running in that building’s router.

-------------------------------------------------------------------

 ## Adresses block

 ![addresses](addresses_block.png)

 ------------------------------------------------------------------

 ## Commands

 ### Static routing
 
    Router(config)# ip route network-address subnet-mask {ip-address | exit-intf}

  
  **network-address:** Destination network address of the remote network to be added to the routing table, often this is referred to as the prefix.

  **subnet-mask:** Subnet mask, or just mask, of the remote network to be added to the routing table. The subnet mask can be modified to summarize a group of networks.

  **ip-address:** The IP address of the connecting router to use to forward the packet to the remote destination network. Commonly referred to as the next hop.

  **exit-intf:** The outgoing interface to use to forward the packet to the next hop.

  **distance:** is used to create a floating static route by setting an administrative distance that is higher than a dynamically learned route.

  Commands to verify:

      Router# show ip route

      Router# show ip route static

### Default static route

    Router(config)# ip route 0.0.0.0 0.0.0.0 {ip-address | exit intf}

--------------------------------------------------------------

### Internet Connection

  In building A, we must have a DSL modem attached to a cloud via phone cable and that clould attached to a router that will simulate the connection to the Internet.

  There are no need of configurations here except the to the ISP router and the forwarding method used on the cloud.

--------------------------------------------------------------

### DHCP Service

Must be done in the router of each building.

Step 1: Exclude specific addresses

    Router(config)# ip dhcp excluded address low-address [high-address]

Step 2: Configuring a DHCPv4 Pool
    
    Router(config)# ip dhcp pool pool-name
    Router(dhcp-config)#

Step 3: Configuring specific tasks

* Define the address pool

      Router(dhcp-config)# network network-number [mask | /prefix-length]

* Define the default router or gateway

      Router(dhcp-config)# default-router address [address2 ... address8]

* Define a DNS Server

      Router(dhcp-config)# dns-server address [address2 ... address8]

* Define the domain name

      Router(dhcp-config)# domain-name domain

* Define the duration of the DHCP lease
  
      Router(dhcp-config)# lease {days [hours] [minutes] | infinite}

* Define the NetBIOS WINS server

      Router(dhcp-config)# netbios-name-server address [address2 ... address8]

Disabling DHCPv4:

      Router(config)# no service dhcp

Verifying DHCPv4:

      Router# show ip dhcp binding
      Router#
      Router#
      Router# show running-config | section dhcp
      Router#
      Router#
      Router# show ip dhcp server statistics

--------------------------------------------------------------

## VoIP Services

  After creating the dhcp pool, we have to configure it differently to the voice services (ip phones).

  When defining the ip pool we must add the command:

      Router(dhcp-config)# option 150 ip ip-address

--------------------------------------------------------------

## Address blocks:

  ### Building A

      172.16.64.0/24
      172.16.65.0/24

  ### Building B

      172.16.66.0/24
      172.16.67.0/24

  ### Building C

      172.16.68.0/24
      172.16.69.0/24

  ### Building D

      172.16.70.0/24
      172.16.71.0/24

  ### Building E

      172.16.72.0/24
      172.16.73.0/24

  ### Building F

      172.16.74.0/24
      172.16.75.0/24
      
  ### Building G

      172.16.76.0/24
      172.16.77.0/24