RCOMP 2018-2019 Project - Sprint 3 - Member 1151422 folder
===========================================

97 | WIFI 172.16.70.0 255.255.255.0 | /24 | necessario: 140hosts | distribuido: 254hosts
99 | VOIP 172.16.71.0 255.255.255.128 | /25 | necessario: 80hosts | distribuido: 126hosts
96 | FO 172.16.71.128 255.255.255.128 | /25 | necessario: 75hosts | distribuido: 126hosts
95 | GF 172.16.78.0 255.255.255.192 | /26 | necessario: 40hosts | distribuido: 62hosts
98 | DMZ 172.16.78.64 255.255.255.240 | /28 | necessario: 12hosts | distribuido: 14hosts

# Configurations #
## RouterG ##
#### Configure vlans ips ####
!
en
conf t
hostname RouterD
interface vlan 95
ip address 172.16.78.1 255.255.255.192
interface vlan 96
ip address 172.16.71.129 255.255.255.12
interface vlan 97
ip address 172.16.70.1 255.255.255.0
interface vlan 98
ip address 172.16.78.65 255.255.255.240
interface vlan 99
ip address 172.16.71.1 255.255.255.128
!
interface fa0/1/0
switchport mode trunk
exit
!
ip dhcp pool GF
network 172.16.78.0 255.255.255.192
default-router 172.16.78.1
ip dhcp pool FO
network 172.16.71.128 255.255.255.128
default-router 172.16.71.129
ip dhcp pool VOIP
network 172.16.71.0 255.255.255.128
default-router 172.16.71.1
option 150 ip 172.16.71.1
ip dhcp pool WIFI
network 172.16.70.0 255.255.255.0
default-router 172.16.70.1
exit
service dhcp
exit
!
conf t
telephony-service
auto-reg-ephone
ip source-address 172.16.71.1 port 2000
max-ephones 5
max-dn 5
auto assign 1 to 3
ephone-dn 1
number 400001
ephone-dn 2
number 400002