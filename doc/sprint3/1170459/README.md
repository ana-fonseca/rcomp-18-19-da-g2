# RCOMP 2018-2019 Project - Sprint 3 - Member 1170459 folder

## Subnetting

First of all, we need to subnet the given ip addresses to each building (they are specified in the planning). The ips given to building C are:

*  172.16.68.0 255.255.255.0
*  172.16.69.0 255.255.255.0

These are already subnetted from prefix /19 to /24, although we are wasting a lot of ips. So, to the following nodes we have the respective networks subnetted:

| Subnet name  | Number of Nodes | Available Hosts | Network | Mask | Decimal Mask | Assignable Range | Broadcast |
|--------------|-----------------|-----------------|---------|------|--------------|------------------|-----------|
| Ground floor | 65 | 126 | 172.16.68.0 | /25 | 255.255.255.128 | 172.16.68.1 - 172.16.68.126 | 172.16.74.127 |
| Floor one  | 80 | 126 | 172.16.68.128 | /25 | 255.255.255.128 | 172.16.68.129 - 172.16.68.254 | 172.16.68.255 |
| Wi-Fi | 120 | 126 | 172.16.69.0 | /25 | 255.255.255.128 | 172.16.69.1 - 172.16.69.126 | 172.16.69.127 |
| DMZ | 12 | 14 | 172.16.69.128 | /28 | 255.255.255.240 | 172.16.69.129 - 172.16.69.142 | 172.16.69.143 |
| VoIP | 48 | 62 | 172.16.69.192 | /26 | 255.255.255.192 | 172.16.69.193 - 172.16.75.254 | 172.16.69.255 |

-------------------------------------------------------------

## Hostname

On the sprint 2, all the hostnames were changed in order to identify each device. Now we should name the router properly to, so we identify each one with the name "HOST-X" where "X" is the letter of the building:

    Router(config)# hostname HOST-C
    HOST-C(config)#

## Static routing

In order to each router of the buildings know all the other networks we need to configure the static routes properly. We can simplify them reducing its mask network bits to /24 prefix (255.255.255.0).

    HOST-F(config)# ip route network-address mask next-hop-address

-------------------------------------------------------------

## DHCP Service

We must configure the dhcp pools to each VLAN and for that we need to create sub-interfaces instead of using switch modules on routers.

    HOST-F(config)# interface f0/0.79
    HOST-F(config-subif)# encapsulation dot1Q 75
    HOST-F(config-subif)# ip address 172.16.68.1 255.255.255.128

This is an example for the VLAN 79 on building C. The others are all the same except for DMZ which is not needed. We assume this sub-interface is only for VLAN 70 encapsulating it and giving it an address that was previously subnetted.

Now for the devices successfully get their IP we need to configure the pools and respective addresses, minding that we need to exclude some addresses (gateway ports on this case):

    HOST-F(config)# ip dhcp pool VLAN75
    HOST-F(config-dhcp)# network 170.16.68.0 255.255.255.128
    HOST-F(config-dhcp)# default-router 170.16.68.1

For the IST services, we need to have an ip to each ip phone so we need to add a special command, enabling TFTP requests:

    HOST-F(config)# ip dhcp pool VLAN79
    HOST-F(config-dhcp)# option 150 ip 172.16.69.193
    HOST-F(config-dhcp)# default-router 172.16.69.193
    HOST-F(config-dhcp)# network 170.16.69.192 255.255.255.192

------------------------------------------------------------

## VoIP and IST services

The best part of this work is know for sure. We have to configure ip phones and for that we need to configure our IST service on the router of each building. IP phones are very tricky and we need to implement all the things carefully.

Let's start:

    HOST-F(config)# telephony-services
    HOST-F(config-telephony)# auto-reg-phone
    HOST-F(config-telephony)# ip source address 172.16.69.193 port 2000
    HOST-F(config-telephony)# max-ephones 48
    HOST-F(config-telephony)# max-dn 48
    HOST-F(config-telephony)# auto assign 1 to 2

    HOST-F(config)# ephone-dn 1
    HOST-F(config-ephone-dn)# number 935073
    HOST-F(config)#
    HOST-F(config)# ephone-dn 2
    HOST-F(config-ephone-dn)# number 935074
    HOST-F(config)#
    HOST-F(config)# ephone 1
    HOST-F(config-ephone)#
    HOST-F(config)# ephone 2
    HOST-F(config-ephone)# 

1. First of all, we enable auto-reg-phone which automatically registers a ip phone on IST service. 

2. Next we define the source address to the ip phones register successfully specifying its port (2000 is the default port)

3. Define the max o ip phones can be registered.

4. Auto assign numbers to the respective existent phones.

5. Declare the dial line and the respective numbers.

Then we define the dial-peer to interconnect the ip phones of each building:

    HOST-C(config)# dial-peer voice 10 voip
    HOST-C(config-dial-peer)# destination-pattern 93.....
    HOST-C(config-dial-peer)# session target ipv4:172.16.69.193