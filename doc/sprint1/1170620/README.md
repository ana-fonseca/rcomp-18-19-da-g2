RCOMP 2018-2019 Project - Sprint 1 - Member 1170620 folder
===========================================
(This folder is to be created/edited by the team member 1170620 only)

#### This team member will take care of building G ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170620) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.


HC's are displayed so there is the minimal cable spent, the most independency between rooms and room arrangement(like door).In explenation, they are closest as possible from floor cable passageway and removable droped cealling, there is 1 HC (when it needs outlets) so each room only depends only from that HC.

Every room that as a HC only have a switch. The switchs used only have 24 ports(2950-T), insted of 48, so there is more independency inside each room. For each HC, a Telecommunication Enclosure 19".

Most of outlets are in the walls, so there are the less cable as possible connecting to the middle of the room. They are displayed so they can cover all the room (there is always an outlet under 3m away). The number of outlets depends on it's room area, where for each 10m^2 2 outlets are needed. There was needed 2 more outlets so the access point could connect.

Access points are displayed so they can cover all the building, even the floor above once they cover 50m radius.

The only optical fiber needed is from outside to IC which will connect to MC.

Each HC is directly connected to IC in the ground floor, in the first floor only the HC in G1.2 room is connected directly to IC, and the resto of HC are connected to this one.


INVENTORY:

HC: 25

IC: 1

Telecommunication Enclosure: 26 19"

copper cable CAT7: 3959,46m

optical fiber: 8,13m

outlets: 384
