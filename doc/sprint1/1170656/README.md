RCOMP 2018-2019 Project - Sprint 1 - Member 1170656 folder
===========================================

In the ground floor, I decided to have an intermmediate connection so communication between buildings is possible. Horizontal connection is placed right next to the IC, making the E.011 a technical room.  
  ![BUILDING-E-F0](./ground_floor/BUILDING-E-F0.jpg)  
  To make sure the whole floor was covered by wi-fi, I placed 3 access points. One in the left side of this building, since its coverage is a 50 diameter, two access points in the right side of the building. On the right side, since It is an atrium, we did not place any outlets. In each room, since they are larger than 20 square meters, we put at least 6 outlets, except in the bathrooms where you don’t particularly need to place physical connections to the network.

In the first floor, since the connection to the ground floor is placed in the room E1.20, I installed two HC, since there are 24 rooms. Like in the first floor, since every room is larger than 20 square meters, I placed more than 6 outlets in every room, except the two bathrooms.  

![BUILDING-E-F1](./first_floor/BUILDING-E-F1.jpg)
Furthermore, I placed 4 access points to guarantee that every room had access to wi-fi.  

In total, we need 8511,05 meters of copper cable, 23,6 meters of optic fiber, 1 IC, 3 HC, 7 access points and 246 outlets.  

In the ground floor, I decided to have an intermmediate connection so communication between buildings is possible. Horizontal connection is placed right next to the IC, making the E.011 a technical room.  
	To make sure the whole floor was covered by wi-fi, I placed 3 access points. One in the left side of this building, since its coverage is a 50 diameter, two access points in the right side of the building. On the right side, since It is an atrium, we did not place any outlets. In each room, since they are larger than 20 square meters, we put at least 6 outlets, except in the bathrooms where you don’t particularly need to place physical connections to the network.
	In the first floor, since the connection to the ground floor is placed in the room E1.20, I installed two HC, since there are 24 rooms. Like in the first floor, since every room is larger than 20 square meters, I placed more than 6 outlets in every room, except the two bathrooms.

![inventario](inventario.jpg)

Furthermore, I placed 4 access points to guarantee that every room had access to wi-fi.
In total, we need 8511,05 meters of copper cable, 23,6 meters of optic fiber, 1 IC, 3 HC, 7 access points and 246 outlets.

