RCOMP 2018-2019 Project - Sprint 1 - Member 1170809 folder
===========================================
(This folder is to be created/edited by the team member 1170809 only)

#### This is just an example for a team member with number 1170809 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170809 will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

#
# BUILDING F - GROUND FLOOR

In the building F which has 2400m2, a connection is received from outside through fiber cable in the F0.8 room.

On the ground floor, the fiber cable connects to the Intermmediate Cross-Connect (IC) stored in the F0.8 room. This IC is composed by one switch (2960) and one patch panel.

This IC is connected to 4 Horizontal Cross-Connect (HC) stored in the rooms displayed in the image below. Three of them has two switches (2960) and patch panels and the last one has just one switch and a patch panel.

The HCs are connected to each other to assure redundancy and prevent the lost of information.

Then, two APs (Access-Points) are displayed equally on this floor to cover all the building including the floor one has well. Each one is connected to a different outlet which is connected to a implemented HC.

The amount of copper cable used in this floor is round about 2173,7 meters (including connections from HC - Outlets and IC - HC) and fiber cable is less then 5 meters. The number of outlets is precisely 92-104. Due to F0.8 room is like a datacentre, the solution is to avoid outlets there. If necessary, the maximum in this floor is 104 outlets.

The IC is likely to be composed by a single patch panel and the respective switch. A router must be used here to provide the connection from this building to the others through the Main Cross-Connect (MC).

![WALKTHROUGH_GROUND_FLOOR](/illustrations/BUILDING-F-F0-WALKTHROUGH.png)

#
# BUILDING F - FLOOR ONE

On the floor one we receive the connection through copper cables from the IC stored on ground floor.

There are 10 Horizontal Cross-Connect (HC) in order to distribute network equally to all rooms and avoiding overloading in each HC (50% free space to future upgrades). Each HC has a maximum of 2 switches (2960) + 2 patch panels of 1,75" like on ground floor. Wireless LAN coverage is provided by the APs stored on the roof of the floor below.

The Telecommunication Enclosures (TEs) or Telecommuncation Rooms (TRs) have 19" (inches) and are used to store the hardware equipment of ICs and HCs.

On this floor, we have a minimum of 234 and a maximum of 246 outlets due to F1.17 room, which is the same situation as the F0.8 room. The amount of copper cable used is round about 6652,8 meters (including connections from HC - Outlets and IC - HC).

![WALKTHROUGH_FLOOR_ONE](illustrations/BUILDING-F-F1-WALKTHROUGH.png)

Copper cables are all of the same type which is CAT6A. The reason to use this against CAT7 is due to they are cheaper and provide **almost** the same performance.

All the HCs comply with the rules and doesn't exceed 1000m2 of covered area and the distances between IC and each HC are below 500 meters.

All switches are of version 2960 (1,75" with 24 ports) and the respective patch panels composed by 24 ports as well. Although they are more expensive they provide the best performance.

The router used on the IC is of version 2911.

**INVETORY:**

    * 326-350 outlets
    * 14 Horizontal Cross-Connect (HCs)
    * 1 Intermmediate Cross-Connect (IC)
    * 14 Telecommunication Enclosures (TRs) 19"
    * Less then 5 meters of fiber cable
    * Nearly 8826,5 meters of copper cable
    * 23 switches (2960) and corresponding patch panels (1,75")
    * 1 Router (2911) for IC

![measure_ground](ground_floor.png)
![measure_one](floor_one.png)

