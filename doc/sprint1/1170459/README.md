# RCOMP 2018-2019 Project - Sprint 1 - Member 1170459 folder
---
## **STRUCTERED CABLING**
- ### BUILDING C

In the building C which has 1600m2, a connection is received from outsider through the room C0.8.

On the ground floor we receive the outsider cable through fibre cable
through room C0.8 which connects to the Intermmediate Cross-Connect (IC) stored in the same room.

This IC is connected to four Horizontal Cross-Connect. They are stored on C0.4, C0.5, C0.7 and C0.9

The HC’s are connected to each other in order to prevent lost of information in case of the hardware somehow fails to do his job.

Then two Access-Points are displayed to cover all the building, even in floor 1. Each one is connected to an outlet provided by the HC’s.

![1.png](illustrations/1.png)

On the floor 1 we receive the copper cables through the IC stored on the floor below.

Implementing 6 HC’s (on C0.4, C0.5, C0.7, C0.8, C0.11 and C0.14). Displayed like this, we can easily cover all the floor without overload the Telecommunications Rooms (TRs) of each HC leaving enough space to future upgrades.

![2.png](illustrations/2.png)

Copper cables are CAT6A. All the HC’s comply the rule not exceeding 1000m2 of coverage and the distances between the IC and the HC’s are below 500 meters.

In both floors all the enclosures (except the IC) will have 4 switches 2960 and the respective path panels trying to not overload each Telecommunication Enclosure (TR) keeping 50% of the size free to future upgrades. The IC must have a switch, a patch panel and a router (providing connection between IC and the Main Cross-Connect (MC) ).

#### **Inventory:**

  - 272-294 outlets
  - 10 HCs
  - 1 IC
  - 11 Telecommunication Enclosures 19"
  - 5 meters of fiber cable
  - 22 switches (2960) and patch panels (1,75")
  - 1 Router (2911) for IC
  - Nearly 14803 meters of copper cable (CAT6A)
