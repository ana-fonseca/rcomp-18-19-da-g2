package RCOMP;
    //cannot exceed 400 Bytes
public class Content {
    
    //cannot exceed 80 chars
    private String name;
    private String text;
    
    public Content(String name, String text){
        this.name = name;
        this.text = text;
    }
    
    public String getName(){
        return name;
    }
    
    public String getText(){
        return text;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setText(String text){
        this.text = text;
    }
}
