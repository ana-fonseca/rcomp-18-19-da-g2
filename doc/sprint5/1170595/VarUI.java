package RCOMP;

import java.util.Scanner;

public class VarUI {

    private static VarController controller = new VarController();

    public static void main(String[] args) {
        boolean toEnd = false;
        while (!toEnd) {
            try {
                System.out.print("1 - Store"
                        + "\n2 - Fetch"
                        + "\n3 - List"
                        + "\n4 - Erase"
                        + "\n-1 - End");
                Scanner in = new Scanner(System.in);
                System.out.println("");
                switch (in.nextLine()) {
                    case "1":
                        System.out.print("Name: ");
                        String username = in.nextLine();
                        System.out.println("Text: ");
                        String text = in.nextLine();
                        System.out.println("Text '" + controller.store(username, text) + "' stored.");
                        break;
                    case "2":
                        System.out.print("Name: ");
                        String username2 = in.nextLine();
                        System.out.println("Text found: " + controller.fetch(username2));
                        break;
                    case "3":
                        System.out.println("Content list: ");
                        for (String str : controller.getNameList()) {
                            System.out.println(str);
                        }
                        break;
                    case "4":
                        System.out.print("Name: ");
                        String username3 = in.nextLine();
                        System.out.println("Text erased: " + controller.erase(username3));
                        break;
                    case "-1":
                        toEnd = true;
                        break;
                    default:
                        System.out.println("Invalid option!");
                        break;
                }
                System.out.println("");
            } catch (Exception ex) {
                System.out.println("Error!");
            }
        }

    }

}
