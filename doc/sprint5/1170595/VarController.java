package RCOMP;

import java.util.List;

public class VarController {
    
    private Var var = new Var();
    
    public String store (String name, String text) {
        return var.store(name, text);
    }
    
    public String fetch (String name) throws Exception {
        return var.fetch(name);
    }
    
    public List<String> getNameList () {
        return var.getNameList();
    }
    
    public String erase (String name) throws Exception{
        return var.erase(name);
    }
    
}
