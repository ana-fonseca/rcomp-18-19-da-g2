
package stack;

import java.util.Map;
import java.util.Scanner;
import stack.Stacks;


public class Main {

    
    public static void main(String[] args) {
        
        Stacks s = new Stacks();
        
        Scanner sc = new Scanner(System.in);
        System.out.println("=================MENU=================\n"
                + "==========================================\n"
                + "1: Add content to a Stack\n"
                + "2: See and remove top element of a Stack.\n"
                + "3: List of all Stacks and number of elements.\n"
                + "==========================================\n");
        
        int opt = sc.nextInt();
        switch(opt){
            case 1:
                
                String id;
                do{
                    System.out.println("Insert a Stack ID: \n");
                    id = sc.next();
                    if(id.length() > 80){ System.out.println("INVALID ID: ID has more than 80 chars.");}
                }while(id.length() > 80);
                
                System.out.println("Insert content (String): \n");
                String content = sc.next();
                
                int size = s.push(id, content);
                System.out.println("Stack size is now: " + size + "\n");
                break;
                
            case 2:
                
                System.out.println("Insert Stack ID: \n");
                id = sc.next();
                String cnt = s.pop(id);
                System.out.println("Content: " + cnt + "\n");
                break;
            
            case 3:
                
                Map<String, Integer> stacks = s.list();
                for(String identifier : stacks.keySet()){
                    System.out.println("Stack ID: " + identifier + "\n"
                            + "Size: " + stacks.get(identifier) + "\n"); 
                }
        }
        
        
    }
    
}
