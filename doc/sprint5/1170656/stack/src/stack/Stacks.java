package stack;

import java.util.Stack;
import java.util.HashMap;
import java.util.Map;

public class Stacks {
    
    Map<String, Stack<String>> stacks = new HashMap<>();
    
    public int push(String id, String content){
        
        if(!stacks.containsKey(id)){
            System.out.println("The Stack didn't exist.\n");
            Stack<String> newStack = new Stack();
            System.out.println("Created a new Stack.\n");
            stacks.put(id, newStack);
            System.out.println("The content was added.\n");
        }else{
            stacks.get(id).push(content);
        }
        
        return stacks.get(id).size();
    }
    
    public String pop(String id){
        
        if(stacks.containsKey(id)){
            if(stacks.get(id).size() == 1){
                String element = stacks.get(id).pop();
                stacks.remove(id);
                return element;
            } 
        }
            return "Given ID from Stack does not exist";
    }
    
    public Map<String, Integer> list(){
        
        Map<String , Integer> info = new HashMap<>();
        for(String id : stacks.keySet()){
            info.put(id, stacks.get(id).size());
        }
        
        return info;
    }
       
}

