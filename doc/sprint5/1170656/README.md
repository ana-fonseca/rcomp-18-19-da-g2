RCOMP 2018-2019 Project - Sprint 5 - Member 1170656 folder
===========================================

Stack: It is a collection that is based on the last in first out (LIFO) principle. On Creation, a stack is empty. It extends Vector class with five methods that allow a vector to be treated as a stack
Each stack must have a limit of 400 bytes and an 80 char limited ID. In class Stack we have three methods which 

- **Push**- Given a stack identifier and a text content, stores it in that stack (top of the pile). If the
identifier doesn�t exist, a new stack is created. As response, returns a number representing the
stack size (number of elements) after the push operation.  
  
-  **Pop** - Given a stack identifier, returns the top element (text content) as response message, and
removes it from the stack. If no elements are left, the stack is removed. If the identifier doesn�t
exist, a suitable error message is returned.  
  
-  **List** - Returns, as response, the list of current stack identifiers, and the number of elements
present in each.

  
    
      
All INDIVIDUAL code is in the stack folder within my individual folder (1170656/stak).
