RCOMP 2018-2019 Project - Sprint 5 - Member 1170809 folder
===========================================
(This folder is to be created/edited by the team member 1170809 only)

#### This is just an example for a team member with number 1170809 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170809) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 5. This may encompass any kind of standard file types.

# TCPmonitor application #

This application is dedicated to TCP services availability monitoring. It manages a non-persitent list of TCP services to be actively checked, each is identified by a name (up to 80 characters long, case sensitive). Together with the identifier two elements are to be stored: the service's IP address and the service's TCP port number.

* For this I created an object class named TCPService and it's complete constructor has passed as arguments an identifier, a IP address and the TCP port number. This TCPService has status which is defined by an enum type (the respective values are: UNCHECKED - It wasn't checked this turn | AVAILABLE - It was checked and the connection succeeded | UNAVAILABLE - Tried to check but the connection failed);

Once the list has elements, the application performs a periodic checking of each service's availability (checking periodicity changes accordingly how many services are on queue). One minute pause between checking all services. The actual checking is simply trying to establish a TCP connection, there should be a timeout of 5 seconds, and if successful the TCP connection should then be closed with no further checking. The service is then marked as available or unavailable.

So, the main class TCPMonitor, has an UdpChat and UdpChatReceive embedded. This means that we can communicate with the core, and the core establishes all the connection between apps. So, if we type a command for an app which is not on our local workspace, then we forward a message with that command and the core program deals with it, directing it to the desired application.

The input commands of TCPMonitor are:

* **AddService** - Given an TCPService which contains an identifier, an IP address and a TCP port number, we add it to the list where are all the services to be checked. When the service exists on the list it is overwritten. If it could be added then we return a boolean type true, and if it failes returns false.

* **RemoveService** - Given an identifier's service, this function checks if the TCPService exists. If it exists then is removed and a success message (true) is returned, if not a failure message is returned (false);

* **ServiceStatus** - Returns a list with the List<TCPService>.

