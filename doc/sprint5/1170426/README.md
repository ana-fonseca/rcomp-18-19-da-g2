RCOMP 2018-2019 Project - Sprint 1 - Member 1170426 folder
===========================================
(This folder is to be created/edited by the team member 1170426 only)

#### This team member will take care of building B ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170426) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

#### UserAuthMD5 ####

This application uses files to "simulate" a database, preventing security breaches by not sending neither the password neither the AuthToken through the network, also, the user's password is never stored nor revealed to the application, rather it's its MD5 Hash version of the password that is stored and used by the application. This application uses the following two methods:

**GetAuthToken:** When receiving an username as parameter, it returns a temporary one time random String to be used in the next authentication, called AuthToken.
If an username is given, after generating the AuthToken, if the username already has an AuthToken related to him in the appointed file, the old one is replaced with the new AuthToken, otherwise it simply adds the username and the AuthToken to the file.
If no username is given, a randomly generated AuthToken is still provided but is not stored, so this AuthToken is unacceptable in the authentication.

**Authenticate:** Given an username and a validation hash, it returns true or false and then the user notified if he is either logged in or not depending whether this combination is valid according to the stored files or not. The validation hash is composed by MD5 Hash version of the concatenation AuthToken, previously generated, and the stored MD5 Hash version of the user's corresponding password. If the log in is validated, then the AuthToken used is removed from the file and so is no longer associated neither available for the user's next login.