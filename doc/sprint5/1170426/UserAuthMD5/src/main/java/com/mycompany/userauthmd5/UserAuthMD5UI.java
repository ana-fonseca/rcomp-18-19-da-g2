package com.mycompany.userauthmd5;

import java.util.Scanner;

public class UserAuthMD5UI {

    private static UserAuthMD5Controller controller = new UserAuthMD5Controller();

    public static void main(String[] args) {
        try {
            System.out.print("1 - GetAuthToken"
                    + "\n2 - Authenticate");
            Scanner in = new Scanner(System.in);
            System.out.println("");
            switch (in.nextLine()) {
                case "1":
                    System.out.print("Press Enter to skip\nUsername: ");
                    String username = in.nextLine();
                    System.out.println("Auth token: " + (username.length() == 0 ? controller.GetAuthToken() : controller.GetAuthToken(username)));
                    break;
                case "2":
                    System.out.print("Username: ");
                    String username2 = in.nextLine();
                    System.out.print("Validation Hash: ");
                    String validationHash = in.nextLine();
                    System.out.println((controller.Authenticate(username2, validationHash) ? "You have been logged in successfully" : "Log in denied! Wrong username or validation hash!"));
                    break;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        } catch (Exception ex) {
            System.out.println("Error!");
        }
    }

}
