RCOMP 2018-2019 Project - Sprint 5 planning
===========================================
### Sprint master: 1170809 ###

## General Purposes ##

* **Arithmetic program:** capable of receiving user-messages with arithmetical expressions and respond back with a message representing the corresponding arithmetical evaluation value.

* **Input commands:** input commands must be qualified with an application name, standing for which application should process it.

**Team UDP port:** 30102

----------------------------------------------------------------------------------------

## The Core Application Protocol ##

* **All messages exchanged between applications are plain text** CR (carriage return) terminated lines, and each is transported by a single UDP datagram, each message may contain several lines, however, due to UDP restrictions it’s established the maximum total size of a message is 500 bytes (CRs included).

* **Messages exchange dialogues** follow the client-server model, the dialogue is always made by two messages, a request in one direction, followed by a response in the opposite direction.

* This port number should be used always both as source port number and destination port number on every message. **Applications are supposed to ignore an incoming message if the source port number is not correct.**

* **The interpretation of the message’s content is up to the application that receives it**, if not understood a comprehensive error message should be sent in response.

* Despite the message’s content interpretation being specific for each application type (application’s name), **there are some that must be understood by every application**, let’s call those **system-messages**, thus, these system-messages will be part of the core application protocol to be defined.

* **Messages specific to an application type** will be called **user-messages**, and are understood by that application type only. Thus, for each application type, a protocol must specified regarding these messages exchange, however, the user-messages exchange must follow the core protocol specification, as described so far.

* **System-messages will encompass the mechanism by which all applications in the system become aware of each other**. System-messages are intended for the system to operate and not for final users, thus when a user interacts through input commands, system-message must not interfere and will not arise. System messages can’t get mixed together with **user-messages** (sent following user’s interaction).

----------------------------------------------------------------------------------------

## Applications awareness (making the system) ##

Every application manages a **list of active applications within the system**. This list contains application names, and for each, the corresponding IP address, the general guidelines on maintaining this list are:

* Applications are provided with a list of IP addresses, they may include broadcast addresses, multicast addresses and unicast addresses (for the latter, DNS names should supported as well). This configuration data is required, it could be provided as arguments on the command line, or for instance, through a configuration file. It shouldn’t be hardcoded.

* Applications are also provided with their own name (application type), this may be hardcoded.

* When an application starts running, it will announce itself, by sending an **announcement system-message** to each of the provided IP addresses, **the announcement system-message must include the application’s name**. In response announcement system-messages are then expected to be received, each must then be used to add/refresh data on the **list of active applications**.

* When an application is running, it may at any time receive an announcement system-message, it must be used to add/refresh date on its list of active applications. Then an announcement system-message should be sent back as response.

* Before exiting, **applications should send an exit system-message to every application on its current list of active applications**. When such a message is received by an application, the source application is removed from the **list of active applications**.

* When an application forwards a user-message to another application, there’s a **response timeout of 5 seconds**, if no response is received, the target application is to be removed from the list of active applications.

**| IMPORTANT: |**

**The core protocol has to establish the text format (syntax) of system-message, the general format of user-messages, and how they are tell apart.**

----------------------------------------------------------------------------------------

## User-messages, forwarding and failover

Once the system is operating (applications are aware of each other) users may enter input commands on any application, input commands are always qualified with an application name.

If the input command’s application name matches the local application, it’s processed locally, otherwise it must be forwarded to the matching application through a user-message, to do that the local application will search the required application’s name in its list of active applications. If not found an error message should be presented to the user (not available).

**The system should be fault tolerant**. When a user-message is forwarded to another application, that application may fail to respond (timeout), then if another application with the same name is available it should be tried next, only when no further applications with that name are available on the list, it should give up, and present the not available message to the user.

As mentioned before, when an application fails to respond to a user message request (timeout) it should also be removed from the **local list of active applications**.

----------------------------------------------------------------------------------------

## User-messages, responses to requests mapping ##

One application may send several request user-messages to another application at the same time, then the same number of response user-messages are expected to be received. Each response has to be mapped to the corresponding request, so the user receives the output corresponding to the typed input command. One possible approach for this issue could be adding a sequence number to each request issued by an application, and have it copied to the response.

----------------------------------------------------------------------------------------

## Special input commands ##

Even though they may be latter removed, for the sake of development and debugging, at this stage some special input commands should be implemented. Unlike other input commands, these will not have matching user-messages because they are meant to be processed locally by the application, thus they are not qualified with an application name.

**Status** - presents the local application’s current status, this should include the current list of active applications in the system.

**Exit** - orderly terminates the application.

If the developer finds the need for it, others may be added, because they are local, they don’t impact on protocols.

----------------------------------------------------------------------------------------

# Team Organization #

**Var** program - Álvaro Dória (1170595)
**UserAuthMD5** program - Tiago Ribeiro (1170426)
**Hash** program - Nicole Silva (1170459)
**NumCovert** program - Bruno Rodrigues (1151422)
**Stack** program - Ana Fonseca (1170656)
**TCPmonitor** program - Vitor Correia (1170809)
**Queue** program - Luís Moreira (1170620)

