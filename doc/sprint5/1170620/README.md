RCOMP 2018-2019 Project - Sprint 5 - Member 1170620 folder
===========================================

#### Queue ####

This application, the Queue, is used to insert elements at the end of the queue and removes from the beginning of the queue. It follows FIFO concept. This application uses the following four methods:


**Enqueue:** When receiving an name (unique) and text as parameter, if the name isn't registed and is valid it creats a new entry(queue) with the name and the text received. If the name alredy exit it adds to that queue the text.

**Dequeue:** Given the identifier (name) by parameter, it removes the last element in that queue.

**Remove:** Remove an entry(queue) identified by the name received by parameter.

**List:** Return a list all the queues names with the number of elements inside of each.

## Notes ##

Each queue is identified by a name (up to 80 characters long, case sensitive).
Each element’s contents.
Size is limited to 400 bytes (CRs included). A queue is a First-In-First-Out (FIFO) data structure.
This application manages  non persistent queues of elements.