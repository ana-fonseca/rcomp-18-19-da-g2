package Queue;

import java.util.ArrayList;
import java.util.List;

public class Queue {//cannot exceed 400 Bytes
    //cannot exceed 80 chars

    private String name;
    private List<String> text = new ArrayList<>();

    public Queue(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<String> getText() {
        return text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int addText(String textAux) {
        this.text.add(textAux);
        return this.text.size();
    }

    public String dequeue() {
        int size = sizeQueue();
        return text.remove(size - 1);
    }

    public int sizeQueue() {
        return this.text.size();
    }
}
