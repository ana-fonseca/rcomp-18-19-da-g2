package Queue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueueController {

    private static final String APPLICATION_NAME = "queue";

    private static List<Queue> contentList = new ArrayList<>();

    private static final int MAX_SIZE_NAME = 80;
    private static final int MAX_SIZE_TEXT = 400;
    private static final String QUEUE_NOT_EXIT = "That Queue doens't exit! \n";

    public static int enqueue(String name, String text) {

        int index = 0;
        if (text.getBytes().length > MAX_SIZE_TEXT) {
            return -1;
        } else if (name.length() > MAX_SIZE_NAME) {
            return -1;
        } else {
            boolean qExist = false;

            for (Queue queue : QueueController.contentList) {
                if (queue.getName().equals(name)) { // name is case sensitive
                    index = queue.addText(text);
                    qExist = true;
                }
            }
            if (!qExist) {
                Queue otherQueue = new Queue(name);
                index = otherQueue.addText(text);
                contentList.add(otherQueue);
            }
        }
        return index;
    }

    public static String dequeue(String name) {
        for (Queue queue : QueueController.contentList) {
            if (queue.getName().equals(name)) {
                return queue.dequeue();
            }
        }
        return QUEUE_NOT_EXIT;
    }

    public static String remove(String name) {
        boolean qExists = false;
        Queue queueFound = null;
        for (Queue queue : QueueController.contentList) {
            if (queue.getName().equals(name)) {
                qExists = true;
                queueFound = queue;
                break;
            }
        }
        if (!qExists) {
            return QUEUE_NOT_EXIT;
        } else {
            contentList.remove(queueFound);
        }
        return "Queue removed! \n";
    }

    public static Map<String, Integer> list() {
        Map<String, Integer> informationMap = new HashMap<>();
        for (Queue queue : QueueController.contentList) {
            informationMap.put(queue.getName(), queue.sizeQueue());
        }
        return informationMap;
    }

    public static String getAPPLICATION_NAME() {
        return APPLICATION_NAME;
    }

    public static String getEnqueue() {
        return "enqueue";
    }

    public static String getDequeue() {
        return "dequeue";
    }

    public static String getRemove() {
        return "remove";
    }

    public static String getList() {
        return "list";
    }

}
