package Queue;

import java.util.Scanner;

public class QueueUI {

    private static final String MENU_QUEUE = "=========MENU=========\n"
            + "| 1 - enqueue         |\n"
            + "| 2 - dequeue         |\n"
            + "| 3 - remove          |\n"
            + "| 4 - list            |\n"
            + "| 0 - sair            |\n"
            + "======================\n";
    private static Scanner sc = new Scanner(System.in);
    private static String name;

    public static void queueUI() {

        int opt;

        boolean ciclo = true;

        while (ciclo) {
            System.out.println(MENU_QUEUE + "Qual é a sua opção? ");
            opt = sc.nextInt();

            switch (opt) {
                case 1:
                    QueueUI.enqueueUI();
                    break;
                case 2:
                    QueueUI.dequeueUI();
                    break;
                case 3:
                    QueueUI.removeUI();
                    break;
                case 4:
                    QueueUI.listUI();

                    break;
                case 0:
                    ciclo = false;
                    break;
                default:
                    System.out.println("Opção inválida");
                    break;
            }
        }

    }

    public static void enqueueUI() {
        System.out.println("Name? ");
        name = sc.next();
        System.out.println("Text? ");
        String text = sc.next();
        QueueController.enqueue(name, text);
    }

    public static void dequeueUI() {
        System.out.println("Name? ");
        name = sc.next();
        QueueController.dequeue(name);
    }

    public static void removeUI() {
        System.out.println("Name? ");
        name = sc.next();
        QueueController.remove(name);
    }

    public static void listUI() {
        QueueController.list();
    }

}
