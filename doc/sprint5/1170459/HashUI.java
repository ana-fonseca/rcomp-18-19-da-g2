/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.util.Scanner;

/**
 *
 * @author becas
 */
public class HashUI {


    public static void main(String[] args) {
        try {
            System.out.print("1 - Digest"
                    + "\n2 - List");
            Scanner in = new Scanner(System.in);
            System.out.println("");
            switch (in.nextLine()) {
                case "1":
                    System.out.print("Write id and content");
                    String[] array = in.nextLine().split(" ");
                    System.out.println("The Hash algorithm is " + Hash.digest(array[0], array[1]));
                    break;
                case "2":
                    for(int i = 0; i < Hash.getListHashCodes().size(); i++){
                        System.out.println(Hash.getListHashCodes().get(i)+ "\n");
                    }
                    break;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        } catch (Exception ex) {
            System.out.println("Error!");
        }
    }
}
