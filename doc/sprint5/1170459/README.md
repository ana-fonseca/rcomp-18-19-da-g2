RCOMP 2018-2019 Project - Sprint 5 - Member 1170459 folder
===========================================
(This folder is to be created/edited by the team member 1170459 only)

## Hash Application ##

This application is dedicated to calculate hash codes from provided data, and it supports various hash algorithms (MD5, SHA-0, SHA-256).

The main class Hash, has an UdpChat and UdpChatReceive embedded. This means this app can communicate with the core. The core establishes all the communication between apps. If we type a command and that app is inexistent from our local workspace, the command is forwarded to the core command which will encompass the command to the desired application. 

The communication is simple, if an '@' symbol is on the first position of the message, it means that the application is locally saved. If the commands are separated by '!' then the application we wish to communicate is externally online (it can be offline and a failed message is shown).

The input commands of Hash are:

* **Digest** - Given an identifier (which defines the algorithm to be used) and a text content used as input, it will calculate the corresponding hash code and return it back. The response is a set of bits (number of bits varies depending on the algorithm used). If the requested algorithm is not supported, a suitable error message should be returned instead.

* **List** - Return as response the list of identifiers for supported hash algorithms.