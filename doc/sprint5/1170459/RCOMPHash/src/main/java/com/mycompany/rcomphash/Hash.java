package com.mycompany.rcomphash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

public class Hash {

    public static String digest(String id, String text) {
        String hash = "";
        try {
            MessageDigest md = MessageDigest.getInstance(id);
            md.update(text.getBytes());
            hash = DatatypeConverter.printHexBinary(md.digest()).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, null, e);
        }
        return hash;
    }

    public static List<String> getListHashCodes() {
        List<String> list = new ArrayList<>();
        list.add("SHA-1");
        list.add("MD5");
        list.add("MD2");
        list.add("RawDSA");
        list.add("RSA");
        list.add("DSA");
        list.add("MD5/RSA");
        list.add("MD2/RSA");
        list.add("SHA-1/RSA");
        list.add("DES");
        list.add("IDEA");
        list.add("RC2");
        list.add("RC4");
        return list;
    }
}
