/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp;

import java.util.Scanner;

/**
 *
 * @author bfaro
 */
public class NumConvert {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int num;
        String id;
        
        
        System.out.println("Diga para que formato quer converter o inteiro\n"
                + "são aceites: bin, octa, hex");
        //scan id
        id = entrada.nextLine();
        System.out.println("Diga o num que quer converter");
        //scan num
        num = entrada.nextInt();
        
        if (id.equals("bin")) {
            System.out.println(integerToBinary(num));
        }else if (id.equals("octa")) {
            System.out.println(integerToOctal(num));
        }else if (id.equals("hex")) {
            System.out.println(integerToHex(num));
        }else{
            System.out.println("Identificador formato inválido");
        }
    }
    
    
     public static int binaryToInteger(String binary) {
        char[] numbers = binary.toCharArray();
        int result = 0;
        for(int i=numbers.length - 1; i>=0; i--)
            if(numbers[i]=='1')
            result += Math.pow(2, (numbers.length-i - 1));
        return result;
}
    
    public static String integerToOctal(int num){
        return (Integer.toOctalString(num));
    }
    
    public static String integerToBinary(int num){
        return (Integer.toBinaryString(num));
    }
    
    public static String integerToHex(int num){
        return (Integer.toHexString(num));
    }
    
}
