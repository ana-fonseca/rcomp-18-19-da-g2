

package rcomp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumConvertMonitor {

    private static final String BCAST_ADDR = "255.255.255.255";

    private static final int SERVICE_PORT = 30102;

    private static final int TIMEOUT = 5;

    private static final String ADD_SERVICE = "tcpMonitor addService";

    private static final String REMOVE_SERVICE = "tcpMonitor removeService";

    private static final String SERVICES_STATUS = "tcpMonitor servicesStatus";
    
    private static Map<InetAddress, String> applicationsOn = new HashMap<>();
    
    
    public static synchronized void addIp(InetAddress ip, String identifier) {
        applicationsOn.put(ip, identifier);
    }

    public static synchronized void remIp(InetAddress ip) {
        applicationsOn.remove(ip);
    }

    public static synchronized void printIPs() {
        for (Map.Entry<InetAddress, String> entry : applicationsOn.entrySet())
            System.out.println(" " + entry.getKey() + "-" + entry.getValue());
    }

    public static void sendToAll(DatagramSocket s, DatagramPacket p) throws IOException {
        for (Map.Entry<InetAddress, String> entry : applicationsOn.entrySet()) {
            p.setAddress(entry.getKey());
            s.send(p);
        }
    }

    public static void sendToApp(DatagramSocket s, DatagramPacket p, String message, InetAddress ip) throws IOException {
        byte[] data = message.getBytes();

        p.setData(data);
        p.setLength(data.length);
        p.setAddress(ip);

        s.send(p);
    }

    static List<TCPService> services = new ArrayList<>();

    static boolean exists(TCPService service) {
        return services.contains(service);
    }
    
    static InetAddress bcastAddress;
    static DatagramSocket sock;

    public static void main(String[] args) throws IOException, InterruptedException {
        String message;
        byte[] data;
        DatagramPacket udpPacket;

        try {
            sock = new DatagramSocket(SERVICE_PORT);
        } catch (SocketException e) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        bcastAddress = InetAddress.getByName(BCAST_ADDR);
        sock.setBroadcast(true);

        data = "@tcpMonitor".getBytes();
        udpPacket = new DatagramPacket(data, data.length, bcastAddress, SERVICE_PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new UdpChatReceiveMonitor(sock));
        udpReceiver.start();

        while (true) {
            System.out.println("Write a command: ");
            message = in.readLine();

            String[] commandInfo = message.split(" ");

            if (message.equalsIgnoreCase(CoreApp.EXIT))
                break;
            else if (message.equalsIgnoreCase(CoreApp.STATUS)) {
                System.out.println("Active applications: ");
                printIPs();
                System.out.print("\n");

            } else if (message.equalsIgnoreCase(ADD_SERVICE))
                new TCPMonitorUI().menuAddService();
            else if (message.equalsIgnoreCase(REMOVE_SERVICE))
                new TCPMonitorUI().menuRemoveService();
            else if (message.equalsIgnoreCase(SERVICES_STATUS))
                new TCPMonitorUI().menuServiceStatus();
            else if (commandInfo[0].equalsIgnoreCase(CoreApp.VAR)) {
                for (Map.Entry<InetAddress, String> entry : applicationsOn.entrySet()) {

                    if (entry.getValue().equalsIgnoreCase(CoreApp.VAR)) {

                        if ("store".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("fetch".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("list".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("erase".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.USER_AUTH_MD5)) {

                        if ("getAuthToken".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("authenticate".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.HASH)) {

                        if ("digest".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("list".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.NUM_CONVERT)) {

                        if ("convert".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("list".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.STACK)) {

                        if ("push".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("pop".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("list".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.TCP_MONITOR)) {

                        if ("addService".equalsIgnoreCase(commandInfo[1])) {
                            String command = "!" + commandInfo[1];
                            for(String input : new CoreApp().getTcpMonitorAddServiceInput())
                                command += "!" + input;
                            sendToApp(sock, udpPacket, command, entry.getKey());
                        } else if ("removeService".equalsIgnoreCase(commandInfo[1])) {
                            String command = "!" + commandInfo[1] + "!" + new CoreApp().getTcpMonitorRemoveServiceInput();
                            sendToApp(sock, udpPacket, command, entry.getKey());
                        } else if ("servicesStatus".equalsIgnoreCase(commandInfo[1])) {
                            String command = "!" + commandInfo[1];
                            sendToApp(sock, udpPacket, command, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else if (entry.getValue().equalsIgnoreCase(CoreApp.QUEUE)) {

                        if ("enqueue".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("dequeue".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("remove".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else if ("list".equalsIgnoreCase(commandInfo[1])) {
                            sendToApp(sock, udpPacket, message, entry.getKey());
                        } else
                            System.out.println("The command doesn't exist");
                    } else
                        System.out.println("The command doesn't exist");
                }

                data[0] = 0;

                udpPacket.setData(data);
                udpPacket.setLength(1);

                sendToAll(sock, udpPacket);

                sock.close();

                udpReceiver.join();
            }
        }
    }
}

class UdpChatReceiveMonitor implements Runnable {

    private DatagramSocket s;

    public UdpChatReceiveMonitor(DatagramSocket udpSocket)  {
        this.s = udpSocket;
    }

    public static void sendToApp(DatagramSocket s, DatagramPacket p, String message, InetAddress ip) throws IOException {
        byte[] command = message.getBytes();

        p.setData(command);
        p.setLength(message.length());
        p.setAddress(ip);

        s.send(p);
    }

    @Override
    public void run()   {
        byte[] data = new byte[500];
        StringBuilder message;
        DatagramPacket p;

        p = new DatagramPacket(data, data.length);

        while(true) {
            p.setLength(data.length);

            try {
                s.receive(p);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(data[0] == '@')  {
                String dataS = new String(data, StandardCharsets.UTF_8);
                String[] tmp = dataS.split("@");

                TCPMonitor.addIp(p.getAddress(), tmp[1]);

                try {
                    s.send(p);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(data[0] == 0) {
                String dataS = new String(data, StandardCharsets.UTF_8);
                String[] tmp = dataS.split("!");

                if(tmp[1].equalsIgnoreCase("addService"))   {
                    message = new StringBuilder(Boolean.toString(new TCPMonitorController().addService(tmp[2], tmp[3], Integer.parseInt(tmp[4]))));
                    try {
                        sendToApp(s, p, message.toString(), p.getAddress());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(tmp[1].equalsIgnoreCase("removeService")) {
                    message = new StringBuilder(Boolean.toString(new TCPMonitorController().removeService(tmp[2])));
                    try {
                        sendToApp(s, p, message.toString(), p.getAddress());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(tmp[1].equalsIgnoreCase("servicesStatus"))    {
                    message = new StringBuilder();

                    for(TCPService service : new TCPMonitorController().servicesStatus())
                        message.append(service.toString());
                    try {
                        sendToApp(s, p, message.toString(), p.getAddress());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    
}
