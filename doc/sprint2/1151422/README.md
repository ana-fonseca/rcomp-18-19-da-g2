RCOMP 2018-2019 Project - Sprint 2 - Member 1151422 folder
===========================================

This is the report from the Sprint 2 where the objective of this sprint is to manipulate/master the layer 2 configuration devices (mostly switches).

On this report the configurations will be described showing all the commands used and their purposes.

On the first hand, the topology should be created implementing all the devices (switches, access-points, ip phones, laptops, pcs and servers).
  
    The switches used on this topology are PT-empty switches which provides higher manageability in order to choose the ports we want (Fast-Ethernet, Gigabit-Ethernet, etc...).

    Access-Points used are the common ones (Access-Point PT).

    IP phones are of 7960 model.

    Laptops are equipped with fast-ethernet port, but we must switch to a wireless adapter (WPC300N) in order to connect to the access-points available.

    Normal PCs (desktop) end devices.

    Normal Server.

Each one of these devices has an unique VLAN.

* VLAN 95 - Ground floor outlets.
* VLAN 96 - Floor 1 outlets.
* VLAN 97 - Access-Point (AP).
* VLAN 98 - DMZ Server (Server).
* VLAN 99 - VoIP (IP phone).

Then, we should connect all the devices. First we connect the switch which represents the Intermmediate Cross-Connect (IC) to all the Horizontal Cross-Connect (HC) and we connect all the HCs to each other to assure **redundancy**. Redundancy is important because if a port or a cable somehow has a problem, the switch can handle the information through another route preventing loss of information.

The module ports used on the IC are the NM-1CGE (gigabit-ethernet) and one of NM-1FGE (fiber gigabit-ethernet). The one that uses optical fiber is the connection between the Main Cross-Connect (MC) and IC. The other ones are used to connect to the rest of the HCs. Although they're more expensive than Fast-Ethernet ports, these provides an higher speed and there are not much to be used on these project.

The modules ports used on the HC are the NM-1CFE (fast-ethernet) and one of NM-1CGE (gigabit-ethernet). The gigabit one is to connect the respective IC and the rest of the fast-ethernet ports are used to end devices and redundancy between HCs. Using gigabit modules on all switches would be more expensive so using fast-ethernet ports is cheaper and provides the same speed and reliability in this type of topology.

Connection between switches or between same devices uses cross-over copper cables.

The next step is the configurations on all switches which is the main objective of this project:

* Hostname 
* VLAN Trunking Protocol (VTP) 
* VLANs  
* Spanning Tree Protocol (STP) 

## Hostname

First we define the hostname to the respective switches which helps to identify each one. So we must have the right permissions to do that.

    Switch> enable
    Switch# configure terminal
    Switch(config)# hostname HC-F0.1


This is an example how we should do it to the HC stored on the F0.1 room.

## VLAN Trunking Protocol (VTP)

In order to avoid the exhausting configuration we should define the VTP server and the VTP clients. All the configurations related to VLANs will be implemented automatically through this protocol.

First we should define the VTP domain (the example below shows the domain name used on this building):

    Switch> enable
    Switch# configure terminal
    Switch(config)# vtp domain building-f-domain
    
Then we assure that the IC is the server using:

    Switch(config)# vtp mode server

Finally we check the changes we did:

    Switch(config)# exit
    Switch# show vtp status

On the client switches (HCs) only one configuration is different:

    Switch(config)# vtp mode client

## VLANs

First we create the vlans that are given to us (in case of building F is between 70 and 74):

    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 70
    Switch(config-vlan)# name GROUND-FLOOR
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 71
    Switch(config-vlan)# name FLOOR-ONE
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 72
    Switch(config-vlan)# name ACCESS-POINT
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 73
    Switch(config-vlan)# name DMZ
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 74
    Switch(config-vlan)# name VOIP

Then we assign the respective vlans to the ports to pcs:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f5/1-f9/1
    Switch(config-if-range)# switchport mode access
    Switch(config-if-range)# switchport access vlan 70 (71 in case of floor 1)

To the AP:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f5/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 72

To the DMZ Server:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 73

Finally, to the IP phone:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 74

In order to the VTP works the ports which are connected to the server should be in trunk mode (including switch-to-switch redundancy):

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f1/1-f4/1
    Switch(config-if-range)# switchport mode trunk

## Spanning Tree Protocol (STP)

Per-VLAN Spanning Tree (PVST) maintains a spanning tree instance for each VLAN configured in the network. It uses ISL Trunking and allows a VLAN trunk to be forwarding for some VLANs while blocking for other VLANs. Since PVST treats each VLAN as a separate network, it has the ability to load balance traffic (at layer-2) by forwarding some VLANs on one trunk and other Vlans on another trunk without causing a Spanning Tree loop.

    Switch> enable
    Switch# configure terminal
    Switch(config)# spanning-tree mode pvst
	
## Altera��es relativamente ao sprint passado:
- Alguns HC's foram removidos para evitar a necessidade de outro IC por piso.
- Os Acess Points do primeiro piso foram removidos, ficando apenas a haver AP no Ground Floor

## Configs
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
 switchport mode trunk
!
interface FastEthernet4/1
 switchport mode trunk
!
interface FastEthernet5/1
 switchport mode trunk
!
interface FastEthernet6/1
 switchport mode trunk
!
interface FastEthernet7/1
 switchport mode trunk
!
interface FastEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
 switchport mode trunk
!
interface FastEthernet4/1
 switchport mode trunk
!
interface FastEthernet5/1
 switchport mode trunk
!
interface FastEthernet6/1
 switchport mode trunk
!
interface FastEthernet7/1
 switchport mode trunk
!
interface FastEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
 switchport access vlan 99
!
interface GigabitEthernet6/1
 switchport access vlan 95
!
interface FastEthernet7/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 99
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
 switchport access vlan 95
!
interface GigabitEthernet4/1
 switchport access vlan 95
!
interface GigabitEthernet5/1
 switchport access vlan 95
!
interface GigabitEthernet6/1
 switchport access vlan 95
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end
---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
 switchport access vlan 95
!
interface GigabitEthernet5/1
 switchport access vlan 95
!
interface GigabitEthernet6/1
 switchport access vlan 95
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
 switchport access vlan 95
!
interface GigabitEthernet5/1
 switchport access vlan 95
!
interface GigabitEthernet6/1
 switchport access vlan 95
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 99
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 95
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

---------

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
 switchport access vlan 99
!
interface GigabitEthernet8/1
 switchport access vlan 95
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end