# RCOMP 2018-2019 Project - Sprint 2 planning

The objective is to master layer 2 (switches and access points) although some end devices will be added in order for the **sprint 3**.

**NOTE: LOGICAL LAYOUT ONLY**

-----------------------------------------------------------------------------

**STEPS TO FOLLOW:**

* Each cross-connect (ICs, HCs and also CPs) is going to be represented by a layer 2 switch (PT-empty switch models, and add them the required port types); 

* Connection between switches -> Crossover cables (A - B);

* Connection between different devices -> Direct cables (A - A);

* All ICs and HCs are connected to each other to assure redundancy. Multiple cable links can be represented by a single connection in the simulation;

--------------------------------------------------------------------------------

**VLANs:**

* 1 VLAN for ground floor;
* 1 VLAN for floor one;
* 1 VLAN for access-points;
* 1 VLAN for local servers and administration workstations (DMZ);
* 1 VLAN for VoIP;
    
    
        Interconnections must be configured in trunk-mode with all VLANs (switchport mode trunk)
        
        VLANs must be configured in all switches.

--------------------------------------------------------------------------------

**Spanning Tree Protocol (STP):**

    Check if STP is enabled on all switches (DON'T DISABLE IT).

--------------------------------------------------------------------------------

**END DEVICES:**

* A PC connected to the VLAN on ground floor;
* A PC connected to the VLAN on the floor one;
* A Wireless laptop connected to a Wireless Access-Point (not a router) connected to the VLAN associated to the wireless connection;
* A server connected to the DMZ VLAN;
* A VoIP phone connected to the VoIP VLAN.

Configure switchport mode access in all ports (switchport mode access) and assign it to the correct VLANs (switchport access vlan _number-vlan/name-vlan_);

For VoIP phones (7960 model), the port on the switch should be configured in access mode as well. The access VLAN must be disabled (no switchport access vlan) and the voice VLAN must be used instead (switch voice vlan _number-vlan/name-vlan_).

--------------------------------------------------------------------------------

**VLAN Trunking Protocol (VTP):**

* All switches must have the same VTP domain name (vtp domain _domain-name_);
* All switches except one must be in VTP client mode (vtp mode client);
* One switch will be in VTP server mode (VLAN database  must be manually defined here) - vtp mode server;
* One VTP server per building on higher cross-connect;

--------------------------------------------------------------------------------

![image](image.png)