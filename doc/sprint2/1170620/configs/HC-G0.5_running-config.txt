!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-G0.5
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet6/1
 switchport access vlan 60
!
interface GigabitEthernet7/1
 switchport access vlan 62
!
interface GigabitEthernet8/1
 switchport access vlan 64
!
interface GigabitEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

