RCOMP 2018-2019 Project - Sprint 2 - Member 1170620 folder
===========================================
(This folder is to be created/edited by the team member 1170620 only)

The owner of this folder (member number 1170620) will commit here all the outcomes (results/artifacts/products) of his/her work during sprint 2. This may encompass any kind of standard file types.

#Logical View#

![logicalView.png](logicalView.png)

#VLAN for G buildinf#

 Number | Name           
________|________________
   60   | GFloor0Outlet  
   61   | GFloor1Outlet  
   62   | GWIFI          
   63   | GDMZ           
   64   | GVoIP          
   100  | CampusBackBone

#Switchs#

	The domain name used is: vtdag2 
	Saved all the running configuration
	Switchs used: PT-Empty (have 10 ports and which provides higher manageability in order to choose the ports we want (Fast-Ethernet, Gigabit-Ethernet, etc...))
	

##	IC##

	Used a fiber port (type of port in configuration) to connect the outside fiber cable to this building. The rest are port to copper cable.
	Changed every port to HC to trunk so all the VLANs can go to HCs insted of only one (in acess).
	They are in Server mode.
	
	Once there are more then 10 HC that the IC need to connect, there is a HC that works as an intermediate (HC-G0.2) to the others HCs.

##	HC##

	Changed the VLAN that connect to each device (ex: to port 60 in case of floor 0 and it was a PC)
	Only have ports to copper cables (type of port in configuration)
	
#Equipment#

	There is a PC connect to a HC for each room (Normal PCs (desktop) end devices).
	A Normal server in B0.2.
	Some IP phones
	A laptop connect to an AP (equipped with fast-ethernet port, but we must switch to a wireless adapter (WPC300N) in order to connect to the access-points available).
	Two Access Point(AP - 7960 model).
	
#Configuration#

Then, we should connect all the devices. First we connect the switch which represents the Intermmediate Cross-Connect (IC) to all the Horizontal Cross-Connect (HC) and we connect all the HCs to each other to assure **redundancy**. Redundancy is important because if a port or a cable somehow has a problem, the switch can handle the information through another route preventing loss of information.

The module ports used on the IC are the NM-1CGE (gigabit-ethernet) and one of NM-1FGE (fiber gigabit-ethernet). The one that uses optical fiber is the connection between the Main Cross-Connect (MC) and IC. The other ones are used to connect to the rest of the HCs. Although they're more expensive than Fast-Ethernet ports, these provides an higher speed and there are not much to be used on these project.

The modules ports used on the HC are the NM-1CFE (fast-ethernet) and one of NM-1CGE (gigabit-ethernet). The gigabit one is to connect the respective IC and the rest of the fast-ethernet ports are used to end devices and redundancy between HCs. Using gigabit modules on all switches would be more expensive so using fast-ethernet ports is cheaper and provides the same speed and reliability in this type of topology.

Connection between switches or between same devices uses cross-over copper cables.

The next step is the configurations on all switches which is the main objective of this project:

* Hostname 
* VLAN Trunking Protocol (VTP) 
* VLANs  
* Spanning Tree Protocol (STP) 

## Hostname

First we define the hostname to the respective switches which helps to identify each one. So we must have the right permissions to do that.

    Switch> enable
    Switch# configure terminal
    Switch(config)# hostname HC-G0.2


This is an example how we should do it to the HC stored on the G0.2 room.

## VLAN Trunking Protocol (VTP)

In order to avoid the exhausting configuration we should define the VTP server and the VTP clients. All the configurations related to VLANs will be implemented automatically through this protocol.

First we should define the VTP domain (the example below shows the domain name used on this building):

    Switch> enable
    Switch# configure t
    Switch(config)# vtp domain vtdag2
    
Then we assure that the IC is the server using:

    Switch(config)# vtp mode server

Finally we check the changes we did:

    Switch(config)# exit
    Switch# show vtp status

On the client switches (HCs) only one configuration is different:

    Switch(config)# vtp mode client

## VLANs

First we create the vlans that are given to us (in case of building G is between 60 and 64):

    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 60
    Switch(config-vlan)# name GFloor0Outlet
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 61
    Switch(config-vlan)# name GFloor1Outlet
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 62
    Switch(config-vlan)# name GWIFI
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 63
    Switch(config-vlan)# name GDMZ
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 64
    Switch(config-vlan)# name GVoIP

Then we assign the respective vlans to the ports to each equipment by changing in the switch the vlan each port is connected.

Or it can be writen by(example), to the PC::

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f5/1-f9/1
    Switch(config-if-range)# switchport mode access
    Switch(config-if-range)# switchport access vlan 70 (71 in case of floor 1)

To the AP:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f5/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 72

To the DMZ Server:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 73

Finally, to the IP phone:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 74

In order to the VTP works the ports which are connected to the server should be in trunk mode (including switch-to-switch redundancy):

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f1/1-f4/1
    Switch(config-if-range)# switchport mode trunk

## Spanning Tree Protocol (STP)

Per-VLAN Spanning Tree (PVST) maintains a spanning tree instance for each VLAN configured in the network. It uses ISL Trunking and allows a VLAN trunk to be forwarding for some VLANs while blocking for other VLANs. Since PVST treats each VLAN as a separate network, it has the ability to load balance traffic (at layer-2) by forwarding some VLANs on one trunk and other Vlans on another trunk without causing a Spanning Tree loop.

    Switch> enable
    Switch# configure terminal
    Switch(config)# spanning-tree mode pvst