RCOMP 2018-2019 Project - Sprint 2 - Member 1170595 folder
 ===========================================

(This folder is to be created/edited by the team member 1170595 only)
 

#### This is just an example for a team member with number 1170595 ####
 ### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1170595) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 2. This may encompass any kind of standard file types.
This is the report from the Sprint 2 where the objective of this sprint is to manipulate/master the layer 2 configuration devices (mostly switches).

On this report the configurations will be described showing all the commands used and their purposes.

On the first hand, the topology should be created implementing all the devices (switches, access-points, ip phones, laptops, pcs and servers).
  
    The switches used on this topology are PT-empty switches which provides higher manageability in order to choose the ports we want (Fast-Ethernet, Gigabit-Ethernet, etc...).

    Access-Points used are the common ones (Access-Point PT).

    IP phones are of 7960 model.

    Laptops are equipped with fast-ethernet port, but we must switch to a wireless adapter (WPC300N) in order to connect to the access-points available.

    Normal PCs (desktop) end devices.

    Normal Server.

Each one of these devices has an unique VLAN.

* VLAN 90 - Ground Floor end-devices.
* VLAN 91 - Floor One end-devices.
* VLAN 92 - Access-Point (AP).
* VLAN 93 - DMZ Server (Server).
* VLAN 94 - VoIP (IP phone).

Then, we should connect all the devices. First we connect the IC with the rest of the HC in the building with the CGE port for the rest of the building we have an HC for each room
so that we can connect all the devices in the building with HC in the room.
Building A is also responsable for the datacenter where it is located the main cross connect this connects to all the IC in the rest of the campus with FFE connection some elements of the group told
to be FGE because they are faster even though they are more expensive but i decided to put FGE ports since i was the responsable for the project assimilation, so for all the cases
it is my responsability this decision. Redundancy was taken in consideration but not always represented in the packet tracer. Redundancy  is important because if a port or a cable somehow has a problem,
 the switch can handle the information through another route preventing loss of information.

The next step is the configurations on all switches which is the main objective of this project:

* Hostname 
* VLAN Trunking Protocol (VTP) 
* VLANs  
* Spanning Tree Protocol (STP) 

## Hostname

First we define the hostname to the respective switches which helps to identify each one. So we must have the right permissions to do that.

    Switch> enable
    Switch# configure terminal
    Switch(config)# hostname HC-F0.1


This is an example how we should do it to the HC stored on the F0.1 room.

## VLAN Trunking Protocol (VTP)

In order to avoid the exhausting configuration we should define the VTP server and the VTP clients. All the configurations related to VLANs will be implemented automatically through this protocol.

First we should define the VTP domain (the example below shows the domain name used on this building):

    Switch> enable
    Switch# configure terminal
    Switch(config)# vtp domain building-f-domain
    
Then we assure that the IC is the server using:

    Switch(config)# vtp mode server

Finally we check the changes we did:

    Switch(config)# exit
    Switch# show vtp status

On the client switches (HCs) only one configuration is different:

    Switch(config)# vtp mode client

## VLANs

First we create the vlans that are given to us (in case of building F is between 70 and 74):

    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 70
    Switch(config-vlan)# name GROUND-FLOOR
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 71
    Switch(config-vlan)# name FLOOR-ONE
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 72
    Switch(config-vlan)# name ACCESS-POINT
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 73
    Switch(config-vlan)# name DMZ
    Switch(config-vlan)# exit
    Switch(config)#
    Switch(config)# vlan 74
    Switch(config-vlan)# name VOIP

Then we assign the respective vlans to the ports to pcs:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f5/1-f9/1
    Switch(config-if-range)# switchport mode access
    Switch(config-if-range)# switchport access vlan 70 (71 in case of floor 1)

To the AP:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f5/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 72

To the DMZ Server:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 73

Finally, to the IP phone:

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface f4/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 74

In order to the VTP works the ports which are connected to the server should be in trunk mode (including switch-to-switch redundancy):

    Switch> enable
    Switch# configure terminal
    Switch(config)# interface range f1/1-f4/1
    Switch(config-if-range)# switchport mode trunk

## Spanning Tree Protocol (STP)

Per-VLAN Spanning Tree (PVST) maintains a spanning tree instance for each VLAN configured in the network. It uses ISL Trunking and allows a VLAN trunk to be forwarding for some VLANs while blocking for other VLANs. Since PVST treats each VLAN as a separate network, it has the ability to load balance traffic (at layer-2) by forwarding some VLANs on one trunk and other Vlans on another trunk without causing a Spanning Tree loop.

    Switch> enable
    Switch# configure terminal
    Switch(config)# spanning-tree mode pvst

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname MC
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
!
interface FastEthernet1/1
!
interface FastEthernet2/1
!
interface FastEthernet3/1
!
interface FastEthernet4/1
!
interface FastEthernet5/1
!
interface FastEthernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-0.1
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
 switchport access vlan 92
!
interface GigabitEthernet7/1
 switchport access vlan 92
!
interface GigabitEthernet8/1
 switchport access vlan 90
!
interface GigabitEthernet9/1
 switchport access vlan 94
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-0.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 90
!
interface GigabitEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-0.3
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
!
interface GigabitEthernet9/1
 switchport access vlan 90
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-1.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 93
!
interface GigabitEthernet9/1
 switchport access vlan 91
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-1.1
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
 switchport access vlan 91
!
interface GigabitEthernet9/1
 switchport access vlan 94
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-A1.5
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
!
interface GigabitEthernet9/1
 switchport access vlan 91
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

