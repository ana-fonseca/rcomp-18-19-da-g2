!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-B1.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 2-1001
!
interface GigabitEthernet1/1
 switchport trunk allowed vlan 2-1001
 switchport mode trunk
!
interface GigabitEthernet2/1
 switchport access vlan 86
!
interface GigabitEthernet3/1
 switchport access vlan 87
!
interface GigabitEthernet4/1
 switchport access vlan 89
!
interface GigabitEthernet5/1
 switchport access vlan 85
!
interface GigabitEthernet6/1
 switchport access vlan 85
!
interface GigabitEthernet7/1
 switchport access vlan 85
!
interface GigabitEthernet8/1
 switchport access vlan 85
!
interface GigabitEthernet9/1
 switchport trunk allowed vlan 2-1001
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

