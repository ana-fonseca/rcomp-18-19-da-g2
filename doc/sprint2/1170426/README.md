RCOMP 2018-2019 Project - Sprint 1 - Member 1170426 folder
===========================================
After the last sprint, one HC was added and one of the access points was transfered from the ground floor to the floor one.

This is the report from the Sprint 2 where the objective of this sprint is to manipulate/master the layer 2 configuration devices (mostly switches).

On the first hand, the topology should be created implementing all the devices (switches, access-points, ip phones, laptops, pcs and servers).
  
    The switches used on this topology are PT-empty switches which provides higher manageability in order to choose the ports we want (Fast-Ethernet, Gigabit-Ethernet, etc...).

    Access-Points used are the common ones (Access-Point PT).

    IP phones are of 7960 model.

    Laptops are equipped with fast-ethernet port, but we must switch to a wireless adapter (WPC300N) in order to connect to the access-points available.

    Normal PCs (desktop) end devices.

    Normal Server.

Each one of these devices has an unique VLAN.

* VLAN 85 - Ground Floor end-devices.
* VLAN 86 - Floor One end-devices.
* VLAN 87 - Access-Point (AP).
* VLAN 88 - DMZ Server (Server).
* VLAN 89 - VoIP (IP phone).

Then, we should connect all the devices. First we connect the switch which represents the Intermmediate Cross-Connect (IC) to all the Horizontal Cross-Connect (HC) and we connect all the HCs to each other to assure **redundancy**. Redundancy is important because if a port or a cable somehow has a problem, the switch can handle the information through another route preventing loss of information.

The module ports used on the IC are the NM-1CGE (gigabit-ethernet) and one of NM-1FGE (fiber gigabit-ethernet). The one that uses optical fiber is the connection between the Main Cross-Connect (MC) and IC. The other ones are used to connect to the rest of the HCs. Although they're more expensive than Fast-Ethernet ports, these provides an higher speed and there are not much to be used on these project.

The modules ports used on the HC are the NM-1CFE (fast-ethernet) and one of NM-1CGE (gigabit-ethernet). The gigabit one is to connect the respective IC and the rest of the fast-ethernet ports are used to end devices and redundancy between HCs. Using gigabit modules on all switches would be more expensive so using fast-ethernet ports is cheaper and provides the same speed and reliability in this type of topology.

Connection between switches or between same devices uses cross-over copper cables.

IC config:
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname IC-B
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport mode trunk
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
 switchport mode trunk
!
interface GigabitEthernet7/1
 switchport trunk allowed vlan 2-1001
 switchport mode trunk
!
interface GigabitEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport access vlan 100
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

HC B0.2 config:
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-B0.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport access vlan 89
!
interface GigabitEthernet1/1
 switchport access vlan 89
!
interface GigabitEthernet2/1
 switchport access vlan 86
 switchport trunk allowed vlan 2-1001
 switchport mode trunk
!
interface GigabitEthernet3/1
 switchport access vlan 86
!
interface GigabitEthernet4/1
 switchport access vlan 84
!
interface GigabitEthernet5/1
 switchport access vlan 84
!
interface GigabitEthernet6/1
 switchport access vlan 84
!
interface GigabitEthernet7/1
 switchport access vlan 84
!
interface GigabitEthernet8/1
 switchport access vlan 84
!
interface GigabitEthernet9/1
 switchport trunk allowed vlan 2-1001
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

HC B1.2 config:
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-B1.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 2-1001
!
interface GigabitEthernet1/1
 switchport trunk allowed vlan 2-1001
 switchport mode trunk
!
interface GigabitEthernet2/1
 switchport access vlan 86
!
interface GigabitEthernet3/1
 switchport access vlan 87
!
interface GigabitEthernet4/1
 switchport access vlan 89
!
interface GigabitEthernet5/1
 switchport access vlan 85
!
interface GigabitEthernet6/1
 switchport access vlan 85
!
interface GigabitEthernet7/1
 switchport access vlan 85
!
interface GigabitEthernet8/1
 switchport access vlan 85
!
interface GigabitEthernet9/1
 switchport trunk allowed vlan 2-1001
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

HC B1.5 config:
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HC-B1.5
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
!
interface GigabitEthernet1/1
!
interface GigabitEthernet2/1
 switchport trunk allowed vlan 2-1001
 switchport mode trunk
!
interface GigabitEthernet3/1
 switchport access vlan 85
!
interface GigabitEthernet4/1
 switchport access vlan 89
!
interface GigabitEthernet5/1
 switchport access vlan 85
!
interface GigabitEthernet6/1
 switchport access vlan 85
!
interface GigabitEthernet7/1
 switchport access vlan 85
!
interface GigabitEthernet8/1
 switchport access vlan 85
!
interface GigabitEthernet9/1
 switchport trunk allowed vlan 2-1001
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

