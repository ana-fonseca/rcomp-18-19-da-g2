package UserAuthMD5;

public class UserAuthMD5Controller {
    
    private UserAuthMD5 userAuth = new UserAuthMD5();
    
    public String GetAuthToken (String username) throws Exception {
        return userAuth.GetAuthToken(username);
    }
    
    public String GetAuthToken () {
        return userAuth.GetAuthToken();
    }
    
    public boolean Authenticate (String username, String validationHash) throws Exception {
        return userAuth.Authenticate(username, validationHash);
    }
    
}
