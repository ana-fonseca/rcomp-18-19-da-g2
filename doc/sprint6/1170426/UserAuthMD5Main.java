package UserAuthMD5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;

class UserAuthMD5Main {

    private static final String BCAST_ADDR = "255.255.255.255";

    private static final int SERVICE_PORT = 30102;

    private static final String STATUS = "status";

    private static final String EXIT = "exit";

    static final String GET_AUTH_TOKEN = "userAuthMD5 getAuthToken";

    static final String AUTHENTICATE = "userAuthMD5 authenticate";

    private static Map<InetAddress, String> applicationsOn = new HashMap<>();

    static synchronized void addIp(InetAddress ip, String identifier) {
        applicationsOn.put(ip, identifier);
    }

    static synchronized void remIp(InetAddress ip) {
        applicationsOn.remove(ip);
    }

    public static synchronized void printIPs() {
        System.out.println("Online Applications:");
        for(Map.Entry<InetAddress, String> entry : applicationsOn.entrySet())
            System.out.println(entry.getValue() + " - " + entry.getKey().toString());
    }

    private static void sendToAll(DatagramSocket s, DatagramPacket p) throws IOException {
        for (Map.Entry<InetAddress, String> entry : applicationsOn.entrySet()) {
            p.setAddress(entry.getKey());
            s.send(p);
        }
    }

    static void sendToApp(DatagramSocket s, DatagramPacket p, String message, InetAddress ip) throws IOException {
        byte[] data = message.getBytes();

        p.setData(data);
        p.setLength(data.length);
        p.setAddress(ip);

        s.send(p);
    }

    static List<UserAuthMD5> authMD5List = new ArrayList<>();

    static boolean exists(UserAuthMD5 authMD5) {
        return authMD5List.contains(authMD5);
    }

    private static InetAddress bcastAddress;

    private static DatagramSocket sock;

    public static void main(String[] args) throws IOException, InterruptedException {
        String message;
        byte[] data;
        DatagramPacket udpPacket;

        try { sock = new DatagramSocket(SERVICE_PORT); }
        catch (IOException ex)  {
            System.out.println("Failed to open local port");
            System.out.println(1); }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        bcastAddress = InetAddress.getByName(BCAST_ADDR);
        sock.setBroadcast(true);

        data = "@userAuthMD5".getBytes();

        udpPacket = new DatagramPacket(data, data.length, bcastAddress, SERVICE_PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new UdpChatReceiveMonitor(sock));
        udpReceiver.start();

        while(true) {
            System.out.println("Write a command: ");
            message = in.readLine();

            if(message.equalsIgnoreCase(EXIT))
                break;
            else if(message.equalsIgnoreCase(STATUS))    {
                printIPs();
            } else if(message.equalsIgnoreCase(GET_AUTH_TOKEN))
                new UserAuthMD5UI().getAuthToken();
            else if(message.equalsIgnoreCase(AUTHENTICATE))
                new UserAuthMD5UI().authenticate();
            else {
                udpPacket.setData(message.getBytes());
                udpPacket.setLength(message.length());


            }
        }

        data = "#tcpMonitor".getBytes();
        udpPacket.setData(data);
        udpPacket.setLength(data.length);

        sendToAll(sock, udpPacket);

        sock.close();

        udpReceiver.join();
    }
}

class UdpChatReceiveMonitor implements Runnable {

    private DatagramSocket sock;

    private static final char PEER_START = '@';

    private static final char PEER_END = '#';

    private static final String SEPARATOR = " ";

    UdpChatReceiveMonitor(DatagramSocket socket)  {
        this.sock = socket;
    }

    @Override
    public void run() {
        byte[] data = new byte[500];
        String message;
        DatagramPacket packet;
        InetAddress currAddress;

        packet = new DatagramPacket(data, data.length);

        while(true) {
            packet.setLength(data.length);

            try { sock.receive(packet); }
            catch (IOException ex) { return; }

            currAddress = packet.getAddress();

            if(data[0] == PEER_START)    {
                UserAuthMD5Main.addIp(packet.getAddress(), Arrays.toString(data).substring(1));
                try { sock.send(packet); }
                catch (IOException e) { e.printStackTrace(); }

            } else if(data[0] == PEER_END)   {
                UserAuthMD5Main.remIp(packet.getAddress());
                try { sock.send(packet); }
                catch (IOException e) { e.printStackTrace(); }

            } else {
                message = new String(packet.getData(), 0,  packet.getLength());

                UserAuthMD5Controller controller = new UserAuthMD5Controller();
                if(message.substring(0, UserAuthMD5Main.GET_AUTH_TOKEN.length()).equalsIgnoreCase(UserAuthMD5Main.GET_AUTH_TOKEN))    {
                    String command = message.substring(UserAuthMD5Main.GET_AUTH_TOKEN.length());

                    String newMessage = "";
                    boolean flag = true;
                    switch (command.length()){
                        case 0:
                            newMessage = controller.GetAuthToken();
                            break;
                        default:
                            String[] args = command.split(SEPARATOR);
                            try {
                                newMessage = controller.GetAuthToken(args[0]);
                                flag=true;
                            } catch (Exception e) {
                                flag = false;
                            }
                            break;
                    }

                    try {
                        if(flag)
                            UserAuthMD5Main.sendToApp(sock, packet, "AuthToken: " + newMessage + "\n", currAddress);
                        else UserAuthMD5Main.sendToApp(sock, packet, "Failed saving AuthToken to storage.\n", currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(message.substring(0, UserAuthMD5Main.AUTHENTICATE.length()).equalsIgnoreCase(UserAuthMD5Main.AUTHENTICATE))  {
                    String command = message.substring(UserAuthMD5Main.AUTHENTICATE.length());

                    String[] args = command.split(SEPARATOR);
                    boolean flag = false;
                    try {
                        flag = controller.Authenticate(args[0], args[1]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if(flag)
                            UserAuthMD5Main.sendToApp(sock, packet, "Successfully authenticated. (UserAuthMD5)\n", currAddress);
                        else UserAuthMD5Main.sendToApp(sock, packet, "The authentication failed! (UserAuthMD5)\n", currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }
}
