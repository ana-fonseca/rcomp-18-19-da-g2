package UserAuthMD5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

public class UserAuthMD5 {

    private static final File USER_FILES = new File("userFiles.csv"); //Username,MD5(password)
    private static final File AUTH_FILES = new File("authFile.csv"); //Username,AuthToken

    private static final int MAX_AUTH = 128;
    private static final int ALPHABET_LENGTH = 26;
    private static final int UNITS_LENGTH = 10;

    public String GetAuthToken(String username) throws Exception {
        String authToken = GetAuthToken();

        List<String> lineList = authFileList(username);
        lineList.add(username + "," + authToken);
        
        if (redoAuthFile(lineList)) {
            return authToken;
        } else {
            throw new Exception("No access to the file!");
        }
    }

    public String GetAuthToken() {
        String authToken = "";
        Random random = new Random();
        for (int i = 0; i < MAX_AUTH; i++) {
            int randomInt = random.nextInt(3);
            switch (randomInt) {
                case 0:
                    authToken += (char) (random.nextInt(UNITS_LENGTH) + 48);
                    break;
                case 1:
                    authToken += (char) (random.nextInt(ALPHABET_LENGTH) + 65);
                    break;
                case 2:
                    authToken += (char) (random.nextInt(ALPHABET_LENGTH) + 97);
                    break;
            }
        }
        return authToken;
    }

    public boolean Authenticate(String username, String validationHash) throws Exception {
        Scanner inAuth = new Scanner(AUTH_FILES);
        Scanner inPasswords = new Scanner(USER_FILES);

        String authToken = "";
        String md5Password = "";

        while (inAuth.hasNextLine()) {
            String[] line = inAuth.nextLine().split(",");
            if (line[0].equals(username)) {
                authToken = line[1];
                break;
            }
        }

        while (inPasswords.hasNextLine()) {
            String[] line = inPasswords.nextLine().split(",");
            if (line[0].equals(username)) {
                md5Password = line[1];
                break;
            }
        }

        if (authToken.isEmpty() || md5Password.isEmpty() || !validationHash.equals(MD5(md5Password + authToken))) {
            return false;
        } else {
            if (!redoAuthFile(authFileList(username))) {
                throw new Exception("No access to the file!");
            }
            return true;
        }
    }

    private String MD5(String string) {
        String hash = "";

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            hash = DatatypeConverter.printHexBinary(md.digest()).toLowerCase();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserAuthMD5.class.getName()).log(Level.SEVERE, null, ex);
        }

        return hash;
    }

    private List<String> authFileList(String username) throws FileNotFoundException {
        if (!AUTH_FILES.exists()){
            return new ArrayList<>();
        }
        Scanner in = new Scanner(AUTH_FILES);
        
        List<String> lineList = new ArrayList<>();
        while (in.hasNextLine()) {
            String line = in.nextLine();
            if (!line.split(",")[0].equals(username) && !line.isEmpty()) {
                lineList.add(line);
            }
        }
        in.close();

        return lineList;
    }

    private boolean redoAuthFile(List<String> lineList) throws IOException {
        try {
            if (AUTH_FILES.exists()){
                if (!AUTH_FILES.delete()){
                    return false;
                }
                FileWriter writer = new FileWriter(AUTH_FILES);
                for (String line : lineList) {
                    writer.append(line + "\r\n");
                }
                writer.close();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
