package UserAuthMD5;

import java.util.Scanner;

public class UserAuthMD5UI {
    
    private static UserAuthMD5Controller controller = new UserAuthMD5Controller();
    
    public void getAuthToken() {
        Scanner in = new Scanner(System.in);
        System.out.print("Press Enter to skip\nUsername: ");
        String username = in.nextLine();
        try {
            System.out.println("Auth token: " + (username.length() == 0 ? controller.GetAuthToken() : controller.GetAuthToken(username)));
        } catch (Exception ex) {
            System.out.println("Error!");
        }
    }

    public void authenticate() {
        Scanner in = new Scanner(System.in);
        System.out.print("Username: ");
        String username = in.nextLine();
        System.out.print("Validation Hash: ");
        String validationHash = in.nextLine();
        try {
            System.out.println((controller.Authenticate(username, validationHash) ? "You have been logged in successfully" : "Log in denied! Wrong username or validation hash!"));
        } catch (Exception ex) {
            System.out.println("Error!");
        }
    }
    
}
