/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.util.Scanner;

/**
 *
 * @author becas
 */
public class VarUI {
     private VarController controller = new VarController();

    public void store() {
        Scanner in = new Scanner(System.in);
        System.out.print("Name: ");
        String username = in.nextLine();
        System.out.println("Text: ");
        String text = in.nextLine();
        System.out.println("Text '" + controller.store(username, text) + "' stored.");
    }

    public void fetch() {
        Scanner in = new Scanner(System.in);
        System.out.print("Name: ");
        String username = in.nextLine();
        try {
            System.out.println("Text found: " + controller.fetch(username));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void list() {
        System.out.println("Name list: ");
        for (String str : controller.getNameList()) {
            System.out.println(str);
        }
    }

    public void erase() {
        Scanner in = new Scanner(System.in);
        System.out.print("Name: ");
        String username = in.nextLine();
        try {
            System.out.println("Text erased: " + controller.erase(username));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
