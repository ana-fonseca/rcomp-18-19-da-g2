package Var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;

class VarMain {

    private static final String BCAST_ADDR = "255.255.255.255";

    private static final int SERVICE_PORT = 30102;

    private static final String STATUS = "status";

    private static final String EXIT = "exit";

    static final String STORE = "var store";

    static final String FETCH = "var fetch";

    static final String LIST = "var list";

    static final String ERASE = "var erase";

    private static Map<InetAddress, String> applicationsOn = new HashMap<>();

    static synchronized void addIp(InetAddress ip, String identifier) {
        applicationsOn.put(ip, identifier);
    }

    static synchronized void remIp(InetAddress ip) {
        applicationsOn.remove(ip);
    }

    public static synchronized void printIPs() {
        System.out.println("Online Applications:");
        for(Map.Entry<InetAddress, String> entry : applicationsOn.entrySet())
            System.out.println(entry.getValue() + " - " + entry.getKey().toString());
    }

    private static void sendToAll(DatagramSocket s, DatagramPacket p) throws IOException {
        for (Map.Entry<InetAddress, String> entry : applicationsOn.entrySet()) {
            p.setAddress(entry.getKey());
            s.send(p);
        }
    }

    static void sendToApp(DatagramSocket s, DatagramPacket p, String message, InetAddress ip) throws IOException {
        byte[] data = message.getBytes();

        p.setData(data);
        p.setLength(data.length);
        p.setAddress(ip);

        s.send(p);
    }

    static List<Var> varList = new ArrayList<>();

    static boolean exists(Var var) {
        return varList.contains(var);
    }

    private static InetAddress bcastAddress;

    private static DatagramSocket sock;

    public static void main(String[] args) throws IOException, InterruptedException {
        String message;
        byte[] data;
        DatagramPacket udpPacket;

        try { sock = new DatagramSocket(SERVICE_PORT); }
        catch (IOException ex)  {
            System.out.println("Failed to open local port");
            System.out.println(1); }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        bcastAddress = InetAddress.getByName(BCAST_ADDR);
        sock.setBroadcast(true);

        data = "@var".getBytes();

        udpPacket = new DatagramPacket(data, data.length, bcastAddress, SERVICE_PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new UdpChatReceiveMonitor(sock));
        udpReceiver.start();

        while(true) {
            System.out.println("Write a command: ");
            message = in.readLine();

            if(message.equalsIgnoreCase(EXIT))
                break;
            else if(message.equalsIgnoreCase(STATUS))    {
                printIPs();
            } else if(message.equalsIgnoreCase(STORE))
                new VarUI().store();
            else if(message.equalsIgnoreCase(FETCH))
                new VarUI().fetch();
            else if(message.equalsIgnoreCase(LIST))
                new VarUI().list();
            else if(message.equalsIgnoreCase(ERASE))
                new VarUI().erase();
            else {
                udpPacket.setData(message.getBytes());
                udpPacket.setLength(message.length());


            }
        }

        data = "#tcpMonitor".getBytes();
        udpPacket.setData(data);
        udpPacket.setLength(data.length);

        sendToAll(sock, udpPacket);

        sock.close();

        udpReceiver.join();
    }
}

class UdpChatReceiveMonitor implements Runnable {

    private DatagramSocket sock;

    private static final char PEER_START = '@';

    private static final char PEER_END = '#';

    private static final String SEPARATOR = " ";

    UdpChatReceiveMonitor(DatagramSocket socket)  {
        this.sock = socket;
    }

    @Override
    public void run() {
        byte[] data = new byte[500];
        String message;
        DatagramPacket packet;
        InetAddress currAddress;

        packet = new DatagramPacket(data, data.length);

        while(true) {
            packet.setLength(data.length);

            try { sock.receive(packet); }
            catch (IOException ex) { return; }

            currAddress = packet.getAddress();

            if(data[0] == PEER_START)    {
                VarMain.addIp(packet.getAddress(), Arrays.toString(data).substring(1));
                try { sock.send(packet); }
                catch (IOException e) { e.printStackTrace(); }

            } else if(data[0] == PEER_END)   {
                VarMain.remIp(packet.getAddress());
                try { sock.send(packet); }
                catch (IOException e) { e.printStackTrace(); }

            } else {
                message = new String(packet.getData(), 0,  packet.getLength());

                VarController controller = new VarController();
                if(message.substring(0, VarMain.STORE.length()).equalsIgnoreCase(VarMain.STORE))    {
                    String command = message.substring(VarMain.STORE.length());

                    String newMessage = "";
                    String[] args = command.split(SEPARATOR);
                    boolean flag = true;
                    newMessage = controller.store(args[0], args[1]);

                    try {
                        if(flag)
                            VarMain.sendToApp(sock, packet, "Text " + newMessage + "stored.\n", currAddress);
                        else VarMain.sendToApp(sock, packet, "Text was not stored.\n", currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(message.substring(0, VarMain.FETCH.length()).equalsIgnoreCase(VarMain.FETCH))  {
                    String command = message.substring(VarMain.FETCH.length());

                    String[] args = command.split(SEPARATOR);
                    boolean flag = false;
                    String newMessage = "";
                    try {
                        newMessage = controller.fetch(args[0]);
                        flag = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        if(flag)
                            VarMain.sendToApp(sock, packet, "Text found: " + newMessage + ".\n", currAddress);
                        else VarMain.sendToApp(sock, packet, "No text was found.\n", currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(message.substring(0, VarMain.LIST.length()).equalsIgnoreCase(VarMain.LIST)) {
                    List<String> list = controller.getNameList();
                    String newMessage = "Name List:\n";

                    for (String name : list){
                        newMessage += name + "\n";
                    }

                    try {
                        VarMain.sendToApp(sock, packet, newMessage, currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(message.substring(0, VarMain.ERASE.length()).equalsIgnoreCase(VarMain.ERASE))  {
                    String command = message.substring(VarMain.ERASE.length());

                    String[] args = command.split(SEPARATOR);
                    boolean flag = false;
                    String newMessage = "";
                    try {
                        newMessage = controller.erase(args[0]);
                        flag = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        if(flag)
                            VarMain.sendToApp(sock, packet, "Content erased: " + newMessage + ".\n", currAddress);
                        else VarMain.sendToApp(sock, packet, "No content was erased.\n", currAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
