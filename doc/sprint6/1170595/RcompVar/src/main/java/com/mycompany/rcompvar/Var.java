/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author becas
 */
public class Var {
     private List<Content> contentList = new ArrayList<>();

    public String store(String name, String text) {
        int count = 0;

        for (int k = 0; k < contentList.size(); k++) {
            Content content = contentList.get(k);
            if (content.getName().equalsIgnoreCase(name)) {
                content.setText(text);
                count = 1;
            }
            if (text.isEmpty()) {
                contentList.remove(content);
            }
        }
        if (count == 0) {
            contentList.add(new Content(name, text));
        }
        return text;
    }

    public String fetch(String name) throws Exception {
        for (int k = 0; k < contentList.size(); k++) {
            Content content = contentList.get(k);
            if (content.getName().equalsIgnoreCase(name)) {
                return content.getText();
            }
        }
        throw new Exception("Error fetching content!");
    }

    public List<String> getNameList() {
        List<String> list = new ArrayList<>();
        int count = 0;

        for (int k = 0; k < contentList.size(); k++) {
            Content content = contentList.get(k);
            list.add(content.getName());
            count = count + content.getName().length();
            if (count > 400) {
                list.remove(content.getName());
                return list;
            }
        }
        return list;
    }

    public String erase(String name) throws Exception {
        for (int k = 0; k < contentList.size(); k++) {
            Content content = contentList.get(k);
            if(content.getName().equalsIgnoreCase(name)){
                String text = content.getText();
                contentList.remove(content);
                return text;
            }
        }
        throw new Exception("Error erasing content!");
    }
}
