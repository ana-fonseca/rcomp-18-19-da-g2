/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

/**
 *
 * @author becas
 */
public class Content {
      //cannot exceed 80 chars
    private String name;
    private String text;
    
    public Content(String name, String text){
        this.name = name;
        this.text = text;
    }
    
    public String getName(){
        return name;
    }
    
    public String getText(){
        return text;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setText(String text){
        this.text = text;
    }
}
