/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Var;

import java.util.List;

/**
 *
 * @author becas
 */
public class VarController {
    
    private static Var var = new Var();

    public String store(String name, String text) {
        return var.store(name, text);
    }

    public String fetch(String name) throws Exception {
        return var.fetch(name);
    }

    public List<String> getNameList() {
        return var.getNameList();
    }

    public String erase(String name) throws Exception {
        return var.erase(name);
    }
}
