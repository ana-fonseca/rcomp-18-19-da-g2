RCOMP 2018-2019 Project - Sprint 4 - Member 1151422 folder
===========================================

## ROUTER Configuration

!                                                            
version 12.4 
no service timestamps log datetime msec 
no service timestamps debug datetime msec 
no service password-encryption 
! 
hostname Router 
! 
! 
! 
! 
ip dhcp excluded-address 172.16.78.1 
ip dhcp excluded-address 172.16.78.65 
ip dhcp excluded-address 172.16.77.129 
ip dhcp excluded-address 172.16.70.1 
ip dhcp excluded-address 172.16.71.1 
ip dhcp excluded-address 172.16.77.193 
! 
ip dhcp pool net60 
 network 172.16.78.0 255.255.255.192 
 default-router 172.16.78.1 
 dns-server 172.16.78.2 
 domain-name buildingG 
ip dhcp pool net61 
 network 172.16.78.64 255.255.255.192 
 default-router 172.16.78.65 
 dns-server 172.16.78.2 
 domain-name buildingG 
ip dhcp pool net62 
 network 172.16.71.128 255.255.255.224 
 default-router 172.16.71.129 
 dns-server 172.16.71.2 
 domain-name buildingG 
ip dhcp pool net63 
 network 172.16.77.0 255.255.255.128 
 default-router 172.16.77.1 
ip dhcp pool net64 
 network 172.16.76.160 255.255.255.224 
 default-router 172.16.76.161 
 option 150 ip 172.16.76.161 
ip dhcp pool net100 
 network 172.16.77.192 255.255.255.192 
 default-router 172.16.76.162 
! 
! 
! 
ip cef 
no ipv6 cef 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
spanning-tree mode pvst 
! 
! 
! 
! 
! 
! 
interface FastEthernet0/0 
 no ip address 
 ip nat inside 
 duplex auto 
 speed auto 
! 
interface FastEthernet0/0.60 
 encapsulation dot1Q 60 
 ip address 172.16.76.1 255.255.255.192 
! 
interface FastEthernet0/0.61 
 encapsulation dot1Q 61 
 ip address 172.16.76.65 255.255.255.192 
! 
interface FastEthernet0/0.62 
 encapsulation dot1Q 62 
 ip address 172.16.76.129 255.255.255.224 
! 
interface FastEthernet0/0.63 
 encapsulation dot1Q 63 
 ip address 172.16.77.1 255.255.255.128 
! 
interface FastEthernet0/0.64 
 encapsulation dot1Q 64 
 ip address 172.16.76.161 255.255.255.224 
! 
interface FastEthernet0/0.100 
 encapsulation dot1Q 100 
 ip address 172.16.77.193 255.255.255.192 
! 
interface FastEthernet0/1 
 ip address 190.5.200.50 255.255.255.252 
 duplex auto 
 speed auto 
! 
interface FastEthernet0/1/0 
 switchport mode access 
 switchport nonegotiate 
! 
interface FastEthernet0/1/1 
 switchport mode trunk 
 switchport nonegotiate 
! 
interface FastEthernet0/1/2 
 switchport mode access 
 switchport nonegotiate 
! 
interface FastEthernet0/1/3 
 switchport mode access 
 switchport nonegotiate 
! 
interface Vlan1 
 no ip address 
 shutdown 
! 
router ospf 7 
 log-adjacency-changes 
 network 172.16.77.192 0.0.0.63 area 0 
 network 172.16.76.0 0.0.0.63 area 7 
 network 172.16.76.64 0.0.0.63 area 7 
 network 172.16.76.128 0.0.0.31 area 7 
 network 172.16.76.160 0.0.0.31 area 7 
 network 172.16.77.0 0.0.0.127 area 7 
 network 172.24.77.192 0.0.0.63 area 7 
! 
ip nat inside source static tcp 172.16.77.3 80 190.5.200.50 80  
ip nat inside source static tcp 172.16.77.3 443 190.5.200.50 443    
ip nat inside source static tcp 172.16.77.2 53 190.5.200.50 53   
ip nat inside source static udp 172.16.77.2 53 190.5.200.50 53   
ip classless  
ip route 172.16.64.0 255.255.255.252 FastEthernet0/1/1   
ip route 172.16.65.0 255.255.255.252 FastEthernet0/1/1   
!  
ip flow-export version 9  
!  
!  
!  
!  
!  
!  
!  
!  
telephony-service  
 max-ephones 2  
 max-dn 2  
 ip source-address 172.16.76.161 port 2000  
 auto assign 1 to 2  
 auto assign 2 to 1  
!  
ephone-dn 1  
 number 905073  
!  
ephone-dn 2  
 number 905074  
!  
ephone 1  
 device-security-mode none  
 mac-address 0006.2A68.8416  
 type 7960  
 button 1:1  
!  
ephone 2  
 device-security-mode none  
 mac-address 0001.9638.12AE  
 type 7960  
 button 1:2  
!  
line con 0  
!  
line aux 0  
!  
line vty 0 4  
 login  
!  
!  
!  
end  



## OSPF (Open Shortest Path First) dynamic routing ##

First of all we will establish the OSPF dynamic routing which is fast and easy to test and the main objective to the communication needed in the building and between buildings.

The first step is to **delete the static routing table configured (except the default one)** on sprint 3 and implement OSPF routing. The OSPF area used for Building G is area id 6.
The OSPF works very efficiently because each router informs other routers about the network around it.

This is how its configured:

    HOST-G(config)# router OSPF 7
    HOST-G(config-router)#

**PROCESS-ID** is used to identify this specific configuration, and makes it possible to have several OSPF configurations running at the same time on the same router bu this works as an internal identifier and it has nothing to do with OSPF areas.


Now we must assign local networks to our OSPF configuration:

    HOST-G(config-router)# network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER

So there it is a pratical example used:

    HOST-G(config)# router OSPF 7
    HOST-G(config-router)#
    HOST-G(config-router)# network 172.16.74.0 0.0.0.63 area 7
    HOST-G(config-router)#
    HOST-G(config-router)# network 172.16.74.128 0.0.0.127 area 7
    HOST-G(config-router)#
    HOST-G(config-router)# network 172.16.75.0 0.0.0.127 area 7
    HOST-G(config-router)#
    HOST-G(config-router)# network 172.16.74.64 0.0.0.15 area 7
    HOST-G(config-router)#
    HOST-G(config-router)# network 172.16.75.128 0.0.0.127 area 7

-----------------------------------------------------------------------------

## HTTP (HyperText Transfer Protocol) ##

Then, we must add a new server that will hold an HTTP service. We just need to add a configure the HTML page identifying which building is stored the HTTP server.

The IPv4 given to this server was: 172.16.74.67/28

-----------------------------------------------------------------------------

## DNS (Domain Name System) ##

The member who has the building A on his charge will create a DNS domain name matching the team's repository name (rcomp-18-19-da-g2). This the highest level domain, so it's going to be used as if it was the DNS root domain.

In case of the Building G, the local DNS domain is **building-G.rcomp-18-19-da-g2**. 

This DNS server should have the unqualified DNS name **ns**, so for Building G it will be **ns.building-G.rcomp-18-19-da-g2**.

DNS database - Building G:
| Record Type | Record name | Record value |
|:-:|:-:|:-:|
| NS  | .  | ns  |
| A  | ns  | 172.16.65.2  |
| NS | building-G.rcomp-18-19-da-g2 | ns.building-G.rcomp-18-19-da-g2 |
| A | ns.building-G.rcomp-18-19-da-g2 | 172.16.74.66 |
| A | server7 | 172.16.74.67 |
| CNAME | web | server7 |
| CNAME | www | server7 |
| CNAME | dns | ns |

-----------------------------------------------------------------------------

## NS (Name Server) records and glue records ##

The NS record itself: the record’s name is the domain’s name and the record’s data is the DNS
name of the name server of that domain.

The A record for the NS record: the record’s name is the DNS name of the name server and
the record’s data is the IPv4 address the name server.

-----------------------------------------------------------------------------

## Other DNS records ##

All HTTP servers are to be named server7 (A record), it’s ok they all have the same name because they belong to different DNS domains.

Within each DNS domain there should be a www alias (CNAME) mapped to the same domain’s server7 A record, and another alias, named web also mapped to the domain’s server7 A record.

One additional alias (CNAME), with name dns, and mapped to the domain’s ns A record, should also
exist.

-----------------------------------------------------------------------------

## DNS client's configuration (end-nodes) ##

All end-nodes within a building should be using the local DNS name server, and, if supported, have the default DNS domain set to the local DNS domain.

For end-nodes with automatic DHCP configuration, that information should be inserted into the local DHCP pools:

    HOST-G(config)# ip dhcp pool net60
    HOST-G(dhcp-config)# dns-server 172.16.74.66
    HOST-G(dhcp-config)#
    HOST-G(dhcp-config)# domain-name building-G.rcomp-18-19-da-g2

On the other hand, on DMZ servers (which don't have dhcp) that should be defined manually.

------------------------------------------------------------------------------

## NAT (Network Address Translation) ##

On this network static NAT will be applied not the dynamic one. So we can use it to redirect traffic.

The specified configurations are:

* HTTP and HTTPS requests received in the router's backbone interface are redirected to the **HTTP server in the local DMZ**. Both use TCP connections, assume the default service ports numbers are 80 and 443 respectively.

* DNS requests received in the router's backbone interface are redirected to the DNS server in the local DMZ. The DNS service may use either UDP or TCP, but in either case the default service port number is 53.

The executed commands were:

    HOST-G(config)# ip nat outside source static tcp 172.16.74.67 80 12.0.0.1 80
    HOST-G(config)#
    HOST-G(config)# ip nat outside source static tcp 172.16.74.67 443 12.0.0.1 443
    HOST-G(config)#
    HOST-G(config)# ip nat outside source static udp 172.16.74.66 53 12.0.0.1 53

-----------------------------------------------------------------------------------

## ACLs (Access Control Lists) ##

First of all we assure that we don't have any configured access lists:

        HOST-G(config)# no access-list 100
        HOST-G(config)# no access-list 110

Now, we configure the port of the intranet and the port directly connected to the backbone with the ACL's. We will use extended ACL's because are well defined than standard ones. With extended ACLs we can declare the protocol, the respective port and source/destination ip addresses.

On port fastEthernet 0/0:

* Block Internal Spoofing:

        HOST-G(config)# access-list 100 permit ip 172.16.74.0 0.0.1.255 any

* Allow all ICMP echo requests and echo replies:

        HOST-G(config)# access-list 100 permit icmp any 172.16.74.0 0.0.1.255 echo
        HOST-G(config)# access-list 100 permit icmp any 172.16.74.0 0.0.1.255 echo-reply

* All trafic from the DMZ is allowed:

        HOST-G(config)# access-list 100 ip 172.16.74.64 0.0.0.15 any

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-G(config)# access-list 100 permit udp any 172.16.74.0 0.0.0.64 eq 67
        HOST-G(config)# access-list 100 permit udp any 172.16.75.0 0.0.0.128 eq 67

        HOST-G(config)# access-list 100 permit udp 172.16.74.0 0.0.0.64 eq 67 any
        HOST-G(config)# access-list 100 permit udp 172.16.75.0 0.0.0.128 eq 67 any

        (Same commands for udp port 68)

        HOST-G(config)# access-list 100 permit udp any 172.16.75.128 0.0.0.127 eq 69
        HOST-G(config)# access-list 100 permit udp 172.16.75.128 0.0.0.127 any eq 69

        HOST-G(config)# access-list 100 permit udp any 172.16.75.128 0.0.0.127 eq 2000
        HOST-G(config)# access-list 100 permit udp 172.16.75.128 0.0.0.127 any eq 2000

        HOST-G(config)# access-list 100 permit udp 172.16.74.0 0.0.1.255 a
On port fastEthernet 0/1:

* Block External Spoofing:

        HOST-G(config)# access-list 110 deny ip 172.16.74.0 0.0.1.255 any

* Allow all ICMP echo requests and echo replies:
  
        HOST-G(config)# access-list 110 permit icmp any 172.16.74.0 0.0.1.255 echo
        HOST-G(config)# access-list 110 permit icmp any 172.16.74.0 0.0.1.255 echo-reply

* All traffic to the DMZ is blocked:

        HOST-G(config)# access-list 110 deny ip any 172.16.74.64 0.0.0.15
        HOST-G(config)# access-list 110 permit tcp any eq 80 host 172.16.74.67
        HOST-G(config)# access-list 110 permit tcp any eq 443 host 172.16.74.67

        HOST-G(config)# access-list 110 permit udp any eq 53 host 172.16.74.66

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-G(config)# access-list 100 permit upd any eq 89 host 12.0.0.1

        OR

        HOST-G(config)# access-list 110 permit ospf any host 12.0.0.1
