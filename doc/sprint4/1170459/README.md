RCOMP 2018-2019 Project - Sprint 4 - Member 1170459 folder
===========================================

## OSPF (Open Shortest Path First) dynamic routing ##

First of all we will establish the OSPF dynamic routing which is fast and easy to test and the main objective to the communication needed in the building and between buildings.

The first step is to **delete the static routing table configured (except the default one)** on sprint 3 and implement OSPF routing. The OSPF area used for building C is area id 3.
The OSPF works very efficiently because each router informs other routers about the network around it.

This is how its configured:

    HOST-C(config)# router OSPF PROCESS-ID
    HOST-C(config-router)#

**PROCESS-ID** is used to identify this specific configuration, and makes it possible to have several OSPF configurations running at the same time on the same router bu this works as an internal identifier and it has nothing to do with OSPF areas.


Now we must assign local networks to our OSPF configuration:

    HOST-C(config-router)# network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER

So there it is a pratical example used:

    HOST-C(config)# router OSPF 10
    HOST-C(config-router)#
    HOST-C(config-router)# network 172.16.68.0 0.0.0.127 area 3
    HOST-C(config-router)#
    HOST-C(config-router)# network 172.16.68.128 0.0.0.127 area 3
    HOST-C(config-router)#
    HOST-C(config-router)# network 172.16.69.0 0.0.0.127 area 3
    HOST-C(config-router)#
    HOST-C(config-router)# network 172.16.69.128 0.0.0.15 area 3
    HOST-C(config-router)#
    HOST-C(config-router)# network 172.16.69.192 0.0.0.63 area 3

-----------------------------------------------------------------------------

## HTTP (HyperText Transfer Protocol) ##

Then, we must add a new server that will hold an HTTP service. We just need to add a configure the HTML page identifying which building is stored the HTTP server.

The IPv4 given to this server was: 172.16.69.131/28

-----------------------------------------------------------------------------

## DNS (Domain Name System) ##

The member who has the building A on his charge will create a DNS domain name matching the team's repository name (rcomp-18-19-da-g2). This the highest level domain, so it's going to be used as if it was the DNS root domain.

In case of the building C, the local DNS domain is **building-C.rcomp-18-19-da-g2**. 

This DNS server should have the unqualified DNS name **ns**, so for building C it will be **ns.building-C.rcomp-18-19-da-g2**.

DNS database - Building C:
| Record Type | Record name | Record value |
|:-:|:-:|:-:|
| NS  | .  | ns  |
| A  | ns  | 172.16.65.2  |
| NS | building-c.rcomp-18-19-da-g2 | ns.building-c.rcomp-18-19-da-g2 |
| A | ns.building-c.rcomp-18-19-da-g2 | 172.16.69.130 |
| A | server1 | 172.16.69.131 |
| CNAME | web | server1 |
| CNAME | www | server1 |
| CNAME | dns | ns |

-----------------------------------------------------------------------------

## NS (Name Server) records and glue records ##

The NS record itself: the records name is the domain's name and the record's data is the DNS
name of the name server of that domain.

The A record for the NS record: the record's name is the DNS name of the name server and
the record's data is the IPv4 address the name server.

-----------------------------------------------------------------------------

## Other DNS records ##

All HTTP servers are to be named server1 (A record), it's ok they all have the same name because they belong to different DNS domains.

Within each DNS domain there should be a www alias (CNAME) mapped to the same domain's server1 A record, and another alias, named web also mapped to the domain's server1 A record.

One additional alias (CNAME), with name dns, and mapped to the domain's ns A record, should also
exist.

-----------------------------------------------------------------------------

## DNS client's configuration (end-nodes) ##

All end-nodes within a building should be using the local DNS name server, and, if supported, have the default DNS domain set to the local DNS domain.

For end-nodes with automatic DHCP configuration, that information should be inserted into the local DHCP pools:

    HOST-C(config)# ip dhcp pool ground-floor
    HOST-C(dhcp-config)# dns-server 172.16.69.130
    HOST-C(dhcp-config)#
    HOST-C(dhcp-config)# domain-name building-c.rcomp-18-19-da-g2

On the other hand, on DMZ servers (which don't have dhcp) that should be defined manually.

------------------------------------------------------------------------------

## NAT (Network Address Translation) ##

On this network static NAT will be applied not the dynamic one. So we can use it to redirect traffic.

The specified configurations are:

* HTTP and HTTPS requests received in the router's backbone interface are redirected to the **HTTP server in the local DMZ**. Both use TCP connections, assume the default service ports numbers are 80 and 443 respectively.

* DNS requests received in the router's backbone interface are redirected to the DNS server in the local DMZ. The DNS service may use either UDP or TCP, but in either case the default service port number is 53.

The executed commands were:

    HOST-C(config)# ip nat outside source static tcp 172.16.69.131 80 13.0.0.1 80
    HOST-C(config)#
    HOST-C(config)# ip nat outside source static tcp 172.16.69.131 443 13.0.0.1 443
    HOST-C(config)#
    HOST-C(config)# ip nat outside source static udp 172.16.69.130 53 13.0.0.1 53

-----------------------------------------------------------------------------------

## ACLs (Access Control Lists) ##

First of all we assure that we don't have any configured access lists:

        HOST-C(config)# no access-list 100
        HOST-C(config)# no access-list 110

Now, we configure the port of the intranet and the port directly connected to the backbone with the ACL's. We will use extended ACL's because are well defined than standard ones. With extended ACLs we can declare the protocol, the respective port and source/destination ip addresses.

On port fastEthernet 0/0:

* Block Internal Spoofing:

        HOST-C(config)# access-list 100 permit ip 172.16.68.0 0.0.1.255 any

* Allow all ICMP echo requests and echo replies:

        HOST-C(config)# access-list 100 permit icmp any 172.16.68.0 0.0.1.255 echo
        HOST-C(config)# access-list 100 permit icmp any 172.16.68.0 0.0.1.255 echo-reply

* All trafic from the DMZ is allowed:

        HOST-C(config)# access-list 100 ip 172.16.69.128 0.0.0.15 any

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-C(config)# access-list 100 permit udp any 172.16.68.0 0.0.0.128 eq 67
        HOST-C(config)# access-list 100 permit udp any 172.16.69.0 0.0.0.64 eq 67

        HOST-C(config)# access-list 100 permit udp 172.16.68.0 0.0.0.128 eq 67 any
        HOST-C(config)# access-list 100 permit udp 172.16.69.0 0.0.0.64 eq 67 any

        (Same commands for udp port 68)

        HOST-C(config)# access-list 100 permit udp any 172.16.69.192 0.0.0.63 eq 69
        HOST-C(config)# access-list 100 permit udp 172.16.69.192 0.0.0.63 any eq 69

        HOST-C(config)# access-list 100 permit udp any 172.16.69.192 0.0.0.63 eq 2000
        HOST-C(config)# access-list 100 permit udp 172.16.69.192 0.0.0.63 any eq 2000

        HOST-C(config)# access-list 100 permit udp 172.16.68.0 0.0.1.255 any eq 5351

On port fastEthernet 0/1:

* Block External Spoofing:

        HOST-C(config)# access-list 110 deny ip 172.16.68.0 0.0.1.255 any

* Allow all ICMP echo requests and echo replies:
  
        HOST-C(config)# access-list 110 permit icmp any 172.16.68.0 0.0.1.255 echo
        HOST-C(config)# access-list 110 permit icmp any 172.16.68.0 0.0.1.255 echo-reply

* All traffic to the DMZ is blocked:

        HOST-C(config)# access-list 110 deny ip any 172.16.69.128 0.0.0.15
        HOST-C(config)# access-list 110 permit tcp any eq 80 host 172.16.69.131
        HOST-C(config)# access-list 110 permit tcp any eq 443 host 172.16.69.131

        HOST-C(config)# access-list 110 permit udp any eq 53 host 172.16.69.130

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-C(config)# access-list 100 permit upd any eq 89 host 13.0.0.1

        OR

        HOST-C(config)# access-list 110 permit ospf any host 13.0.0.1

-----------------------------------------------------------------------------------------------------------------

## Extra information: ##

### Port Numbers: ###

* 53 - DNS
* 67 - DHCP
* 68 - DHCP Client
* 69 - TFTP
* 80 - HTTP
* 89 - OSPF
* 443 - HTTPS
* 2000 - ITS Services (defined on telephony-services mode)
* 5351 - Static NAT
