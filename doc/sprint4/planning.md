RCOMP 2018-2019 Project - Sprint 4 planning
===========================================
### Sprint master: 1170459 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
In this sprint, each team member will keep working on the same network simulation from the previous
sprint, regarding the same building. From the already established layer three configurations, now OSPF
based dynamic routing will be used to replace static routing used before.
Other configuration tasks encompassed in this sprint will be:
* Adding a second server to each DMZ network to run the HTTP service.
* Configuring DNS servers to establish a DNS domains tree.
* Enforcing NAT (Network Address Translation).
* Establishing traffic access policies (static firewall) on routers

# 2. Technical decisions and coordination #

## OSFP Dynamic Routing ##

-----------------------------------------------------------------------------

The existing routing tables should be erased except the default route established in the building A router.
The overall infrastructure will become now OSFP domain (Autonomous System), it'll be splitted into OSFP areas, one for each building and one for the backbone area (area id 0).

OSFP area ids to be used in each building must be settled and different from each others. **The team member in charge of building A must insert into the OSPF routing protocol the default route, pointing to the ISP router (default-information originate).**

## HTTP Servers ##

-----------------------------------------------------------------------------

In addition to the existing DMZ server (added on sprint 3), a new server must be added to the local DMZ network defining its IPv4 address manually. On this server the HTTP service must be enabled, and a simple HTML page should be created, at least identifying it as belonging to the building it does.

## DNS (Domain Name Service) ##

-----------------------------------------------------------------------------

Independent DNS domains are going to be established on each building.

The team member in charge of building A will create a DNS domain name matching the team's repository name **(rcomp-18-19-cc-gn)**. This is going to be the highest level domain, so it's going to be used as if it was the DNS root domain.

Team members in charge of other buildings will create local DNS domains named as: **building-X.rcomp-18-19-cc-gn**.
So these are subdomains of the first defined on building A.

In each building, there must be a DNS name server to support the local DNS domain, the server that was added to each DMZ, on the previous sprint, should be used.

All DNS servers should have the unqualified DNA name **ns**, so for building A it will be **ns.rcomp18-19-cc-gn**, and for instance for building C it will be **ns.building-C.rcomp-18-19-cc-gn**.

Additional required information to establish the DNS tree (and make DNS name servers work together):

* The DNS server for domain rcomp-18-19-cc-gn (building A) must know the IPv4 addresses of the name servers of its subdomains (each one in each of the other buildings).
* The DNS servers of other domains (other buildings) must know the IPv4 address of the name server of the rcomp-18-19-cc-gn domain.

## NS (Name Server) ##
-----------------------------------------------------------------------------

The way DNS works requires every domain to know the name servers of each of its subdomain, and also to know the name servers of the root domain. Knowing a name server that is out of the scope of the local domain requires is in fact two DNS records:

* The **NS record itself**: the record’s name is the domain’s name and the record’s data is the DNS name of the name server of that domain.
* The **A record for the NS record**: the record’s name is the DNS name of the name server and the record’s data is the IPv4 address the name server.

The last one is usually called a glue record, it’s required because the NS record doesn’t provide an IP address, yet that is required to contact the name server. In this case DNS name servers are using IPv4 only, if they were using IPv6, then there should be also an AAAA glue record as well.

## Other DNS records ##
-----------------------------------------------------------------------------

All HTTP servers are to be named **server1** (A record).

Within each DNS domain there should be a **www** alias (CNAME) mapped to the same domain's **server1** A record, and another alias, named **web** also mapped to the domain's **Server1** A record.

One additional alias (CNAME), with name **dns**, and mapped to the demain's **ns** A record, should also exist.

## DNS client's configuration (end-nodes)
-----------------------------------------------------------------------------

All end-nodes within a building should be using the local DNS name server, and, if supported, have the default DNS domain set to the local DNS domain.

For end-nodes with automatic DHCP configuration, that information should be inserted into the local DHCP pools (**dns-server** and **domain-name** commands).

For end-nodes with static manual configuration, namely servers, must be added manually.

## NAT (Network Address Translation) ##
-----------------------------------------------------------------------------

NAT can have several uses, and it's often enforced to hide private addresses (dynamic NAT). Static NAT, on the other hand, can be used to redirect traffic.
Each router (on all building) will require the configurations as follows:

* HTTP and HTTPS requests received in the router's backbone interface are redirected to the HTTP server on the local DMZ. Both HTTP and HTTPS use TCP connections, assume the default service ports numbers are used, 80 and 443.
* DNS requests received in the router's backbone interface are redirected to the DNS server in the local DMZ. The DNS service may use either UDP or TCP, but in either case the default service port number is 53. 
  
  **Both cases should be covered.**

## Static Firewall (ACLs) ##

-----------------------------------------------------------------------------
In CISCO IOS, a static firewall establishes rules (Access Control Lists) regarding which individual packets are allowed to be received or sent through a network interface and which are not. It’s called static or stateless because it always behaves the same way regardless of the transaction context in which the packet is analysed.

**In must be emphasised that the enforcement of ACLs should be the last task in this sprint, only once the previously described features are in place and well tested, then this subtask should be embraced.**

This is recommended approach because previously described features must still work once the ACLs are enforced. Thus, under troubleshooting point of view, by undertaking this method, the administrator knows any arising problem with previously described features is due to issues in firewall rules, and not the feature implementation itself.

Traffic access policies (which packets are allowed or not) are going to be implemented in routers, by the administrator in charge of each router. They will be particularly restrictive on traffic exchanged with the local DMZ, and traffic that is intended to the router itself.

**Traffic access policies to be enforced (by order):**

1.  Block all spoofing, as far as possible. Internal spoofing from local networks (DMZ excluded). External spoofing in traffic incoming from the backbone network.
2. All ICMP echo requests and echo replies are always allowed.
3. All traffic to the DMZ is to be blocked except for the DNS service and HTTP/HTTPS services to the corresponding servers. All traffic incoming from the DMZ is allowed.
4. All traffic directed to the router itself is to be blocked, except the traffic required for the current features to work.
5. Remaining traffic passing through the router should be allowed.

**Regarding the 4th access policy exceptions, they will encompass several services that must be allowed for networks where they are required, some not to be forgotten are:**
* DHCP service
* TFTP service
* ITS service
* OSPF traffic
* Traffic encompassed by the recently enforced NAT static rules

# 3. Subtasks assignment #
![tasks.png](tasks.png)

# 4. Useful Commands #

(needs information)
