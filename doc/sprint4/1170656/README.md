# RCOMP 2018-2019 Project - Sprint 4 - Member 1170656 folder #

## OSPF (Open Shortest Path First) dynamic routing ##

First, the OSPF dynamic routing which is fast and easy to test and  it works very efficiently because each router informs other routers about the network around it, its main objective to the communication needed in the building and between buildings.

The first step is to delete the static routing table configured on sprint 3 and implement OSPF routing. The OSPF area used for building E is the number 5.

This is how its configured:

    HOST-E(config)# router OSPF PROCESS-ID

**PROCESS-ID** is used to identify this specific configuration, and makes it possible to have several OSPF configurations running at the same time on the same router bu this works as an internal identifier and it has nothing to do with OSPF areas.


Now we must assign local networks to our OSPF configuration:

    HOST-E(config-router)# network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER

So there it is a pratical example used:

![networks.JPG](networks.JPG)

-----------------------------------------------------------------------------

## DNS (Domain Name System) ##

The member who has the building A on his charge will create a DNS domain name matching the team's repository name (rcomp-18-19-da-g2). This the highest level domain, so it's going to be used as if it was the DNS root domain.

In case of the building F, the local DNS domain is **building-E.rcomp-18-19-da-g2**. 

DNS database - Building E:  

![dns.JPG](dns.JPG)

-----------------------------------------------------------------------------

## NS (Name Server) records and glue records ##

The NS record itself: the record's name is the domain's name and the record's data is the DNS
name of the name server of that domain.


The A record for the NS record: the record�s name is the DNS name of the name server and the record�s data is the IPv4 address the name server

-----------------------------------------------------------------------------

## DNS client's configuration (end-nodes) ##

All end-nodes within a building should be using the local DNS name server, and, if supported, have the default DNS domain set to the local DNS domain.

For end-nodes with automatic DHCP configuration, that information should be inserted into the local DHCP pools:

    HOST-E(config)# ip dhcp pool ground-floor
    HOST-E(dhcp-config)# dns-server 172.16.72.66
    HOST-E(dhcp-config)#
    HOST-E(dhcp-config)# domain-name building-e.rcomp-18-19-da-g2

On the other hand, on DMZ servers (which don't have dhcp) that should be defined manually.

------------------------------------------------------------------------------

## NAT (Network Address Translation) ##

On this network static NAT will be applied not the dynamic one. So we can use it to redirect traffic.

The specified configurations are:

* HTTP and HTTPS requests received in the router's backbone interface are redirected to the **HTTP server in the local DMZ**. Both use TCP connections, assume the default service ports numbers are 80 and 443 respectively.

* DNS requests received in the router's backbone interface are redirected to the DNS server in the local DMZ. The DNS service may use either UDP or TCP, but in either case the default service port number is 53.

The executed commands were:

    HOST-E(config)# ip nat outside source static tcp 172.16.72.67 80 14.0.0.1 80
    HOST-E(config)#
    HOST-E(config)# ip nat outside source static tcp 172.16.72.67 443 14.0.0.1 443
    HOST-E(config)#
    HOST-E(config)# ip nat outside source static udp 172.16.72.66 53 14.0.0.1 53

-----------------------------------------------------------------------------------

## ACLs (Access Control Lists) ##

First of all we assure that we don't have any configured access lists:

        HOST-E(config)# no access-list 100
        HOST-E(config)# no access-list 110

Now, we configure the port of the intranet and the port directly connected to the backbone with the ACL's. We will use extended ACL's because are well defined than standard ones. With extended ACLs we can declare the protocol, the respective port and source/destination ip addresses.

On port fastEthernet 0/0:

* Block Internal Spoofing:

        HOST-E(config)# access-list 100 permit ip 172.16.72.0 0.0.1.255 any
        HOST-E(config)# access-list 100 permit ip 172.16.90.0 0.0.0.255 any

* Allow all ICMP echo requests and echo replies:

        HOST-E(config)# access-list 100 permit icmp any 172.16.72.0 0.0.1.255 echo
        HOST-E(config)# access-list 100 permit icmp any 172.16.72.0 0.0.1.255 echo-reply
        HOST-E(config)# access-list 100 permit icmp any 172.16.90.0 0.0.0.255 echo
        HOST-E(config)# access-list 100 permit icmp any 172.16.90.0 0.0.0.255 echo-reply


* All trafic from the DMZ is allowed:

        HOST-E(config)# access-list 100 permit ip 172.16.72.64 0.0.0.15 any

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-E(config)# access-list 100 permit udp any 172.16.74.0 0.0.0.64 eq 67
        HOST-E(config)# access-list 100 permit udp any 172.16.75.0 0.0.0.128 eq 67

        HOST-E(config)# access-list 100 permit udp 172.16.74.0 0.0.0.64 eq 67 any
        HOST-E(config)# access-list 100 permit udp 172.16.75.0 0.0.0.128 eq 67 any

        (Same commands for udp port 68)

        HOST-E(config)# access-list 100 permit udp any 172.16.75.128 0.0.0.127 eq 69
        HOST-E(config)# access-list 100 permit udp 172.16.75.128 0.0.0.127 any eq 69

        HOST-E(config)# access-list 100 permit udp any 172.16.75.128 0.0.0.127 eq 2000
        HOST-E(config)# access-list 100 permit udp 172.16.75.128 0.0.0.127 any eq 2000

        HOST-E(config)# access-list 100 permit udp 172.16.74.0 0.0.1.255 any eq 5351

On port fastEthernet 0/1:

* Block External Spoofing:

        HOST-E(config)# access-list 110 deny ip 172.16.74.0 0.0.1.255 any

* Allow all ICMP echo requests and echo replies:
  
        HOST-E(config)# access-list 110 permit icmp any 172.16.74.0 0.0.1.255 echo
        HOST-E(config)# access-list 110 permit icmp any 172.16.74.0 0.0.1.255 echo-reply

* All traffic to the DMZ is blocked:

        HOST-E(config)# access-list 110 deny ip any 172.16.74.64 0.0.0.15
        HOST-E(config)# access-list 110 permit tcp any eq 80 host 172.16.74.67
        HOST-E(config)# access-list 110 permit tcp any eq 443 host 172.16.74.67

        HOST-E(config)# access-list 110 permit udp any eq 53 host 172.16.74.66

* All traffic directed to the router itself s to be blocked except for the traffic required for the current features to work (DHCP, TFTP, ITS, OSPF, NAT static rules):

        HOST-E(config)# access-list 100 permit upd any eq 89 host 12.0.0.1

        OR

        HOST-E(config)# access-list 110 permit ospf any host 12.0.0.1



**Router Configuration**

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.16.72.1
ip dhcp excluded-address 172.16.73.1
ip dhcp excluded-address 172.16.72.129
ip dhcp excluded-address 172.16.76.1
ip dhcp excluded-address 172.16.90.1
!
ip dhcp pool vlan65
 network 172.16.72.0 255.255.255.192
 default-router 172.16.72.1
 dns-server 172.16.72.66
 domain-name building-e.rcomp-18-19-da-g2
ip dhcp pool vlan66
 network 172.16.73.0 255.255.255.0
 default-router 172.16.73.1
 dns-server 172.16.72.66
 domain-name building-e.rcomp-18-19-da-g2
ip dhcp pool vlan67
 network 172.16.72.128 255.255.255.128
 default-router 172.16.72.129
 dns-server 172.16.72.66
 domain-name building-e.rcomp-18-19-da-g2
ip dhcp pool vlan68
 network 172.16.90.0 255.255.255.128
 default-router 172.16.90.1
 option 150 ip 172.16.90.1
 dns-server 172.16.72.66
 domain-name building-e.rcomp-18-19-da-g2
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.65
 encapsulation dot1Q 65
 ip address 172.16.72.1 255.255.255.192
!
interface FastEthernet0/0.66
 encapsulation dot1Q 66
 ip address 172.16.73.1 255.255.255.0
!
interface FastEthernet0/0.67
 encapsulation dot1Q 67
 ip address 172.16.72.129 255.255.255.128
!
interface FastEthernet0/0.68
 encapsulation dot1Q 68
 ip address 172.16.90.1 255.255.255.128
!
interface FastEthernet0/0.69
 encapsulation dot1Q 69
 ip address 172.16.72.65 255.255.255.240
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 10
 log-adjacency-changes
 network 172.16.72.0 0.0.0.63 area 5
 network 172.16.73.0 0.0.0.255 area 5
 network 172.16.72.128 0.0.0.127 area 5
 network 172.16.90.0 0.0.0.127 area 5
 network 172.16.72.64 0.0.0.15 area 5
 network 14.0.0.0 0.0.0.3 area 0
!
router rip
!
ip nat outside source static tcp 14.0.0.1 80 172.16.72.67 80 
ip nat outside source static tcp 14.0.0.1 443 172.16.72.67 443 
ip nat outside source static udp 172.16.72.66 53 14.0.0.1 53 
ip classless
!
ip flow-export version 9
!
!
access-list 100 permit ip 172.16.72.0 0.0.1.255 any
access-list 100 permit ip 172.16.90.0 0.0.0.255 any
access-list 100 permit icmp any 172.16.90.0 0.0.0.255 echo
access-list 100 permit icmp any 172.16.90.0 0.0.0.255 echo-reply
access-list 100 permit icmp any 172.16.72.0 0.0.1.255 echo
access-list 100 permit icmp any 172.16.72.0 0.0.1.255 echo-reply
access-list 100 permit ip 172.16.72.64 0.0.0.15 any
!
!
!
!
!
!
telephony-service
 max-ephones 2
 max-dn 2
 ip source-address 172.16.76.1 port 2000
 auto assign 1 to 2
!
ephone-dn 1
 number 925073
!
ephone-dn 2
 number 925074
!
ephone 1
 device-security-mode none
 mac-address 0001.9736.D7E9
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0002.167B.C336
 type 7960
 button 1:2  
!  
line con 0  
!  
line aux 0  
!  
line vty 0 4  
 login  
!  
!  
!  
end  

